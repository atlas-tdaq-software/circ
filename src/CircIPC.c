/***************************************************************************/
/* CircIPC.c, 26/12/96
 *
 * Contains 'Circ' code related to Inter-Process-Communication (IPC).
 *
 * We have to deal with 3 types of IPC: 
 *
 * 1. between threads: threads can share normal malloc'ed memory and
 *    a POSIX mutex is used to syncronize access (very fast!).
 * 2. between processes using SYSV shared memory and semapore.
 * 3. between processes using POSIX shared memory and semapore.
 * 
 * One problem is how to define a shared memory region and its associated 
 * semaphore for the processes. The solution chosen is the POSIX like way
 * using key-files. These files are specified through a unique path-name
 * and created automatically if they do not already exist at creation
 * time. The following extensions are then added to the given path-name:
 *         <path-name>._CIRC_SEM and <path-name>._CIRC_SHM.
 * At the end they are removed so they should normally signal a running
 * circular buffer. So normally the user do not have to care about these
 * files. 
 */
/***************************************************************************/

#ifdef USE_THREAD
#include <pthread.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "circ/Error.h"
#define THIS_IS_CircIPC
#include "circ/Circ.h"
#include "circ/Circ_private.h"
#include "circ/Circ_params.h"

/***************************************************************************/
/* Semaphores:                                                             */
/***************************************************************************/

int CircSemCreate(struct CircBuffer* cb, char* keyname, int master)
/*-----------------------------------------------------------------
 * Opens and initializes a semaphore to be used for this circular buffer.
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
#ifdef USE_THREAD
  SEMID* semid = &cb->sem_id;
 
  pthread_mutexattr_t mut_attr;

  char key[256];           /* These lines just to shut up the compiler */

  if (master)
    sprintf(key,"%s._CIRC_SEM",keyname);

/*.........................................................................*/
  
  DEBUG(("CircSemCreate> cid=%d: we use a POSIX mutex for locking.\n",
	 cb->cid));
#if defined AIX || defined Linux
  if ( pthread_mutexattr_init(&mut_attr) )
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: pthread_mutexattr_init", 
		     cb->cid);
      return (Error_YES);
    }
  if ( pthread_mutex_init(semid, &mut_attr) )
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: pthread_mutex_init",
		     cb->cid);
      return (Error_YES);
    }
#else
  if ( pthread_mutexattr_create(&mut_attr) < 0 )
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: pthread_mutexattr_create", 
		     cb->cid);
      return (Error_YES);
    }
  if ( pthread_mutex_init(semid, mut_attr) < 0 )
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: pthread_mutex_init",
		     cb->cid);
      return (Error_YES);
    }
#endif
#else

#ifdef USE_POSIX_SEM
  SEMID semid;
  char key[256];
  int  oflags, value;
/*.........................................................................*/

  sprintf(key,"%s._CIRC_SEM",keyname);
  DEBUG(("CircSemCreate> cid=%d: we use POSIX semaphore %s for locking\n",
	 cb->cid, key));
  
  /* create a semaphore in unlocked state 
   */
  if (master)
    oflags = O_CREAT;
  else
    oflags = 0;
  
  DEBUG(("CircSemCreate> open semaphore with flags=%x\n",oflags));
  if ( (semid = sem_open(key, oflags, 0644, 1)) == (void*)-1)
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: can not open sem for '%s'",
		     cb->cid, key);
      return(Error_YES);
    }
  sem_getvalue(semid,&value);
  DEBUG(("CircSemCreate> semaphore value at open: %d\n",value));
  cb->sem_id = semid;
  cb->sem_name = (char*)malloc(strlen(key)+1);
  strcpy(cb->sem_name, key);
#endif 

#ifdef USE_SYSV_SEM
  SEMID semid;
  int semkey;
  char key[256];
/*.........................................................................*/

  sprintf(key,"%s._CIRC_SEM",keyname);
  DEBUG(("CircSemCreate> cid=%d: we use SYSV semaphore %s for locking\n",
	 cb->cid, key));
  if (master) /* create file if it does not exist */
    {
      int fd;
      int oflags = O_CREAT | O_RDWR;
      int omode  = S_IRWXU | S_IRWXG | S_IRWXO;
      if ( (fd = open(key,oflags,omode)) < 0)
	{
	  ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: can not open key file %s",
			  cb->cid, key);
	  return(Error_YES);
	}
      else
	close(fd);
   }

  if ( (semkey = ftok(key, 1)) < 0 )
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: can not get sem-key for %s",
		      cb->cid, key);
      return (Error_YES);
    }

  if ( (semid = semget(semkey, 1, IPC_CREAT | 0777)) < 0)
    {
      ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: can not get sem-id for '%x'",
		      cb->cid, semkey);
      return (Error_YES);
    }

  /* initialize lock semaphore operations */
  waitop[0].sem_num = clrop[0].sem_num  = 0;
  waitop[0].sem_op  = -1;
  clrop[0].sem_op   =  1;
  clrop[0].sem_flg  = waitop[0].sem_flg = 0;

/*   if (master) */
    { 
      int arg = 1;
      /* initialize locking semaphore,initial state unlocked */
      if ( semctl(semid, 0, SETVAL, arg) < 0 )
	{
	  ErrorSetSystemF(CIRC_ERR_SEMCREATE,"CircSemCreate","cid=%d: can not initialize sem",
			  cb->cid);
	  return(Error_YES);
	}
    }
  cb->sem_name = (char*)malloc(strlen(key)+1);
  strcpy(cb->sem_name, key);
  cb->sem_id = semid;
#endif

#endif

  return (Error_NO);
}

/***************************************************************************/
int CircSemDestroy(struct CircBuffer* cb)
/*---------------------------------------
 * Close and remove all related objects including the key-files. 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
/*.........................................................................*/
#ifdef USE_THREAD
  if (cb->shm_size > 0)
    {
#if defined AIX || defined Linux
      if (pthread_mutex_destroy (&cb->sem_id))
	ErrorSetSystem (CIRC_ERR_SEMDESTROY,"CircSemDestroy","pthread_mutex_destroy");
#else
      if (pthread_mutex_destroy (&cb->sem_id) < 0)
	ErrorSetSystem (CIRC_ERR_SEMDESTROY,"CircSemDestroy","pthread_mutex_destroy");
#endif
/*
   if (pthread_mutexattr_delete(&mut_attr) < 0)
     ErrorSetSystem(CIRC_ERR_SEMDESTROY,"CircSemDestroy","pthread_mutexattr_delete");
 */
    }
#else

#ifdef USE_POSIX_SEM
  {
    int value;
    sem_getvalue(cb->sem_id,&value);
    DEBUG(("CircSemDestroy> semaphore value at close: %d\n",value));
  }
  sem_close (cb->sem_id);
  if (cb->shm_size > 0)
    unlink (cb->sem_name);
#endif

#ifdef USE_SYSV_SEM
  if (cb->shm_size > 0) /* Clean up, remove SHM and semaphore and key file */
    {
#ifdef Linux
      if (semctl (cb->sem_id, 0, IPC_RMID, NULL) < 0)
#else
      if (semctl (cb->sem_id, 0, IPC_RMID) < 0)
#endif
	ErrorSetSystem (CIRC_ERR_SEMDESTROY,"CircSemDestroy","can not remove semaphores (semctl)");
      if (shmctl (cb->shm_id, IPC_RMID, NULL) < 0)
        ErrorSetSystem (CIRC_ERR_SEMDESTROY,"CircSemDestroy","can not remove SHM (shmctl)");
      unlink(cb->sem_name);
    }
#endif

#endif

  return(Error_NO);
}

/***************************************************************************/
/* Shared memory:                                                          */
/***************************************************************************/

/***************************************************************************/
int CircShmCreate(struct CircBuffer* cb, char* keyname, int size)
/*---------------------------------------------------------------
 * Opens and initializes shared memory to be used for this circular buffer.
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *shmptr;
  int  shmid;
/*.........................................................................*/

#ifdef USE_THREAD
  char key[256];           /* These two lines just to shut up the compiler */
  sprintf(key,"%s._CIRC_SHM",keyname);

/*.........................................................................*/
  if ( (shmptr = (char*)malloc(size)) == NULL)
    {
      ErrorSetSystemF(CIRC_ERR_SHMCREATE,"CircShmMap","cid=%d: malloc of %d bytes failed",
		      cb->cid, size);
      return (Error_YES);
    }
  shmid = 0;

#else

#ifdef USE_POSIX_SHM
  u_int omode, oflags;
  char key[256];
/*.........................................................................*/

  /* Open the shared memory object. Create it if we are the master!
   */
  sprintf(key,"%s._CIRC_SHM",keyname);
  DEBUG(("CircShmCreate> cid=%d: we use POSIX shared memory defined by %s\n",
	 cb->cid, key));
  if (size > 0)
    {
      oflags = O_CREAT | O_RDWR | O_TRUNC;
      omode  = S_IRWXU | S_IRWXG | S_IRWXO;
    }
  else
    {
      oflags = O_RDWR;
      omode  = 0;
    }
  if ( (shmid = shm_open(key, oflags, omode)) < 0 )
    {
      ErrorSetSystemF(CIRC_ERR_SHMCREATE,"CircShmCreate","cid=%d: can not open shm for '%s'",
		      cb->cid, key);
      return(Error_YES);
    }

  if (size > 0)
    {
      /* Align length on page boundary
       */
#ifdef __osf__
      int pagesize = sysconf(_SC_PAGE_SIZE);
#else
      int pagesize = 4096;
#endif

      if ( size & (pagesize-1) )
	size = (size & ~(pagesize-1)) + pagesize;

      /* Set the file size accordingly
       */
      if ( ftruncate(shmid, size) < 0)
	{
	  ErrorSetSystemF(CIRC_ERR_SHMCREATE,"CircShmMap","cid=%d: ftruncate of size %d failed",
			  cb->cid, size);
	  return (Error_YES);
	}
      fsync(shmid);
    }
  else
    {
      struct stat this_stat;

      /* Get the file size which should be equal to the size of the 
       * shared memory
       */
      if (fstat(shmid, &this_stat) < 0) 
	{
	  ErrorSetSystemF(CIRC_ERR_SHMMAP,"CircShmMap","cid=%d: can not get file-size (fd=%d)",
			  cb->cid, shmid);
	  return (Error_YES);
	}
      size = this_stat.st_size;
    }

  /* Map a shared memory region previously opened and defined by shmid.
   */
  DEBUG(("CircShmMap> mmap %d bytes of shared memory\n",size));
  if ( (shmptr = (char*)mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED,
			     shmid, 0)) == (char*)-1 )
    {
      ErrorSetSystemF(CIRC_ERR_SHMMAP,"CircShmMap","cid=%d: mmap failed, shmid=%x", 
		      cb->cid, shmid);
      return (Error_YES);
    }
  cb->shm_size_real = size;

#endif

#ifdef USE_SYSV_SHM
  int shmkey;
  char key[256];
/*.........................................................................*/

  sprintf(key,"%s._CIRC_SHM",keyname);
  DEBUG(("CircShmCreate> cid=%d: we use SYSV shared memory defined by %s\n",
	 cb->cid, key));
  if (size > 0)
    {
      int fd, omode, oflags;
      oflags = O_CREAT | O_RDWR;
      omode  = S_IRWXU | S_IRWXG | S_IRWXO;
  
      if ( (fd = open(key,oflags,omode)) < 0)
	{
	  ErrorSetSystemF(CIRC_ERR_SHMCREATE,"CircShmCreate","cid=%d: can not open shm key file %s",
			  cb->cid, key);
	  return(Error_YES);
	}
      else
	close(fd);
   }

  if ( (shmkey = ftok(key, 1)) < 0 )
    {
      ErrorSetSystemF(CIRC_ERR_SHMCREATE,"CircShmCreate","cid=%d: can not get shm-key for %s",
		      cb->cid, key);
      return (Error_YES);
    }
  if ( (shmid = shmget(shmkey, size, IPC_CREAT | 0777)) < 0 )
    {
      ErrorSetSystemF(CIRC_ERR_SHMCREATE,"CircShmCreate","cid=%d: can not get shm (%d bytes)",
		      cb->cid, size);
      return (Error_YES);
    }

  /* Map a shared memory region previously opened and defined by shmid.
   */
  if ( (shmptr = (char*)shmat(shmid, NULL, SHM_R | SHM_W)) == NULL)
    {
      ErrorSetSystemF(CIRC_ERR_SHMMAP,"CircShmCreate","cid=%d: shmat failed, shmid=%x", 
		      cb->cid, shmid);
      return (Error_YES);
    }
#endif

  cb->shm_id   = shmid;
  cb->shm_name = (char*)malloc(strlen(key)+1);
  strcpy(cb->shm_name, key);

#endif/*USE_THREAD*/

  cb->shm_desc  = (struct CircDesc*)shmptr;
  return(Error_NO);
}

/***************************************************************************/
int CircShmDestroy(struct CircBuffer* cb)
/*---------------------------------------
 * Close and remove all related objects including the key-files 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
/*.........................................................................*/
  if (cb->shm_id != -1)
    {
#ifdef USE_POSIX_SHM
      munmap    (cb->shm_desc, cb->shm_size_real);
      close     (cb->shm_id);
      if (cb->shm_size > 0)
	shm_unlink(cb->shm_name);
#endif

#ifdef USE_SYSV_SHM
      if (shmdt ((char *) cb->shm_desc) < 0)
	ErrorSetSystem (CIRC_ERR_SHMDESTROY,"CircShmDestroy","can not detach SHM (shmdt)");
      if (cb->shm_size > 0)
	unlink(cb->shm_name);
#endif
      DEBUG(("CircShmDestroy> %s cleanup done.\n",
	     (cb->shm_size > 0)?"Master":"Slave")); 
    }

  return(Error_NO);
}

/***************************************************************************/
int CircShmInfo (struct CircBuffer* cb, struct shmid_ds* meminfo)
/*-------------------------------------------------------------------------
 * Get information about the buffer.
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
/*.........................................................................*/
  if (cb->shm_id != -1)
    {
#ifdef USE_THREAD
      /* Unuseful in this case... */
      memset (meminfo, '0', sizeof (struct shmid_ds));
#else
      if (shmctl (cb->shm_id, IPC_STAT, meminfo) < 0)
	{
	  ErrorSetSystem (CIRC_ERR_SHMINFO,"CircShmInfo",
			  "Can not get memory information");
	  return (Error_YES);
	}
#endif

      DEBUG(("CircShmInfo> Got shared memory information.\n")); 
    }

  return(Error_NO);
}
