/***************************************************************************/
/* Circservice.c 
 *
 * =h=> Circular buffer server and client
 * =p=>
 * This file contains add-on to the application interface.
 * A buffer server can be asked to open a buffer as
 * master (or slave) by a client. The client/server handshaking
 * uses TCP/IP protocol.
 * The server takes care of cleaning up the unused buffers in
 * case of crash on the client side.
 * Not available in threaded mode but usable by threaded applications.
 * ===<
 * Modification history:
 * 22/09/08 EP  : file created
 * 18/03/11 EP  : CircServerServeRequest has now an argument to return the
 *                request type. Allows clean management of buffer lists
 *                outside the library.
 * 09/08/12 EP  : Open and close calls protected.
 * 25/04/21 EP  : Connect request added to allow multiple open and close
 *                requests for the same buffer. Client list and connection
 *                counters added.
 *                Ancillary function CircServerGetInstances added.
 * 23/06/22 EP  : CircServerServeRequest splitted in two functions
 *                (CircServerGetRequest and CircServerExecuteRequest).
 *                That allows to pass to the external world the request type before
 *                executing it, allowing to manage buffer lists without additional
 *                mutexes. That requires that the connection description is
 *                passed as well - not very beautiful, but safe if correctly managed.
 */
/***************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <sys/select.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "circ/Error.h"

#include "circ/Circ.h"
#include "circ/Circutils.h"
#include "circ/Circservice.h"
#include "circ/Circ_private.h"
#include "circ/Circ_params.h"

#define FALSE   0
#define TRUE    1

#ifndef USE_THREAD

int fd0;
struct buflist_elem *buflist = (struct buflist_elem *) NULL;
struct buflist_elem *clientlist = (struct buflist_elem *) NULL;

static pthread_mutex_t clist_mutex_;

/***************************************************************************/
/* =i=> */
int CircSafeSend (int fd, char *buf, int buflen)
/*----------------------------------------------------
 * =p=>
 * Safe tcp send function.
 *
 *  buf:    buffer to be sent.
 *  buflen: buffer length.
 *
 * Returns the length of the information sent.  
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  fd_set fds;
  int nsel;
  int sent = 0;

  FD_ZERO (&fds);
  FD_SET (fd, &fds);

  if ((nsel = select ((fd + 1), 0, &fds, 0, 0)) > 0)
  {
    if (FD_ISSET (fd, &fds))
    {
      while (sent < buflen)
      {
	int count = write (fd, buf + sent, buflen - sent);
	sent += count;

	if (count == 0)
	{
	  /* connection has been closed, return 0 */

	  return count;
	}
	else if (count < 0)
	{
	  if (errno == EINTR)
	    continue;
	  else
	  {
	    ErrorSetSystem (CIRC_ERR_SOCKET,"CircSafeSend",
			    "write on socket failed");
	    return count;
	  }
	}
      }
    }
  }
  else
  {
    if (errno != EINTR)
    {
      ErrorSetSystemF (CIRC_ERR_SOCKET, "CircSafeSend",
		       "select failed with mask=0x%x",
		       fds);
      return nsel;
    }
  }

  return sent;
}

/***************************************************************************/
/* =i=> */
int CircSafeReceive (int fd, char *buf, int buflen)
/*----------------------------------------------------
 * =p=>
 * Safe tcp receive function.
 *
 *  buf:    buffer to receive data.
 *  buflen: expected data length.
 *  
 * Returns the length of the received information or the
 * return value of select or read in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  fd_set fds;
  int nsel;
  int received = 0;

  FD_ZERO (&fds);
  FD_SET (fd, &fds);

  if ((nsel = select ((fd + 1), &fds, 0, 0, 0)) > 0)
  {
    if (FD_ISSET (fd, &fds))
    {
      while (received < buflen)
      {
	int count = read (fd, buf + received, buflen - received);
	received += count;

	if (count == 0)
	{
	  /* connection has been closed, return 0 */

	  return count;
	}
	else if (count < 0)
	{
	  if (errno == EINTR)
	    continue;
	  else
	  {
	    ErrorSetSystem (CIRC_ERR_SOCKET,"CircSafeReceive",
			    "read on socket failed");
	    return count;
	  }
	}
      }
    }
  }
  else
  {
    if (errno != EINTR)
    {
      ErrorSetSystemF (CIRC_ERR_SOCKET, "CircSafeReceive",
		       "select failed with mask=0x%x",
		       fds);
      return nsel;
    }
  }

  return received;
}

/***************************************************************************/
/* =i=> */
int CircServerInit (unsigned short port)
/*----------------------------------------------------
 * =p=>
 * Initialize the Circular buffer server.
 *
 *  port: TCP port the bind the service to. If port = 0, default is used.
 *  
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int val = 1;
  struct sockaddr_in sinme;

  /* Initialize the pointer to the circ list */

  buflist = (struct buflist_elem *) NULL;

  /* Initialize a global mutex to protect circ list */

  pthread_mutex_init (&clist_mutex_, 0);

  /* Initialize TCP listener */

  if (port == 0)
    port = CIRC_SERVER_DEFAULT_PORT;

  if ((fd0 = socket (AF_INET, SOCK_STREAM, 0)) < 0)
  {
    ErrorSetSystem (CIRC_ERR_SOCKET, "CircServerInit", "open socket failed");
    return (Error_YES);
  }

  if (setsockopt (fd0, SOL_SOCKET, SO_REUSEADDR,
		  (char *) &val, sizeof (val)) < 0)
    ErrorSetSystem (CIRC_ERR_SOCKET, "CircServerInit", "setsockopt REUSEADDR failed");

  if (setsockopt (fd0, IPPROTO_TCP, TCP_NODELAY,
		  (char *) &val, sizeof (val)) < 0)
    ErrorSetSystem (CIRC_ERR_SOCKET, "CircServerInit", "setsockopt TCP_NODELAY failed");

  /* bind host */

  sinme.sin_family      = AF_INET;
  sinme.sin_addr.s_addr = INADDR_ANY;
  sinme.sin_port        = htons (port);

  if (bind (fd0, (struct sockaddr *) &sinme, sizeof (sinme)) < 0)
  {
    ErrorSetSystemF(CIRC_ERR_SOCKET, "CircServerInit", "bind of port %d failed",
		    port);
    return (Error_YES);
  }

  /* listen, allow maximum possible queue */

  if (listen (fd0, SOMAXCONN) < 0)
  {
    ErrorSetSystemF (CIRC_ERR_SOCKET, "CircServerInit", "listen on port %d failed",
                     port);
    return (Error_YES);
  }

  return (Error_NO);
}

/***************************************************************************/
/* =i=> */
int CircServerAddToList (struct buflist_elem **clist, int cid, char *keyname)
/*----------------------------------------------------
 * =p=>
 * Add a buffer id and name to a linked list.
 *  
 * Returns list length or -1 in case of error.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int list_len = 0;
  int found = FALSE;

  struct buflist_elem *thebuf;
  struct buflist_elem *newbuf;
  struct buflist_elem *lastbuf = (struct buflist_elem *) NULL;

  thebuf = *clist;

  pthread_mutex_lock (&clist_mutex_);

  while (thebuf)
  {
    if (!strcmp (thebuf->bufname, keyname))
    {
      ++thebuf->connected;
      found = TRUE;
    }

    ++list_len;
    thebuf = thebuf->next;
  }

  if (! found)
  {
    if ((newbuf =
	 (struct buflist_elem *) malloc (sizeof (struct buflist_elem))) ==
	(struct buflist_elem *) NULL)
    {
      ErrorSetF (CIRC_ERR_MALLOC, "CircServerAddToList",
		 "Memory allocation error");
      pthread_mutex_unlock (&clist_mutex_);
      return -1;
    }

    if ((newbuf->bufname =
	 (char *) malloc (strlen (keyname) + 1)) == NULL)
    {
      free (newbuf);
      ErrorSetF (CIRC_ERR_MALLOC, "CircServerAddToList",
		 "Memory allocation error");
      pthread_mutex_unlock (&clist_mutex_);
      return -1;
    }

    newbuf->cid = cid;
    newbuf->connected = 1;

    time (&(newbuf->buftime));
    strcpy (newbuf->bufname, keyname);

    newbuf->last = (struct buflist_elem *) NULL;
    newbuf->next = (struct buflist_elem *) NULL;

    if (*clist)
    {
      thebuf = buflist;

      while (thebuf)
      {
	lastbuf = thebuf;
	thebuf = thebuf->next;
      }

      lastbuf->next = newbuf;
      newbuf->last = lastbuf;
    }
    else
      *clist = newbuf;

    ++list_len;
  }

  pthread_mutex_unlock (&clist_mutex_);

  return list_len;
}

/***************************************************************************/
/* =i=> */
int CircServerRemoveFromList (struct buflist_elem **clist, int cid)
/*----------------------------------------------------
 * =p=>
 * Remove a buffer id and name from a linked list.
 *  
 * Returns list length or -1 in case of error.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int retval = -1;
  int found = FALSE;
  int list_len = 0;
  struct buflist_elem *thebuf;

  pthread_mutex_lock (&clist_mutex_);

  thebuf = *clist;

  while (thebuf)
  {
    struct buflist_elem *lastbuf = thebuf->last;
    struct buflist_elem *nextbuf = thebuf->next;

    if (thebuf->cid == cid)
    {
      if (thebuf->connected > 1)
        --thebuf->connected;
      else
      {
        free (thebuf->bufname);
        free (thebuf);

        if (nextbuf)
	  nextbuf->last = lastbuf;

        if (lastbuf)
	  lastbuf->next = nextbuf;
        else
	  *clist = nextbuf;
      }

      found = TRUE;
      break;
    }

    thebuf = nextbuf;
  }

  if (found)
  {
    thebuf = *clist;
    
    while (thebuf)
    {
      ++list_len;
      thebuf = thebuf->next;
    }

    retval = list_len;
  }

  pthread_mutex_unlock (&clist_mutex_);

  return retval;
}

/***************************************************************************/
/* =i=> */
int CircServerSearchList (struct buflist_elem *clist, char *keyname)
/*----------------------------------------------------
 * =p=>
 * Search the buffer list for a buffer key keyname.
 *  
 * Returns cid or -1 in case of error.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int retval, cid = -1;
  struct buflist_elem *thebuf = clist;

  pthread_mutex_lock (&clist_mutex_);

  while (thebuf)
  {
    if (!strcmp (thebuf->bufname, keyname))
      break;

    thebuf = thebuf->next;
  }

  retval = thebuf ? thebuf->cid : cid;

  pthread_mutex_unlock (&clist_mutex_);

  return retval;
}

/***************************************************************************/
/* =i=> */
int CircServerGetCid (char *keyname)
/*----------------------------------------------------
 * =p=>
 * Search the buffer list for a buffer key keyname. Usable by external programs
 * implementing a server.
 *
 * Returns cid or -1 in case of error.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int retval, cid = -1;
  struct buflist_elem *thebuf = buflist;

  pthread_mutex_lock (&clist_mutex_);

  while (thebuf)
  {
    if (!strcmp (thebuf->bufname, keyname))
      break;

    thebuf = thebuf->next;
  }

  retval = thebuf ? thebuf->cid : cid;

  pthread_mutex_unlock (&clist_mutex_);

  return retval;
}

int CircServerGetInstances (struct buflist_elem *clist, char *keyname)
/*----------------------------------------------------
 * =p=>
 * Get the number of clients connected to a buffer with key keyname.
 *  
 * Returns the number of instances or -1 in case keyname is not in the list.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int retval, err = -1;
  struct buflist_elem *thebuf = clist;

  pthread_mutex_lock (&clist_mutex_);

  while (thebuf)
  {
    if (!strcmp (thebuf->bufname, keyname))
      break;

    thebuf = thebuf->next;
  }

  retval = thebuf ? thebuf->connected : err;

  pthread_mutex_unlock (&clist_mutex_);

  return retval;
}

/***************************************************************************/
/* =i=> */
void CircServerCleanAll ()
/*----------------------------------------------------
 * =p=>
 * Cleans up all the active buffers.
 *  
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  struct buflist_elem *thebuf = buflist;
  struct buflist_elem *lastbuf;

  pthread_mutex_lock (&clist_mutex_);

  while (thebuf)
  {
    CircClose (thebuf->cid);

    free (thebuf->bufname);
    lastbuf = thebuf;
    thebuf = thebuf->next;
    free (lastbuf);
  }

  buflist = (struct buflist_elem *) NULL;

  pthread_mutex_unlock (&clist_mutex_);
}

/***************************************************************************/
/* =i=> */
int CircServerGarbageCollector ()
/*----------------------------------------------------
 * =p=>
 * Cleans up the buffers left opened by crashed or disconnected
 * clients. Useful to free system resources during a server's life.
 * CircClose call is protected with the list mutex.
 *  
 * Returns the number of deleted buffers.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  static const double mintime = CIRC_SERVER_MIN_CONNECTION_LIFETIME;
  struct buflist_elem *thebuf;
  int ndel = 0;

  pthread_mutex_lock (&clist_mutex_);

  thebuf = buflist;

  while (thebuf)
  {
    struct buflist_elem *lastbuf = thebuf->last;
    struct buflist_elem *nextbuf = thebuf->next;

    struct shmid_ds meminfo;

    /* Get buffer parameters */

    CircGetInfo (thebuf->cid, &meminfo);

    if (meminfo.shm_nattch > 0) /* We are using real IPC (no threaded version) and are attached */
    {
      if (meminfo.shm_nattch < 2) /* No client is attached */
      {
	/* Check client's connection lifetime */

	if (difftime (meminfo.shm_dtime, thebuf->buftime) > mintime)
	{
	  /* Close the buffer */

	  CircClose (thebuf->cid);
	  ++ndel;

	  /* Remove the buffer element from the list */

	  free (thebuf->bufname);
	  free (thebuf);

	  if (nextbuf)
	    nextbuf->last = lastbuf;

	  if (lastbuf)
	    lastbuf->next = nextbuf;
	  else
	    buflist = nextbuf;
	}
      }
    }

    thebuf = nextbuf;
  }

  pthread_mutex_unlock (&clist_mutex_);

  return (ndel);
}

/***************************************************************************/
/* =i=> */
int CircServerGetCircList (int **cids, int *cidlen)
/*----------------------------------------------------
 * =p=>
 * Gives the identifiers of the buffers owned
 * by the server.
 * cids is a pointer to a vector to be filled by this function
 * and cidlen is the pointer to its length. If the number of open buffers
 * is greater than cidlen, the vector is re-allocated.
 * The caller is responsible for freeing cids.
 * The number of buffers is returned.
 * WARNING: cids cannot be used without further protection in a 
 * multi-threaded environment, as well as any direct call to CircClose
 * should be forbidden.
 *
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int nc = 0;
  int l = *cidlen;
  int *v = *cids;
  static const int chunk = 64;
  register struct buflist_elem *theelem;

  pthread_mutex_lock (&clist_mutex_);

  for (theelem = buflist; theelem; theelem = theelem->next, ++nc)
  {
    if ((nc + 1) > (l - 1))
    {
      l += chunk;

      if ((v = (int *) realloc (v, l * sizeof (int))) == (int *) NULL)
      {
	ErrorSetF (CIRC_ERR_MALLOC, "CircServerGetCircList",
		   "Memory allocation error");
	pthread_mutex_unlock (&clist_mutex_);
	return -1;
      }

      *cidlen += l;
      *cids = v;
    }

    v[nc] = theelem->cid;
  }

  pthread_mutex_unlock (&clist_mutex_);

  return nc;
}

/***************************************************************************/
/* =i=> */
int CircServerCheckRequest ()
/*----------------------------------------------------
 * =p=>
 * Check if a request to open/close a buffer is available.
 *  
 * Returns 1 in case a request is available, 0 otherwise.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  struct timeval t = {0, 100000};

  fd_set readfds;
  FD_ZERO (&readfds);
  FD_SET (fd0, &readfds);

  return (select ((fd0 + 1), &readfds, 0, 0, &t) > 0);
}

/***************************************************************************/
/* =i=> */
int CircServerGetRequest (struct circserver_request *request)
/*----------------------------------------------------
 * =p=>
 * Accept a connection and get a request.
 * The request parameters are returned in struct circserver_request *request.
 *  
 * Returns fd > 0 in case of success.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int fd = -1;
  struct sockaddr addr;
  socklen_t addrlen;

  int namelength;
  uint32_t length;

  request->reqtype = CIRC_SERVER_REQTYPE_NULL;;
  request->circsize = 0;
  request->circname = (char *) NULL;

  addrlen = sizeof (addr);

  if (CircServerCheckRequest ())
  {
    if ((fd = accept (fd0, &addr, &addrlen)) > 0)
    {
      uint32_t type;

      if (CircSafeReceive (fd, (char *) &type, sizeof (type)) < (int) sizeof (type))
      {
	ErrorSet (CIRC_ERR_SOCKET, "CircServerGetRequest",
		  "Couldn't get request type");
	close (fd);
	return -1;
      }

      request->reqtype = ntohl (type);
    }

    if (CircSafeReceive (fd, (char *) &length, sizeof (length)) < (int) sizeof (length))
    {
      ErrorSet (CIRC_ERR_SOCKET, "CircServerGetRequest",
		"Couldn't get circ name");
      close (fd);
      return -1;
    }

    namelength = ntohl (length);

    if ((request->circname = (char *) malloc (namelength + 1)) == (char *) NULL)
    {
      ErrorSetSystem (CIRC_ERR_MALLOC, "CircServerGetRequest",
		      "Memory allocation failed");
      close (fd);
      return -1;
    }

    if (CircSafeReceive (fd, request->circname, namelength) < namelength)
    {
      ErrorSet (CIRC_ERR_SOCKET, "CircServerGetRequest",
		"Couldn't get circ name");
      free (request->circname);
      close (fd);
      return -1;
    }

    request->circname[namelength] = '\0';

    if (request->reqtype == CIRC_SERVER_REQTYPE_OPEN || request->reqtype == CIRC_SERVER_REQTYPE_CONNECT)
    {
      if (CircSafeReceive (fd, (char *) &length, sizeof (length)) < (int) sizeof (length))
      {
	ErrorSet (CIRC_ERR_SOCKET, "CircServerGetRequest",
		  "Couldn't get circ length");
	free (request->circname);
	close (fd);
	return -1;
      }

      request->circsize = ntohl (length);
    }
  }

  return fd;
}

/***************************************************************************/
/* =i=> */
int CircServerExecuteRequest (int fd, struct circserver_request *request)
/*----------------------------------------------------
 * =p=>
 * Execute a request to open/close a buffer.
 * Resulting cid and error code are sent to the requestor.
 *  
 * Returns cid or -1 in case of error.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int cid = -1;
  int reqtype;
  uint32_t length, type;

  reqtype = request->reqtype;

  if (reqtype == CIRC_SERVER_REQTYPE_OPEN || reqtype == CIRC_SERVER_REQTYPE_CONNECT)
  {
    uint32_t netcid;
    int found = FALSE;
    char additional_info[512];

    struct buflist_elem *thebuf = buflist;

    /* Cannot open two buffers with the same name */

    pthread_mutex_lock (&clist_mutex_);

    while (thebuf)
    {
      if (!strcmp (thebuf->bufname, request->circname))
      {
	found = TRUE;
	break;
      }

      thebuf = thebuf->next;
    }

    if (!found)
      cid = CircOpen (NULL, request->circname, request->circsize);
    else
    {
      sprintf (additional_info, " Buffer %s exists ", request->circname);

      if (reqtype == CIRC_SERVER_REQTYPE_CONNECT)
	cid = thebuf->cid;
    }

    pthread_mutex_unlock (&clist_mutex_);

    netcid = htonl (cid);
 
    if (CircSafeSend (fd, (char *) &netcid,
		      sizeof (netcid)) < (int) sizeof (netcid))
    {
      if (!found && cid != -1)
	CircClose (cid);

      free (request->circname);
      close (fd);
      return -1;
    }

    if (cid < 0)
    {
      uint32_t netlen;
      uint32_t netcode = htonl (0);
      char errmessage[1024] = "Failed to open buffer ";

      strcat (errmessage, request->circname);
      strcat (errmessage, additional_info);

      ErrorSet (CIRC_ERR_OPENERR, "CircServerExecuteRequest", errmessage);

      /* Send error code and message */

      if (CircSafeSend (fd, (char *) &netcode,
			sizeof (netcode)) < (int) sizeof (netcode))
      {
	free (request->circname);
	close (fd);
	return -1;
      }

      length = strlen (errmessage);
      netlen = htonl (length);

      if (CircSafeSend (fd, (char *) &netlen,
			sizeof (netlen)) < (int) sizeof (netlen))
      {
	free (request->circname);
	close (fd);
	return -1;
      }

      if (CircSafeSend (fd, errmessage, length) < (int) length)
      {
	free (request->circname);
	close (fd);
	return -1;
      }
    }
    else
    {
      /* Add cid to the cid list */

      if (CircServerAddToList (&buflist, cid, request->circname) < 0)
      {
	ErrorSetF (CIRC_ERR_CIDALLOC, "CircServerExecuteRequest",
		   "Error adding buffer %s to buffer list",
		   request->circname);
	free (request->circname);
	close (fd);
	return -1;
      }
    }
  }
  else if (reqtype == CIRC_SERVER_REQTYPE_CLOSE)
  {
    struct buflist_elem *thebuf;

    /* Look for the buffer name in the buffer list */

    if ((cid = CircServerSearchList (buflist, request->circname)) < 0)
    {
      ErrorSetF (CIRC_ERR_OPENERR, "CircServerExecuteRequest",
		 "Buffer %s not found", request->circname);
      free (request->circname);
      close (fd);
      return -1;
    }

    pthread_mutex_lock (&clist_mutex_);

    thebuf = buflist;

    while (thebuf)
    {
      struct buflist_elem *lastbuf = thebuf->last;
      struct buflist_elem *nextbuf = thebuf->next;

      if (thebuf->cid == cid)
      {
	if (thebuf->connected > 1)
	  --thebuf->connected;
	else
	{
	  /* Close the buffer */

	  CircClose (cid);

	  /* Remove the buffer element from the list */

	  free (thebuf->bufname);
	  free (thebuf);

	  if (nextbuf)
	    nextbuf->last = lastbuf;

	  if (lastbuf)
	    lastbuf->next = nextbuf;
	  else
	    buflist = nextbuf;
	}
      }

      thebuf = nextbuf;
    }

    pthread_mutex_unlock (&clist_mutex_);
  }
  else
    ErrorSetF (CIRC_ERR_REQUEST, "CircServerExecuteRequest",
	       "Unrecognized request : %d", reqtype);

  /* Wait for connection closed on client's side */

  if (CircSafeReceive (fd, (char *) &type, sizeof (char)) != 0)
  {
    ErrorSet (0, "CircServerExecuteRequest",
	      "Extra unrecognized messages from client");
  }

  free (request->circname);
  close (fd);

  return cid;
}

/***************************************************************************/
/* =i=> */
int CircServerServeRequest ()
/*----------------------------------------------------
 * =p=>
 * Accept a connection and serve a request to open/close a buffer.
 * Resulting cid and error code are sent to the requestor.
 *  
 * Returns cid or -1 in case of error.
 * ===<
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int fd;
  int cid = -1;

  struct circserver_request req;

  if ((fd = CircServerGetRequest (&req)) > 0)
    cid = CircServerExecuteRequest (fd, &req);

  return cid;
}

/***************************************************************************/
/* =i=> */
int CircServerEnd ()
/*----------------------------------------------------
 * =p=>
 * End the Circular buffer server.
 *  
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  CircServerCleanAll ();
  close (fd0);

  /* Destroy the global mutex */

  pthread_mutex_destroy (&clist_mutex_);

  return (Error_NO);
}

/***************************************************************************/
/* =i=> */
int CircClientOpenConnection (char *server, unsigned short port)
/*-------------------------------------------------------------------------
 * =p=>
 * Open a TCP/IP connection to the server.
 *
 *  server :  host name or address of the server. If NULL, the default is used.
 *  port :    port on which the server is listening. If 0, the default is used.
 *  
 *  Return value:
 *   socket id or -1 in case of failure.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int s;
  int option = 1;
  uint32_t server_addr;

  struct sockaddr_in saddr;
  struct hostent *h_address;

  char default_hostname[] = CIRC_SERVER_DEFAULT_HOSTNAME;

  if (server == (char *) NULL)
    server = default_hostname;

  if (port == 0)
    port = CIRC_SERVER_DEFAULT_PORT;

  if ((s = socket (PF_INET, SOCK_STREAM, 0)) < 0)
  {
    ErrorSetSystem (CIRC_ERR_SOCKET, "CircClientOpenConnection", "Couldn't open socket");
    return -1;
  }

  if (setsockopt (s, SOL_TCP, TCP_NODELAY, &option, sizeof (option)) < 0)
  {
    ErrorSetSystem (CIRC_ERR_SOCKET, "CircClientOpenConnection", "Couldn't set TCP_NODELAY");
    close (s);
    return -1;
  }

  if (isalpha ((int) server[0]))
  {
    if ((h_address = gethostbyname (server)) == (struct hostent *) NULL)
    {
      ErrorSetSystemF (CIRC_ERR_SOCKET, "CircClientOpenConnection",
		       "Couldn't get server's address: %s", server);
      close (s);
      return (Error_YES);
    }

    memcpy ((char *) &server_addr, h_address->h_addr, h_address->h_length);
    saddr.sin_family      = h_address->h_addrtype;
    saddr.sin_addr.s_addr = server_addr;
  }
  else
  {
    saddr.sin_family      = AF_INET;
    saddr.sin_addr.s_addr = inet_addr (server);
  }

  saddr.sin_port        = htons (port);

  if (connect (s, (struct sockaddr *) &saddr, sizeof (saddr)) < 0)
  {
    ErrorSetSystemF (CIRC_ERR_SOCKET, "CircClientOpenConnection",
		     "Couldn't connect to server %s", server);
    close (s);
    return -1;
  }

  return s;
}

/***************************************************************************/
/* =i=> */
int CircClientCloseConnection (int s)
/*-------------------------------------------------------------------------
 * =p=>
 * Close a TCP/IP connection to the server.
 *
 *  s : socket id.
 *  
 *  Return value:
 *   Error_NO;
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  close (s);

  return (Error_NO);
}

/***************************************************************************/
/* =i=> */
int CircSendRequest (int reqtype, char *server,
		     unsigned short port, char *keyname, int size)
/*----------------------------------------------------
 * =p=>
 * Asks a buffer server to open a circular buffer in master or slave mode
 * or to close it according to the request type.
 * The handshaking protocol uses a TCP/IP connection. Error codes are
 * received via the open connection.
 *
 *  reqtype : request type (CIRC_SERVER_REQTYPE_OPEN, CIRC_SERVER_REQTYPE_CONNECT
 *            or CIRC_SERVER_REQTYPE_CLOSE)
 *  server :  host name or address of the server. If NULL, the default is used.
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpen.
 *  size :    buffer size in bytes as in CircOpen.
 *  
 *  Return value:
 *   Error_YES in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int freename = 0;
  int sock, cid, msglen;
  uint32_t nlen, namelength;
  uint32_t circlen = htonl (size), rtype = htonl (reqtype);
  uint32_t c;
  char *message;

  if (server == (char *) NULL)
  {
    if ((server = (char *) malloc (strlen (CIRC_SERVER_DEFAULT_HOSTNAME) + 1)) == (char *) NULL)
    {
      ErrorSetSystem (CIRC_ERR_MALLOC, "CircClientRequestOpen",
		      "Memory allocation failed");
      return (Error_YES);
    }

    strcpy (server, CIRC_SERVER_DEFAULT_HOSTNAME);
    freename = 1;
  }

  if ((sock = CircClientOpenConnection (server, port)) < 0)
  {
    if (freename) free (server);
    return (Error_YES);
  }

  nlen = strlen (keyname);
  namelength = htonl (nlen);

  if (CircSafeSend (sock, (char *) &rtype,
		    sizeof (rtype)) < (int) sizeof (rtype))
  {
    CircClientCloseConnection (sock);
    if (freename) free (server);
    return (Error_YES);
  }

  if (CircSafeSend (sock, (char *) &namelength,
		    sizeof (namelength)) < (int) sizeof (namelength))
  {
    CircClientCloseConnection (sock);
    if (freename) free (server);
    return (Error_YES);
  }

  if (CircSafeSend (sock, keyname, nlen) < (int) nlen)
  {
    CircClientCloseConnection (sock);
    if (freename) free (server);
    return (Error_YES);
  }

  if (reqtype == CIRC_SERVER_REQTYPE_OPEN || reqtype == CIRC_SERVER_REQTYPE_CONNECT)
  {
    if (CircSafeSend (sock, (char *) &circlen,
		      sizeof (circlen)) < (int) sizeof (circlen))
    {
      CircClientCloseConnection (sock);
      if (freename) free (server);
      return (Error_YES);
    }

    if (CircSafeReceive (sock, (char *) &c, sizeof (c)) < (int) sizeof (c))
    {
      ErrorSetSystemF (CIRC_ERR_SOCKET, "CircClientRequestOpen",
		       "Couldn't get cid from server %s", server);
      CircClientCloseConnection (sock);
      if (freename) free (server);
      return (Error_YES);
    }

    cid = ntohl (c);

    if (cid < 0)
    {
      int errcode;

      /* id of remote buffer < 0 : receive error code and message if available */

      ErrorSetF (CIRC_ERR_OPENERR, "CircClientRequestOpen",
		 "Server %s failed to open buffer %s", server, keyname);

      if (CircSafeReceive (sock, (char *) &c, sizeof (c)) < (int) sizeof (c))
      {
	ErrorSetSystemF (CIRC_ERR_SOCKET, "CircClientRequestOpen",
			 "Failed to receive error code from %s", server);
	CircClientCloseConnection (sock);
	if (freename) free (server);
	return (Error_YES);
      }

      errcode = ntohl (c);

      if (CircSafeReceive (sock, (char *) &c, sizeof (c)) < (int) sizeof (c))
      {
	ErrorSetSystemF (CIRC_ERR_SOCKET, "CircClientRequestOpen",
			 "Failed to receive error message from %s", server);
	CircClientCloseConnection (sock);
	if (freename) free (server);
	return (Error_YES);
      }

      msglen = ntohl (c);

      if ((message = (char *) malloc (msglen + 1)) == (char *) NULL)
      {
	ErrorSetSystem (CIRC_ERR_MALLOC, "CircClientRequestOpen",
			"Memory allocation failed");
	CircClientCloseConnection (sock);
	if (freename) free (server);
	return (Error_YES);
      }

      if (CircSafeReceive (sock, message, msglen) < msglen)
      {
	ErrorSetSystemF (CIRC_ERR_SOCKET, "CircClientRequestOpen",
			 "Failed to receive error message from %s", server);
      }
      message[msglen] = '\0';

      ErrorSetF (errcode, "CircClientRequestOpen",
		 "Circ server returned message : %s", message);

      free (message);

      CircClientCloseConnection (sock);
      if (freename) free (server);
      return (Error_YES);
    }
  }

  CircClientCloseConnection (sock);
  if (freename) free (server);
  return (Error_NO);
}

/***************************************************************************/
/* =i=> */
int CircRequestOpen (char *server, unsigned short port, char *keyname, int size)
/*----------------------------------------------------
 * =p=>
 * Asks a buffer server to open a circular buffer in master or slave mode.
 *
 *  server :  host name or address of the server. If NULL, the default is used.
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpen.
 *  size :    buffer size in bytes as in CircOpen.
 *  
 *  Return value:
 *   Error_YES in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  return CircSendRequest (CIRC_SERVER_REQTYPE_OPEN, server,
			  port, keyname, size);
}

/***************************************************************************/
/* =i=> */
int CircRequestConnect (char *server, unsigned short port, char *keyname, int size)
/*----------------------------------------------------
 * =p=>
 * Asks a buffer server to open or connect to a circular buffer in master or slave mode.
 *
 *  server :  host name or address of the server. If NULL, the default is used.
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpen.
 *  size :    buffer size in bytes as in CircOpen.
 *  
 *  Return value:
 *   Error_YES in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  return CircSendRequest (CIRC_SERVER_REQTYPE_CONNECT, server,
			  port, keyname, size);
}

/***************************************************************************/
/* =i=> */
int CircRequestClose (char *server, unsigned short port, char *keyname)
/*----------------------------------------------------
 * =p=>
 * Asks a buffer server to close a circular buffer.
 *
 *  server :  host name or address of the server. If NULL, the default is used.
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpen.
 *  
 *  Return value:
 *   Error_YES in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  return CircSendRequest (CIRC_SERVER_REQTYPE_CLOSE, server,
			  port, keyname, 0);
}

/***************************************************************************/
/* =i=> */
int CircOpenCircConnection (unsigned short port, char *keyname, int size)
/*----------------------------------------------------
 * =p=>
 * Asks a buffer server to open a circular buffer in master mode
 * and connects to it. Only works on local host.
 *
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpen.
 *  size :    buffer size in bytes as in CircOpen.
 *  
 *  Return value:
 *   Buffer identifier or -1 in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int cid;

  if (size <= 0)
  {
    ErrorSetF (CIRC_ERR_SOCKET, "CircOpenCircConnection",
	       "Wrong size parameter for remote master %d", size);
    return -1;
  }

  if (CircRequestOpen ((char *) NULL, port, keyname, size) == Error_YES)
    return -1;

  if ((cid = CircOpen ((char *) NULL, keyname, 0)) < 0)
  {
    CircRequestClose ((char *) NULL, port, keyname);
    return -1;
  }

  return cid;
}

/***************************************************************************/
/* =i=> */
int CircOpenCircConnection_t (unsigned short port, char *keyname, int size)
/*----------------------------------------------------
 * =p=>
 * Asks a buffer server to open a circular buffer in master mode
 * and connects to it. Same as CircOpenCircConnection, but works for multi-threaded
 * client writers, provided the use CircPut_t() or CircReserve_t() and CircValidate_t().
 * Only works on local host.
 *
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpen.
 *  size :    buffer size in bytes as in CircOpen.
 *  
 *  Return value:
 *   Buffer identifier or -1 in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int cid;

  if (size <= 0)
  {
    ErrorSetF (CIRC_ERR_SOCKET, "CircOpenCircConnection_t",
	       "Wrong size parameter for remote master %d", size);
    return -1;
  }

  if (CircRequestConnect ((char *) NULL, port, keyname, size) == Error_YES)
    return -1;

  if ((cid = CircServerSearchList (clientlist, keyname)) < 0)
  {
    if ((cid = CircOpen_t ((char *) NULL, keyname, 0)) < 0)
    {
      CircRequestClose ((char *) NULL, port, keyname);
      return -1;
    }
  }

  if (CircServerAddToList (&clientlist, cid, keyname) < 0)
  {
    CircClose_t (cid);
    CircRequestClose ((char *) NULL, port, keyname);
    return -1;
  }

  return cid;
}

/***************************************************************************/
/* =i=> */
int CircCloseCircConnection (unsigned short port, char *keyname, int cid)
/*----------------------------------------------------
 * =p=>
 * Close a buffer in client mode and 
 * asks a buffer server to close it in master mode.
 * Only works on local host.
 *
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpenCircConnection.
 *  cid :     buffer id as returned by CircOpenCircConnection.
 *  
 *  Return value:
 *   Error_YES in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  CircClose (cid);
  CircRequestClose ((char *) NULL, port, keyname);

  return (Error_NO);
}

/***************************************************************************/
/* =i=> */
int CircCloseCircConnection_t (unsigned short port, char *keyname, int cid)
/*----------------------------------------------------
 * =p=>
 * Close a buffer in client mode and 
 * asks a buffer server to close it in master mode.
 * To be used when the open request has been done via CircOpenCircConnection_t().
 * Only works on local host.
 *
 *  port :    port on which the server is listening. If 0, the default is used.
 *  keyname : name of the buffer as in CircOpenCircConnection_t.
 *  cid :     buffer id as returned by CircOpenCircConnection_t.
 *  
 *  Return value:
 *   Error_YES in case of error.
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int nconn;

  nconn = CircServerGetInstances (clientlist, keyname);

  if (nconn == 1)
    CircClose_t (cid);

  CircRequestClose ((char *) NULL, port, keyname);
  CircServerRemoveFromList (&clientlist, cid);

  return (Error_NO);
}

#endif
