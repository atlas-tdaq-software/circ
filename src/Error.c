/***************************************************************************/
/* Error.c
 * =h=> Error, a simple error handling library
 * =p=>
 * This library comprises a set of easy to use functions for 
 * simple and flexible error handling. It should mainly allow:
 * 
 *   => creation & formatting of error messages with one function.
 *   => error number handling (custom, fatal, warning etc).
 *   => functions to get number and message of the most recent error. 
 *   => redirection of the error message output stream also to a
 *      log file or a dialog window.
 *
 * Note: some functionality is not yet implemented but this can
 *       be done later without changing the error set routines.
 * 
 * The error handling here is similar to the standard UNIX one e.g.
 * the error message and number is only valid after an error status
 * was detected. By default the message is printed to standard output.
 *
 * Note: the maximum lenght of an error message is Error_MESSAGE_SIZE
 *       (currently 1024 bytes).
 *
 * The error handling functions are more or less self explanatory.
 * Have a look to the hyperlinked example and test file =e=> ErrorTest.c
 * and run this program: $COMMONBIN/ErrorTest.
 * ===<
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include "circ/Error.h"
#include "circ/Error_protos.h"
#include "circ/Error_private.h"

/***************************************************************************/

/* This one will be imported from the Error_threads_xx module */
void ErrorGetRec (ErrorRec **rec); /* OUT */

int ErrorPrintMessage (ErrorRec *rec,
		       const char *header);

/***************************************************************************/
/* Public functions */
/***************************************************************************/

/***************************************************************************/
/* =i=> */
void ErrorClear (void)
/*--------------------
 * =p=>
 * resets (cleans) the error message and number
 * ===<
 */
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  rec->number = Error_UNKNOWN;
  strcpy(rec->message, "no message");
}
/***************************************************************************/
/* =i=> */
void ErrorInit (int mode)
/*-----------------------
 * =p=>
 * resets the error message and the print mode, see also 
 * ErrorSetPrintMode(). Normally not needed. 
 * ===<
 */
{
/*.........................................................................*/
  ErrorClear ();
  ErrorSetPrintMode (mode);
}

/***************************************************************************/
/* =i=> */
void ErrorSet (int number, const char* header, const char* message)
/*-----------------------------------------------------
 * =p=>
 * sets an error number, header and unformatted error message.
 * The header is usually the function name in which the
 * error occured.
 *
 * Example: ErrorSet(0,"MyFunc","something went wrong");  
 * ===<
 */
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  if ((message-(rec->message))<Error_MESSAGE_SIZE)
    { /* message was obtained with ErrorGetMessage() */
      int mlen = strlen(message)+1;
      char *tmp;

      tmp = (char*) malloc(mlen);
      if (tmp==NULL)
	{
	  sprintf (rec->message,"%s: Internal error of Error! (Malloc(%i) failed)",
		   header,mlen);
	}
      else
	{
	  memcpy(tmp,message,mlen);
	  sprintf (rec->message,"%s: %s", header,tmp); 
	  free(tmp);
	}
    }
  else
    sprintf (rec->message,"%s: %s", header,message); 

  rec->number = number;
  rec->count++;
  if (rec->mode&Error_PRINT_AT_SET)
    (void)ErrorPrintMessage(rec,"");
}
/***************************************************************************/
/* =i=> */
void ErrorSetF (int number, const char* header, const char* format, ...)
/*----------------------------------------------------------
 * =p=>
 * sets an error number, header and formatted error message. 
 * The header is usually the function name in which the error occured.
 * The formatted message can be constructed like with printf().
 *
 * Example: ErrorSetF(0,"MyFunc","name %s unknown",name);  
 * ===<
*/
{
  va_list args;
  char msg[Error_MESSAGE_SIZE - Error_HEADER_SIZE];
  ErrorRec *rec;
  ErrorGetRec(&rec);
/*.........................................................................*/
  va_start (args,format);
  vsprintf (msg, format, args);
  va_end (args);
  sprintf (rec->message,"%s: %s", header, msg); 
  rec->number = number;
  rec->count++;
  if (rec->mode&Error_PRINT_AT_SET)
    (void)ErrorPrintMessage(rec,"");
}
/***************************************************************************/
/* =i=> */
void ErrorSetSystem (int number, const char* header, const char* message)
/*-----------------------------------------------------------
 * =p=>
 * sets an error number, header and unformatted error message including
 * the system error message (like with perror).
 * The header is usually the function name in which the error occured. 
 *
 * Example: ErrorSetSystem(0, "MyFunc", "open failed");  
 * ===<
 */
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  sprintf(rec->message,"%s: %s: %s",header,message,strerror(errno));
  rec->number = number;
  rec->number |= errno | Error_SYSTEM;
  rec->count++;
  if (rec->mode&Error_PRINT_AT_SET)
    (void)ErrorPrintMessage(rec,"");
}
/***************************************************************************/
/* =i=> */
void ErrorSetSystemF (int number, const char* header, const char* format, ...)
/*----------------------------------------------------------------
 * =p=>
 * sets an error number, header and formatted error message including
 * the system error message (like with perror).
 * The header is usually the function name in which the error occured. 
 * The formatted message can be constructed like with printf().
 *
 * Example: ErrorSetSystem(0, "MyFunc", "can not open %s", name);  
 * ===<
*/
{
  char msg[Error_MESSAGE_SIZE - Error_HEADER_SIZE];
  va_list args;
  ErrorRec *rec;
  ErrorGetRec(&rec);
/*........ .................................................................*/
  va_start (args,format);
  vsprintf (msg, format, args);
  va_end   (args);
  sprintf  (rec->message,"%s: %s: %s",header, msg, strerror(errno)); 
  rec->number = number;
  rec->number |= errno | Error_SYSTEM;
  rec->count++;
  if (rec->mode&Error_PRINT_AT_SET)
    (void)ErrorPrintMessage(rec,"");
}
/***************************************************************************/
/* =i=> */
void ErrorSetPrintMode (int mode)
/*-------------------------------
 * =p=>
 * allows to modifiy the print mode. By default all messages are
 * print to standard output. Possible modes for the moment are:
 *
 * mode = Error_PRINT_OFF     : no print out of the error message.
 *        Error_PRINT_AT_SET  : print when ErrorSetXxx() is called.
 *        Error_PRINT_AT_GET  : print when ErrorGetXxx() is called.
 *        Error_PRINT_DEFAULT : = Error_PRINT_AT_SET; 
 * ===<
 */
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  if ( (mode & Error_PRINT_OFF) == Error_PRINT_OFF ||
       (mode & Error_PRINT_AT_SET) == Error_PRINT_AT_SET || 
       (mode & Error_PRINT_AT_GET) == Error_PRINT_AT_GET)
    rec->mode = mode;
  else
    rec->mode = Error_PRINT_DEFAULT;
}
/***************************************************************************/
/* =i=> */
int ErrorGetPrintMode (void)
/*--------------------------
 * =p=>
 * returns current print mode, see also ErrorSetPrintMode().
 * ===<
 */
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  return (rec->mode);
}
/***************************************************************************/
/* =i=> */
int ErrorGetNumber (void)
/*-----------------------
 * =p=>
 * returns the error number of the most previous error.
 * ===<
 */
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  if (rec->mode&Error_PRINT_AT_GET)
    (void)ErrorPrintMessage(rec,"");
  return (rec->number);
}
/***************************************************************************/
/* =i=> */
int ErrorGetCount (void)
/*----------------------
 * =p=>
 * returns the current error count.
 * ===<
*/
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  return (rec->count);
}
/***************************************************************************/
/* =i=> */
char *ErrorGetMessage (void)
/*--------------------------
 * =p=>
 * returns the error message of the most previous error.
 * ===<
*/
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  if (rec->mode&Error_PRINT_AT_GET)
    (void)ErrorPrintMessage(rec,"");
  return(rec->message);
}
/***************************************************************************/
/* =i=> */
int ErrorCopyMessage (char *destination)
/*--------------------------------------
 * =p=>
 * returns the error number and copies the error message of the 
 * most previous error into <destination>.
 * ===<
*/
{
/*.........................................................................*/
  ErrorRec *rec;
  ErrorGetRec(&rec);

  strcpy(destination,rec->message);
  if (rec->mode&Error_PRINT_AT_GET)
    (void)ErrorPrintMessage(rec,"");
  return(rec->number);
}  

