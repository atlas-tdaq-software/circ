/***************************************************************************/
/* Circ.c 
 *
 * =h=> Circular buffer application interface
 * =p=>
 * The idea and the first implementation of the circular buffer comes from
 * Harry Beker. It allows high speed event buffering and passing between 
 * processes or threads using a simple reserve/validate and locate/release
 * mechanism. Event data can be prepared and processed in place and hence
 * avoiding unnecessary data copies.
 * 
 * Modifications have been applied to improve error handling, POSIX and thread
 * support and documentation. Furthermore there are now also special functions
 * to ease parallel processing especially for event building.
 *
 * The circular buffer supports the following interfaces for inter-process-
 * communication (IPC).
 *
 *  1. SYSV shared memory and semaphore.
 *  2. POSIX (P1003.1b) shared memory and semaphore.
 *  3. POSIX threads and mutex function.
 *
 * For the inter-thread version the POSIX mutex calls are used to make
 * atomic access to the circular buffer header instead of POSIX or SYSV
 * semaphores for the inter-process version.
 *
 * The message system implemented inside KLOE DAQ uses interrupts for
 * signaling new command arrival. This can interrupt a semaphore allocation,
 * so the interupts are forced to be ignored during the SETSEMA operation.
 * To enable interupts, set CircSemIgnoreIntr to 0 at run-time.
 * (CircSemIgnoreIntr is 1 per default)
 *
 * It is also possible to use a timeout while waiting for a semaphore. That allows not
 * to block writers in case of backpressure or killed readers.
 *
 * Have a look to the example and test programs:
 *    =e=> Circread.c   and 
 *    =e=> Circwrite.c  for inter-process
 *    =e=> Circthread.c for inter-threads.
 *
 * Note: There are FORTRAN bindings as well (inter-process version only)!
 * ===<
 * Modification history:
 * some time ago: original idea and implementation from Harry Beker
 *                (see files circular.c and circular.h).
 *  4/ 6/96 WG  : application interface to simplify circular buffer usage.  
 * 20/12/96 WG  : code review and restructuring to support POSIX stuff. 
 * 21/12/96 WG  : multiple open and locate for parallel processing.
 * 06/01/97 WG  : now full function names, old short names as macros.
 * 05/11/98 IS  : Conditional validating
 *                The semantics of GetCircContents has slightly changed.
 * 10/11/98 IS  : Magic number is set to 0 during the master close
 * 09/12/98 IS  : Some statistics added
 * 01/04/00 EP  : Code checked on linux
 * 05/12/06 EP  : Fortran bindings reviewed
 * 16/06/08 EP  : Master process pid added to buffer management structure
 * 17/06/08 EP  : CircGetMasterControl added
 * 06/10/08 EP  : Directory in keyname is created if it does not exist
 * 06/10/08 EP  : CircGetMasterControl removed
 * 11/05/12 EP  : CircRevalidate added
 * 02/02/20 EP  : CircOpen_t, CircClose_t and CircPut_t added to support threaded writers
 *                to the same Circ.
 *                Blocking by mutex serializes the operation, so performance is degraded.
 *                Using Multi buffer is a better solution when possible.
 */
/***************************************************************************/
/*
#define DEBUG_ON
*/
/***************************************************************************/
/* Public functions                                                        */
/***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>

#include "circ/Error.h"

#include "circ/Circ.h"
#include "circ/Circ_params.h"
#include "circ/Circ_private.h"

static int channels_allocated = 0;
static struct CircBuffer *cb  = NULL;

static pthread_mutex_t access_mutex_;

#define RETURN_IF_CID_INVALID(cid,where,return_code) \
  if (cid != cb[cid].cid || cb[cid].shm_size == -1) { \
    ErrorSetF(CIRC_ERR_INVALID,where,"circular buffer id %d is invalid",cid); \
    return(return_code); }

#define ALIGN_TO sizeof(long) /* bytes */

/* if CircSemIgnoreIntr true, SETSEMA will ignore the EINTR signal */
/* This is the default */
/* It is also possible to set a timeout for SETSEMA */

int CircSemIgnoreIntr = 1;
int CircSemTimeout = 300;

/***************************************************************************/
/* Public functions                                                        */
/***************************************************************************/

/**
 * Opens a circular buffer for master (size>0) or slave (size=0) process
 * access. The master process actually creates, initializes and removes the
 * shared memory and semaphore object used. The slave process just maps to
 * an already existing circular buffer  
 *  
 *  allocate: Can be used to pass a pointer of a preallocated buffer. 
 *   If allocate=NULL CircOpen() of CircOpenMulti() will do the appropriate
 *   allocation e.g. creating shared memory or doing a malloc() for threads.
 *
 *  keyname : Key name to identify the associated shared memory segment
 *   and semaphores for the inter-process-communication. Actually two files 
 *   <keyname>._CIRC_SHM and <keyname>._CIRC_SEM are automatically created
 *   and removed by the master process at CircOpen() and CircClose() time.
 *   If <keyname> contains a directory name and the directory does not
 *   exist, it is created.
 *            
 *  size: Total size of the circular buffer in bytes. Only if size>0 the 
 *   appropriate space is allocated and setup and only the master process
 *   should do that once. Slave processes should call CircOpen() with 
 *   size=0 to map to an existing circular.
 *  
 *  Return values:
 *   >= 0 : circular buffer identifier.
 *   <  0 : open failed.
 */

int CircOpen (char *allocate, char *keyname, int size)
{
  int cid;
  char *slash;

  /* Check for the directory where the key files are created */

  if ((slash = strrchr (keyname, '/')) != (char *) NULL)
  {
    DIR *d;
    char *dirname;
    int dirlen = slash - keyname;

    if ((dirname = (char *) malloc (dirlen + 1)) == (char *) NULL)
      return (Error_YES);

    strncpy (dirname, keyname, dirlen);
    dirname[dirlen] = '\0';

    if ((d = opendir (dirname)) == (DIR *) NULL)
    {
      int mode = 0777;
      if (mkdir (dirname, mode) < 0)
	return (Error_YES);
    }
    else
      closedir (d);

    free (dirname);
  }

  if ((cid = CircGetId()) == Error_YES)
    return (Error_YES);

  cb[cid].cid      = cid;
  cb[cid].master   = size > 0 ? getpid () : 0;

  /* Allocate space for circular buffer if not already done.*/

  if (allocate == NULL)
    {
      if (CircShmCreate(&cb[cid], keyname, size) == Error_YES)
	return (Error_YES);
    }
  else
    cb[cid].shm_desc  = (struct CircDesc*)allocate;

  cb[cid].shm_size = size;

  /* Open semaphore needed for synchronisation. 
   * Note threads need the address of the semaphore location!
   */

  if (CircSemCreate(&cb[cid], keyname, size) == Error_YES)
    return (Error_YES);

  DEBUG(("CircOpen> cid=%d master=%d ptr=0x%x size=%d shmid=0x%x semid=0x%x\n",
	 cb[cid].cid, cb[cid].master, (unsigned int) cb[cid].shm_desc,
	 cb[cid].shm_size, cb[cid].shm_id, cb[cid].sem_id));

  if (size > 0)	 /* only if we are the master */
    {
      if (CircInit (&cb[cid], 0) == Error_YES)
	return (Error_YES);
    }

  DEBUG(("CircOpen> head=0x%x tail=0x%x\n",
         (cb[cid].shm_desc)->head, (cb[cid].shm_desc)->tail));

  return (cid);
}

/**
 * Same as CircOpen(), but to be used by multi-threaded writers
 * accessing the same buffer.
 * Opens a circular buffer for master (size>0) or slave (size=0) process
 * access. The master process actually creates, initializes and removes the
 * shared memory and semaphore object used. The slave process just maps to
 * an already existing circular buffer  
 *  
 *  allocate: Can be used to pass a pointer of a preallocated buffer. 
 *   If allocate=NULL CircOpen() of CircOpenMulti() will do the appropriate
 *   allocation e.g. creating shared memory or doing a malloc() for threads.
 *
 *  keyname : Key name to identify the associated shared memory segment
 *   and semaphores for the inter-process-communication. Actually two files 
 *   <keyname>._CIRC_SHM and <keyname>._CIRC_SEM are automatically created
 *   and removed by the master process at CircOpen() and CircClose() time.
 *   If <keyname> contains a directory name and the directory does not
 *   exist, it is created.
 *            
 *  size: Total size of the circular buffer in bytes. Only if size>0 the 
 *   appropriate space is allocated and setup and only the master process
 *   should do that once. Slave processes should call CircOpen() with 
 *   size=0 to map to an existing circular.
 *  
 *  Return values:
 *   >= 0 : circular buffer identifier.
 *   <  0 : open failed.
 */

int CircOpen_t (char *allocate, char *keyname, int size)
{
  int cid;

  pthread_mutex_init (&access_mutex_, 0);

  if ((cid = CircOpen (allocate, keyname, size)) < 0)
    pthread_mutex_destroy (&access_mutex_);

  return cid;
}

/**
 * The same as CircOpen() but allows to open and maintain a specified number
 * of circular buffers using only one piece of shared memory and semaphore.
 * This is especially useful for parallel tasks like event building. It 
 * simplifies the management of the circular buffers but for small events 
 * normal CircOpen() with it's associated semaphore might be faster. 
 *
 * The first three arguments are the same as for CircOpen(). <cid_amount> 
 * circular buffers will be opened and the identifiers are returned in 
 * <cid_array>. If size=0 <cid_amount> is treated as maximum value and 
 * should be appropriate for the size of array <cid_array>.
 * 
 * Note: <size> is the size to be allocated for ALL circular buffers. 
 *       CircCloseMulti() must be used for close.
 *
 * Return values:
 *  > 0 : number of circular buffers opened or found.
 *  < 0 : open failed.
 */

int CircOpenMulti (char* allocate, char* keyname, int size, 
		   int cid_amount, int* cid_array)
{
  char *shmptr;
  int   cid, cid_first;
  int   size_each, i;
/*.........................................................................*/
  
  DEBUG(("CircOpenMulti> IN: allocate=0x%x keyname=%s size=%d cid_amount=%d\n",
	 (unsigned int) allocate, keyname, size, cid_amount)); 

  if (size > 0 && cid_amount < 1)
    {
      ErrorSetF(CIRC_ERR_INVALID,"CircOpenMulti","invalid amount of %d buffer(s) requested",
		cid_amount);
      return (Error_YES);
    }
  
  /* we can do a normal open for the first one
   */

  cid_first = CircOpen(allocate, keyname, size);
  if (cid_first < 0)
    {
      ErrorSetF(CIRC_ERR_INVALID,"CircOpenMulti","can not open the first buffer out of %d",
		cid_amount);
      return (Error_YES);
    }

  shmptr       = (char*)cb[cid_first].shm_desc;
  cid_array[0] = cid_first;

  if (size > 0) /* Master: create and setup the remaining buffers */
    {
      if (cid_amount > 1) /* more than one requested */
	{
	  size_each  = size / cid_amount;
	  size_each &= ~(ALIGN_TO -1); /* align on long word boundary */
	  /* correct the first one */
	  cb[cid_first].shm_size = size_each;
	  if (CircInit(&cb[cid_first], cid_amount-1) == Error_YES)
	    return (Error_YES);
	  DEBUG(("CircOpenMulti> size for %d buffer(s) = %d, per buffer = %d\n",
		 cid_amount, size, size_each));
	  for (i = 1; i < cid_amount; i++)
	    {
	      shmptr += size_each; 
	      if ((cid = CircGetId()) < 0)
		return (Error_YES);
	      cb[cid].cid        = cid;
	      cb[cid].master     = getpid ();
	      cb[cid].shm_size   = size_each;
	      cb[cid].shm_desc   = (struct CircDesc*)shmptr;
	      cb[cid].sem_id     = cb[cid_first].sem_id;
	      cb[cid].shm_id     = cb[cid_first].shm_id;
	      if (CircInit(&cb[cid], cid_amount-i-1) == Error_YES)
		return (Error_YES);
	      cid_array[i] = cid;
	    }
	}
    }
  else /* Slave: find out how much buffers there are */
    {
      struct CircDesc* d = (struct CircDesc*)cb[cid_first].shm_desc;  
      if (d->magic != CIRC_HEADER_MAGIC)
	{
	  ErrorSetF(CIRC_ERR_HEADER,"CircOpenMulti",
		    "circular buffer head invalid: magic=0x%x !=0x%x",
		    d->magic, CIRC_HEADER_MAGIC);
	  return(Error_YES);
	}
      i = d->multi + 1;
      if (i > cid_amount)
	{
	  ErrorSetF(CIRC_ERR_MULTI_FIT,"CircOpenMulti",
		    "%d circular buffer id do not fit in cid_array[%d]",
		    i, cid_amount);
	  return(Error_YES);
	}
      cid_amount = i;
      size_each  = d->size;
      DEBUG(("CircOpenMulti> found %d buffer(s) with size %d each.\n",
	     cid_amount, size_each));
      
      for (i = 1; i < cid_amount; i++)
	{
	  d = (struct CircDesc*)((char*)d + d->size);
	  if (d->size != size_each)
	    {
	      ErrorSetF(CIRC_ERR_HEADER,"CircOpenMulti",
			"circular buffer head invalid: size=%d !=%d",
			d->size, size_each);
	      return(Error_YES);
	    }
	  if (d->magic != CIRC_HEADER_MAGIC)
	    {
	      ErrorSetF(CIRC_ERR_HEADER,"CircOpenMulti",
			"circular buffer head invalid: magic=0x%x !=0x%x",
			d->magic, CIRC_HEADER_MAGIC);
	      return(Error_YES);
	    }
	  if (d->multi != (cid_amount-i-1))
	    {
	      ErrorSetF(CIRC_ERR_HEADER,"CircOpenMulti",
			"circular buffer head invalid: multi=%d for buffer %d",
			d->multi, i);
	      return(Error_YES);
	    }
	  shmptr += size_each; 
	  if ((cid = CircGetId()) < 0)
	    return (Error_YES);
	  cb[cid].cid        = cid;
	  cb[cid].master     = 0;
	  cb[cid].shm_desc   = (struct CircDesc*)shmptr;
	  cb[cid].sem_id     = cb[cid_first].sem_id;
	  cb[cid].shm_id     = cb[cid_first].shm_id;
	  cid_array[i]       = cid;
	}
    }
  return (cid_amount);
}

/**
 * Closes the specified multiple circular buffer. 
 *
 * Note: Only the master of the circular buffer will remove the shared 
 *       memory and semaphore. 
 *
 *  cid: circular buffer id returned by CircOpen().
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircClose (int cid)
{
  int status = Error_NO;
/*.........................................................................*/

  RETURN_IF_CID_INVALID (cid, "CircClose", Error_YES);

  if (cb[cid].shm_size>0) cb[cid].shm_desc->magic = 0; /* only if master */

  status  = CircSemDestroy(&cb[cid]);
  status |= CircShmDestroy(&cb[cid]);

  cb[cid].cid      = -1; 
  cb[cid].shm_desc = NULL;
  cb[cid].shm_id   = -1;
  
  return (status);
}

/**
 * Same as CircClose(); to be used by multi_threaded accessing the same buffer
 * which called CircOpen_t().
 * Closes the specified multiple circular buffer. 
 *
 * Note: Only the master of the circular buffer will remove the shared 
 *       memory and semaphore. 
 *
 *  cid: circular buffer id returned by CircOpen_t().
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircClose_t (int cid)
{
  int ret = Error_NO;

  if ((ret = CircClose (cid)) != Error_NO)
    return ret;

  pthread_mutex_destroy (&access_mutex_);
  return ret;
}

/**
 * Closes the specified multiple circular buffer. 
 *
 * Note: Only the master of the circular buffer will remove shared 
 *       memory and semaphore. 
 *
 *  cid_amount and cid_array: number and array of circular buffers
 *       returned by CircOpenMulti().
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircCloseMulti (int cid_amount, int* cid_array)
{
  int i, cid;
  int status = Error_NO;
  struct CircDesc* d;
/*.........................................................................*/

  d = (struct CircDesc*)cb[cid_array[0]].shm_desc;  
  if (d->multi != (cid_amount-1))
    {
      ErrorSetF(CIRC_ERR_CLOSEERR,"CircCloseMulti", "can not close %d buffer(s) of %d found",
		cid_amount, d->multi+1);
      cid_amount = d->multi+1; /* correct */
      status     = Error_YES;
    }

  for (i = 1; i < cid_amount; i++) 
    {
      cid = cid_array[i];

      RETURN_IF_CID_INVALID (cid, "CircCloseMulti", Error_YES);
      cb[cid].cid      = -1; 
      if (cb[cid].shm_size>0) cb[cid].shm_desc->magic = 0; /* only if master */
      cb[cid].shm_desc = NULL;
      cb[cid].shm_id   = -1;
    }

  status |= CircClose(cid_array[0]);

  return (status);
}

/**
 * Resets the specified circular buffer. Note: This function (re)initializes 
 * the circular buffer. It should be handled with care because it destroys
 * all events currently stored.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircReset (int cid)
{
  RETURN_IF_CID_INVALID (cid, "CircReset", Error_YES);

  if (CircInit (&cb[cid], -1) == Error_YES)
    {
      ErrorSetF(CIRC_ERR_INITERR,"CircInit","can not (re)initialize circular buffer %d",
		cid);
      return (Error_YES);
    }
  return (Error_NO);
}

/**
 * Resets the specified circular buffer statistics. 
 * Does not modify the content of the circular buffer.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircResetStatistics (int cid)
{
  SEMID semid = cb[cid].sem_id;
  struct CircDesc *shm_desc = cb[cid].shm_desc;

  RETURN_IF_CID_INVALID (cid, "CircResetStatistics", Error_YES);

  SETSEMA (semid, "CircResetStatistics", Error_YES);

  shm_desc->nr_insert = 0;
  shm_desc->nr_retreive = 0;
  shm_desc->nr_try_retreive = 0;
  shm_desc->nr_spy = 0;
  shm_desc->nr_try_spy = 0;

  CLRSEMA (semid, "CircResetStatistics", Error_YES);

  return (Error_NO);
}

/**
 * Set the specified buffer in the dead condition.
 * After this operation, Reserve and Validate commands are forbidden.
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircSetDead (int cid)
{
/*.........................................................................*/

  RETURN_IF_CID_INVALID (cid, "CircSetDead", Error_YES);

  if (cb[cid].shm_size>0) cb[cid].shm_desc->magic = 0; /* only if master */

  return (Error_NO);
}

/**
 * Set the specified multiple buffer in the dead condition.
 * After this operation, Reserve and Validate commands are forbidden.
 * 
 *  Return values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircSetDeadMulti (int cid_amount, int* cid_array)
{
  int i, cid;
  struct CircDesc* d;

  d = (struct CircDesc*)cb[cid_array[0]].shm_desc;  
  if (d->multi != (cid_amount-1))
    {
      ErrorSetF(CIRC_ERR_SETDEAD,"CircSetDead", "can not set dead %d buffer(s) of %d found",
		cid_amount, d->multi+1);
      cid_amount = d->multi+1; /* correct */
      return Error_YES;
    }

  if (cb[cid_array[0]].shm_size>0)
    for (i = 0; i < cid_amount; i++) 
      {
	cid = cid_array[i];
	
	RETURN_IF_CID_INVALID (cid, "CircCloseMulti", Error_YES);
	cb[cid].shm_desc->magic = 0;
      }

  return (Error_NO);
}

/**
 * Tells if the master have clossed the connection.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   true (>0) if new events can arrive
 *   false (0) else 
 */

int CircIsAlive (int cid)
{
  SEMID semid = cb[cid].sem_id;
  RETURN_IF_CID_INVALID (cid, "CircIsAlive", 0);

  /* Check also the existance of the semaphores */
  SETSEMA (semid, "CircIsAlive", 0);
  CLRSEMA (semid, "CircIsAlive", 0);

  return (cb[cid].shm_desc->magic!=0);
}

/**
 * Returns the base address of the circular buffer associated with 
 * identifier <cid>. For special purposes only.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   NULL in case of error, otherwise pointer to the circular buffer.
 */

char *CircGetAddress (int cid)
{
  RETURN_IF_CID_INVALID (cid, "CircGetAddress", NULL);
  return ((char *) cb[cid].shm_desc);
}

/**
 * Returns the number of readable events currently in this circular buffer.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   >= 0: readable events in circular buffer.
 *   <  0: error.
 */

int CircGetContents (int cid)
{
  register struct CircDesc *d = cb[cid].shm_desc;

  RETURN_IF_CID_INVALID (cid, "CircGetContents", Error_YES);

  DEBUG(("CircGetContents> head=0x%x tail=0x%x contents %d\n",
         d->head, d->tail, d->contents));

  return (d->contents);
}

/**
 * Returns the memory currently allocated in this circular buffer.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   >= 0: bytes in circular buffer.
 *   <  0: error.
 */

int CircGetOccupancy (int cid)
{
  SEMID semid = cb[cid].sem_id;
  int occupancy;
  struct CircDesc *d = cb[cid].shm_desc;

  RETURN_IF_CID_INVALID (cid, "CircGetOccupancy", Error_YES);

  SETSEMA (semid, "CircGetOccupancy", Error_YES);
  occupancy = d->head > d->tail ? d->size - d->head + d->tail :
                                  d->tail - d->head;
  CLRSEMA (semid, "CircGetOccupancy", Error_YES);
  return (occupancy);
}

/**
 * Returns the current statistics.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 *  cstat: will be filled with the statistics
 * 
 *  Return values:
 *   == 0: no error
 *   <  0: error.
 */

int CircGetStatistics (int cid, TCircStatistics *cstat)
{
  SEMID semid = cb[cid].sem_id;
  struct CircDesc *d = cb[cid].shm_desc;

  RETURN_IF_CID_INVALID (cid, "CircGetStatistics", Error_YES);

  SETSEMA (semid, "CircGetStatistics", Error_YES);
  cstat->nr_insert = d->nr_insert;
  cstat->nr_retreive = d->nr_retreive;
  cstat->nr_try_retreive = d->nr_try_retreive;
  cstat->nr_spy = d->nr_spy;
  cstat->nr_try_spy = d->nr_try_spy;
  CLRSEMA (semid, "CircGetStatistics", Error_YES);

  return (Error_NO);
}

/**
 * Returns the actual size of the circular buffer.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return values:
 *   >  0: size of the circular buffer in bytes.
 *   <= 0: error.
 */

int CircGetSize (int cid)
{
  register struct CircDesc *d;

  RETURN_IF_CID_INVALID (cid, "CircGetSize", Error_YES);
  d = cb[cid].shm_desc;
  return (d->size);
}

/**
 * Returns the actual total size of the multiple circular buffer.
 *
 *  cid_amount, cid_array : number and array of circular buffer ids
 *                          returned by CircOpenMulti().
 *
 *  Return values:
 *   >  0: total size of all circular buffers in bytes.
 *   <= 0: error.
 */

int CircGetSizeMulti (int cid_amount, int* cid_array)
{
  register struct CircDesc *d = 0;
  int cid = cid_array[0];

  RETURN_IF_CID_INVALID (cid, "CircGetSizeMulti", Error_YES);

  if (cid < cid_amount)        /* just to shut up the compiler */
    d = cb[cid].shm_desc;

  return ( d ? ((d->multi+1) * d->size) : 0 );
}

/**
 * Get information about the buffer.
 */

int CircGetInfo (int cid, struct shmid_ds* meminfo)
{
  return CircShmInfo (&cb[cid], meminfo);
}

/**
 * Reserve space for an event in the circular buffer.
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: number of this event.
 *  size  : event size to be reserved.
 *
 *  Return values:
 *   pointer to a free block in the circular buffer memory. 
 *   (char*)-1 if no space is available (buffer full).
 */

char* CircReserve (int cid, int number, int size)
{
  register long32 head, tail, where, words;
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  words = size + sizeof (struct EventHeader);	/*  include event header */

  /* align end of buffer with longword address */
  if ( words & (ALIGN_TO -1) ) 
    words = (words & ~(ALIGN_TO -1)) + ALIGN_TO;

  DEBUG(("CircReserve> size %d header size %d words %d\n",
         size, sizeof (struct EventHeader), words));

  SETSEMA (semid, "CircReserve", (char*)-1);
  tail = d->tail;
  head = d->head;

  DEBUG(("CircReserve> cid=%d ptr=0x%x tail=0x%x head=0x%x\n",
	 cb[cid].cid, (unsigned int) cb[cid].shm_desc, tail, head));


  if (head <= tail)		/* free space between tail and ceiling */
    if ((tail + words) < d->ceiling)	/* enough space before ceiling */
      {
	where = tail;
	tail += words;
      }
    else
      /* wrap around to beginning of buffer */
      {
	if (words < head)	/* enough space here ? */
	  {
	    d->ceiling = tail;
	    where = 0;
	    tail = words;
	  }
	else			/* too bad */
	  where = -1;
      }
  else
    /* free space between tail and head */
    {
      if ((words + tail) < head)	/* enough ? */
	{
	  where = tail;
	  tail += words;
	}
      else			/* too bad */
	where = -1;
    }

  DEBUG(("CircReserve> where 0x%x\n", where));

  if (where >= 0)
    {
      Event = (struct CircEntry *) &(d->buffer[where]);
      Event->EventHeader.Status = CIRC_FILLING;
      Event->EventHeader.EventNr = number;
      Event->EventHeader.EventLen = size;
      Event->EventHeader.Blen = words;
      if (d->filling==0)
	d->firstcond = where;
      d->filling ++;
      d->lastreserved++;
      d->tail = tail;
      CLRSEMA (semid, "CircReserve", (char*)-1);
      return (&(Event->buffer[0]));
    }
  else
    {
      CLRSEMA (semid, "CircReserve", (char*)-1);
      return (char*)-1;
    }
}

/**
 * Reserve space for an event in the circular buffer.
 * This function should be used by multi-threaded writers.
 * Do not mix with calls to CircPut_t.
 *
 *  cid   : circular buffer id returned by CircOpen_t()
 *  size  : event size to be reserved.
 *
 *  Return values:
 *   pointer to a free block in the circular buffer memory. 
 *   (char*)-1 if no space is available (buffer full).
 */

char* CircReserve_t (int cid, int size)
{
  char* ret;
  static int reserve_packet_number = 0;

  pthread_mutex_lock (&access_mutex_);

  if ((ret = CircReserve (cid, reserve_packet_number, size)) != (char *) -1)
    ++reserve_packet_number;
  else
    pthread_mutex_unlock (&access_mutex_);

  return ret;
}

/**
 * Conditionally validate event reserved with CircReserve() and release unused space.
 *
 * WARNING: Do not mix CircValidate and CircCondValidate calls!
 *          From the first call to CircCondValidate up to the call to
 *          CircCondConfirm or CircCondRemove, no CircValidate are allowed!
 *
 *  cid      : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number   : number of this event as already given by CircReserve().
 *  ptr      : pointer of reserved space returned by CircReserve().
 *  size     : real size of this event in bytes.
 *  setready : if true(!=0), the event can be read from the circular buffer
 *               (same as CircValidate)
 *             else, the event will be ready for reading after a call to CircCondConfirm
 *               (same as CircCondValidate)
 *
 *  Return values:
 *   = 0 if everything was ok
 *   < 0 in case of error(s)
 */

int CircValidateParam (int cid, int number, char *ptr, int size, int setready)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  register long32 diff;
  SEMID semid = cb[cid].sem_id;

  DEBUG(("CircValidateParam> IN: d->filling %d\n", d->filling));

  if (d->filling < 1)
    {
      ErrorSet(CIRC_ERR_VALIDATE,"CircValidateParam","FATAL error: no filling elements found");
      return (Error_YES);
    }
  Event = (struct CircEntry *) (ptr - sizeof (struct EventHeader));
  if (Event->EventHeader.EventNr != number)
    {
      ErrorSetF(CIRC_ERR_VALIDATE,"CircValidateParam",
		"FATAL error: tried to cut Event %d but Event found was %d",
		number, Event->EventHeader.EventNr);
      return (Error_YES);
    }
  diff = Event->EventHeader.EventLen - size;
  if (diff < 0)
    {
      ErrorSetF(CIRC_ERR_VALIDATE, "CircValidateParam",
		"FATAL error: tried to extend buffer length by %d", diff);
      return (Error_YES);
    }
  SETSEMA (semid, "CircValidateParam", Error_YES);

  if (diff > 0)
    {
      Event->EventHeader.EventLen = size;

      if ((long32) ((long)ptr - (long)&(d->buffer[0]) +
		    Event->EventHeader.Blen - sizeof (struct EventHeader)) == d->tail)
	{			/* free space in buffer too */
	  diff &= ~(ALIGN_TO -1); /* Align on longword boundary */
	  Event->EventHeader.Blen -= diff;
	  d->tail -= diff;
	}
    }
  
  d->lastreserved--;

  DEBUG(("CircValidateParam> setready %d\n",
         setready));

  if (setready)
    {
      Event->EventHeader.Status = CIRC_READY;
      d->filling--;
      d->contents++;
      d->nr_insert++;
      d->nr_insert&=0x3fffffff;

      if (d->filling!=d->lastreserved)
	{
	  CLRSEMA (semid, "CircValidateParam", Error_YES);
	  ErrorSetF(CIRC_ERR_VALIDATE,"CircValidateParam",
		    "Fatal error: Mixing CondValidate and Validate!\n");
	  return (Error_YES);
	}
    }

  DEBUG(("CircValidateParam> head=0x%x tail=0x%x contents %d\n",
         d->head, d->tail, d->contents));

  CLRSEMA (semid, "CircValidateParam", Error_YES);
  return (Error_NO);
}

/**
 * Validate event reserved with CircReserve() and release unused space.
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: number of this event as already given by CircReserve().
 *  ptr   : pointer of reserved space returned by CircReserve().
 *  size  : real size of this event in bytes.
 *
 *  Return values:
 *   = 0 if everything was ok
 *   < 0 in case of error(s)
 */

int CircValidate (int cid, int number, char *ptr, int size)
{
  return CircValidateParam(cid,number,ptr,size,1);
}

/**
 * Validate event reserved with CircReserve_t() and release unused space.
 * This function should be used by multi-threaded writers.
 * Do not mix with calls to CircPut_t.
 *
 *  cid   : circular buffer id returned by CircOpen_t().
 *  ptr   : pointer of reserved space returned by CircReserve_t().
 *  size  : real size of this event in bytes.
 *
 *  Return values:
 *   = 0 if everything was ok
 *   < 0 in case of error(s)
 */

int CircValidate_t (int cid, char *ptr, int size)
{
  int ret;
  static int validate_packet_number = 0;

  //pthread_mutex_lock (&access_mutex_);

  if ((ret = CircValidate (cid, validate_packet_number, ptr, size)) != Error_YES)
    ++validate_packet_number;

  pthread_mutex_unlock (&access_mutex_);

  return ret;
}

/**
 * Conditionally validate event reserved with CircReserve() and release unused space.
 * The event will be ready to be read after a call to CircCondConfirm.
 *
 * WARNING: Do not mix CircValidate and CircCondValidate calls!
 *          From the first call to CircCondValidate up to the call to
 *          CircCondConfirm or CircCondRemove, no CircValidate are allowed!
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: number of this event as already given by CircReserve().
 *  ptr   : pointer of reserved space returned by CircReserve().
 *  size  : real size of this event in bytes.
 *
 *  Return values:
 *   = 0 if everything was ok
 *   < 0 in case of error(s)
 */

int CircCondValidate (int cid, int number, char *ptr, int size)
{
  return CircValidateParam(cid,number,ptr,size,0);
}

/**
 * All the conditionaly validated events are finaly validated,
 * i.e. they can be read out from the circular buffer
 *
 *  cid : circular buffer id returned by CircOpen() or CircOpenMulti().
 *
 *  Return values:
 *   = 0 if everything was ok
 *   < 0 in case of error(s)
 */

int CircCondConfirm(int cid)
{
  struct CircDesc *d = cb[cid].shm_desc;
  struct CircEntry *Event;
  long32 this_ptr;
  SEMID semid = cb[cid].sem_id;
  int i;
  

  SETSEMA (semid, "CircCondConfirm", -1);
  if ((d->filling - d->lastreserved)<1)
    {
      CLRSEMA (semid, "CircCondConfirm", -1);
      return 0; /* nothing to do */
    }

  this_ptr = d->firstcond;

  for (i=0; i<(d->filling - d->lastreserved); i++)
    {
      Event = (struct CircEntry *) &(d->buffer[this_ptr]);
      if (Event->EventHeader.Status==CIRC_FILLING)
	Event->EventHeader.Status = CIRC_READY;
      else
	{
	  CLRSEMA (semid, "CircCondConfirm", Error_YES);
	  ErrorSetF(CIRC_ERR_CONFIRM,"CircCondConfirm",
		    "Fatal error: Found a non filling event!!\n");
	  return Error_YES;
	}

      this_ptr += Event->EventHeader.Blen;
      if (this_ptr>=d->ceiling) /* wrap around */
	this_ptr = 0;
    }

  d->contents+=(d->filling - d->lastreserved);

  d->nr_insert+=(d->filling - d->lastreserved);
  d->nr_insert&=0x3fffffff;

  if (d->lastreserved>0)
    {
      d->filling = d->lastreserved;
      d->firstcond = this_ptr;
    }
  else
    {
      d->filling = 0;
      d->firstcond = 0;
    }

  CLRSEMA (semid, "CircCondConfirm", Error_YES);
  return Error_NO;
}

/**
 * All the conditionaly validated events are removed from the circular buffer.
 * All the events must be validated to do this operation!
 *
 *  cid : circular buffer id returned by CircOpen() or CircOpenMulti().
 *
 *  Return values:
 *   = 0 if everything was ok
 *   < 0 in case of error(s)
 */

int CircCondRemove(int cid)
{
  struct CircDesc *d = cb[cid].shm_desc;
  struct CircEntry *Event;
  long32 this_ptr;
  SEMID semid = cb[cid].sem_id;

  SETSEMA (semid, "CircCondRemove", -1);
  if (d->lastreserved>0)
    {
      CLRSEMA (semid, "CircCondRemove", -1);
      ErrorSetF(CIRC_ERR_REMOVE,"CircCondRemove",
		"Last event was not validated. Cannot remove.");
      return Error_YES;
    }

  if (d->filling<1)
    {
      CLRSEMA (semid, "CircCondRemove", -1);
      return Error_NO; /* nothing to do */
    }

  this_ptr = d->firstcond;

  Event = (struct CircEntry *) &(d->buffer[this_ptr]);
  if (Event->EventHeader.Status==CIRC_FILLING)
    {
      if (d->contents==0)
	{ /* if all filling, simply delete everything */
	  d->head = d->tail = 0;
	  d->ceiling = d->length-1;
	}
      else
	{
	  if (this_ptr==0)
	    { /* wipe a wrap around */
	      d->tail = d->ceiling;
	      d->ceiling = d->length-1;
	    }
	  else
	    {
	      if (d->tail<this_ptr)
		d->ceiling = d->length-1; /* wipe a wrap around */
	      
	      d->tail = this_ptr;
	    }
	}
    }
  else
    {
      CLRSEMA (semid, "CircCondRemove", Error_YES);
      ErrorSetF(CIRC_ERR_REMOVE,"CircCondRemove",
		"Fatal error: Found a non filling event!!\n");
      return Error_YES;
    }

  d->firstcond = 0;
  d->filling=0;

  CLRSEMA (semid, "CircCondRemove", Error_YES);
  return Error_NO;
}

/**
 * Locate the next event in the circular buffer.
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: number of this event.
 *  size  : size in bytes of event found.
 *
 *  Return values:
 *   pointer where event was found.
 *   (char*)-1 if no events available (buffer empty).
 */

char* CircLocate (int cid, int *number, int *size)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  SETSEMA (semid, "CircLocate", (char*)-1);

  d->nr_try_retreive++;
  d->nr_try_retreive&=0x3fffffff;

  if (!(d->contents))
    {
      CLRSEMA (semid, "CircLocate", (char*)-1)
      return (char *) -1;		/* there are no events */
    }

  if (d->head >= d->ceiling)
    {
      d->head = 0;
      d->ceiling = d->length - 1;
    }
  Event = (struct CircEntry *) &(d->buffer[d->head]);
  if (Event->EventHeader.Status != CIRC_READY)
    {
      CLRSEMA (semid, "CircLocate", (char*)-1);
      return (char *) -1;	/* the oldest event is not yet ready */
    }
  Event->EventHeader.Status = CIRC_EMPTYING;
  *size   = Event->EventHeader.EventLen;
  *number = Event->EventHeader.EventNr;

  d->nr_retreive++;
  d->nr_retreive&=0x3fffffff;
  
  CLRSEMA (semid, "CircLocate", (char*)-1);
  return &(Event->buffer[0]);
}

/**
 * Release latest event and free space in circular buffer.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return Values: 
 *    = 0 if everything was ok.  
 *    < 0 in case of error(s).  
 */

int CircRelease (int cid)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  SETSEMA (semid, "CircRelease", Error_YES);
  if (d->contents < 1)
    {
      CLRSEMA (semid, "CircRelease", Error_YES);
      ErrorSet(CIRC_ERR_RELEASE, "CircRelease",
		"FATAL error: tried to release event in empty buffer");
      return (Error_YES);
    }
  if (d->head >= d->ceiling)
    {
      d->head = 0;
      d->ceiling = d->length - 1;
    }
  Event = (struct CircEntry *) &(d->buffer[d->head]);
  if (Event->EventHeader.Status != CIRC_EMPTYING)
    {
      CLRSEMA (semid, "CircRelease", Error_YES);
      ErrorSet(CIRC_ERR_RELEASE, "CircRelease",
	       "FATAL error: tried to release not ready event");
      return (Error_YES);
    }
  d->head += Event->EventHeader.Blen;
  d->contents -= 1;
  if ((d->contents+d->filling) == 0)
    {				/* reset to beginning */
      d->head = d->tail = 0;
      d->ceiling = d->length - 1;
    }

  CLRSEMA (semid, "CircRelease", Error_YES);
  return (Error_NO);
}

/**
 * Revalidates latest located event.Use it after a CircLocate
 * if you want to keep the event in the buffer.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 * 
 *  Return Values: 
 *    = 0 if everything was ok.  
 *    < 0 in case of error(s).  
 */

int CircRevalidate (int cid)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  SETSEMA (semid, "CircRevalidate", Error_YES);
  if (d->contents < 1)
    {
      CLRSEMA (semid, "CircRevalidate", Error_YES);
      ErrorSet(CIRC_ERR_REVALIDATE, "CircRevalidate",
		"FATAL error: tried to revalidate event in empty buffer");
      return (Error_YES);
    }
  if (d->head >= d->ceiling)
    {
      d->head = 0;
      d->ceiling = d->length - 1;
    }
  Event = (struct CircEntry *) &(d->buffer[d->head]);
  if (Event->EventHeader.Status != CIRC_EMPTYING)
    {
      CLRSEMA (semid, "CircRevalidate", Error_YES);
      ErrorSet(CIRC_ERR_REVALIDATE, "CircRevalidate",
	       "FATAL error: tried to revalidate not ready event");
      return (Error_YES);
    }
  Event->EventHeader.Status = CIRC_READY;

  d->nr_retreive--;
  d->nr_retreive&=0x3fffffff;
  
  CLRSEMA (semid, "CircRevalidate", Error_YES);
  return (Error_NO);
}

/**
 * Like Circloc() but allows to locate a set of events in a multiple circular
 * buffer with one semaphore lock operation. <mask> defines the circular 
 * buffer ids in <cid_arrary> to be tested. For each event located the bit in 
 * <mask> is cleared and finally returned.   
 *
 * Note: polling must be done by the user. For example:
 *
 *       mask = 0xf;
 *       while ( (mask = CircLocateMulti(mask, 4, ...)) =! 0)
 *         sleep_a_bit;
 *
 *  cid_amount, cid_array : number and array of circular buffer ids
 *                          returned by CircOpenMulti().
 *  buffer_arrary (output): array of pointers to located event.
 *  number_arrary (output): array of event numbers found.
 *  size_arrary   (output): array of event size found.
 *
 *  Return Values:
 *   mask of not located buffers. 
 *   mask=0 means that all locates have been successful.
 *   (char*)-1 in case of error(s).
 */

int CircLocateMulti (int mask, int cid_amount, int* cid_array, 
		     char** buffer_array, int* number_array,
		     int* size_array)
{
  int i, cid = 0;
  int sem_set = 0;
  unsigned int this_bit;
  register struct CircEntry *Event;

  /* Loop over all circular buffers */

  for (i = 0, this_bit = 1; i < cid_amount; i++, this_bit <<= 1) 
    {
      if (mask & this_bit)
	{
	  register struct CircDesc *d;
	  cid = cid_array[i];
	  d   = cb[cid].shm_desc;

	  if (sem_set++ == 0) /* we want to do it with one lock only */
	    SETSEMA (cb[cid].sem_id, "CircLocateMulti", Error_YES);

	  d->nr_try_retreive++;
	  d->nr_try_retreive&=0x3fffffff;

	  if (!(d->contents))
	    {
	    break; /* or continue ... unclear what is better ? */
	    }
	  if (d->head >= d->ceiling)
	    {
	      d->head = 0;
	      d->ceiling = d->length - 1;
	    }
	  Event = (struct CircEntry *) &(d->buffer[d->head]);
	  if (Event->EventHeader.Status != CIRC_READY)
	    break; /* or continue ... unclear what is better ? */
	  Event->EventHeader.Status = CIRC_EMPTYING;
	  size_array  [i] = Event->EventHeader.EventLen;
	  number_array[i] = Event->EventHeader.EventNr;
	  buffer_array[i] = &(Event->buffer[0]);

	  d->nr_retreive++;
	  d->nr_retreive&=0x3fffffff;

	  mask &= ~this_bit;
	}
    }

  if (sem_set)
    CLRSEMA (cb[cid].sem_id, "CircLocateMulti", Error_YES);

  return (mask);
}

/**
 * Release latest event in the circular buffers defined by a bit set in
 * <mask>. Useful together with CircLocateMulti().
 *
 *  cid_amount, cid_array : number and array of circular buffer ids
 *                          returned by CircOpenMulti().
 *
 *  Return Values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircReleaseMulti (int mask, int cid_amount, int* cid_array)
{
  int i, cid = 0;
  register struct CircEntry *Event;
  int sem_set = 0;
  unsigned int this_bit;

  /* Loop over all circular buffers */

  for (i = 0, this_bit = 1; i < cid_amount; i++, this_bit <<= 1) 
    {
      if (mask & this_bit)
	{
	  register struct CircDesc *d;
	  cid = cid_array[i];
	  d   = cb[cid].shm_desc;
	  
	  if (sem_set++ == 0) /* we want to do it with one lock only */
	    SETSEMA (cb[cid].sem_id, "CircReleaseMulti", Error_YES);
	  if (d->contents < 1)
	    {
	      if (sem_set)
		CLRSEMA (cb[cid].sem_id, "CircReleaseMulti", Error_YES);
	      ErrorSet(CIRC_ERR_RELEASE, "CircReleaseMulti",
		       "FATAL error: tried to release event in empty buffer");
	      return (Error_YES);
	    }
	  if (d->head >= d->ceiling)
	    {
	      d->head = 0;
	      d->ceiling = d->length - 1;
	    }
	  Event = (struct CircEntry *) &(d->buffer[d->head]);
	  if (Event->EventHeader.Status != CIRC_EMPTYING)
	    {
	      CLRSEMA (cb[cid].sem_id, "CircReleaseMulti", Error_YES);
	      ErrorSet(CIRC_ERR_RELEASE, "CircReleaseMulti",
		       "FATAL error: tried to release not ready event");
	      return (Error_YES);
	    }
	  d->head += Event->EventHeader.Blen;
	  d->contents -= 1;
	  if ((d->contents + d->filling) == 0)
	    {				/* reset to beginning */
	      d->head = d->tail = 0;
	      d->ceiling = d->length - 1;
	    }
	}
    }

  if (sem_set)
    CLRSEMA (cb[cid].sem_id, "CircReleaseMulti", Error_YES);

  return (mask);
}

/**
 * Locate the next not yet located event in the circular buffer. This 
 * function behaves mainly like CircLocate() but allows to locate events 
 * in advance for parallel execution.
 *
 * Note: Events can only be released in sequence. Your code must handle 
 *       this case correctly! You must call CircReleaseAhead() to release 
 *       an event.
 *
 *  cid   : circular buffer id returned by CircOpen().
 *  number: number of this event.
 *  size  : size in bytes of this event.
 *
 *  Return Values:
 *   pointer where this event was located.
 *   (char*)-1 if there is no more event available.
 */

char* CircLocateAhead (int cid, int *number, int *size)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  DEBUG(("LocCircAhead> IN: contents=%d, head=%d, loc_ahead=%d loc_head=%d\n",
	 d->contents,d->head,d->loc_ahead,d->loc_head));
  if ((d->contents - d->loc_ahead) <= 0)
    {
      DEBUG(("LocCircAhead> no events available.\n"));
      return (char *) -1;	/* there are no more events to locate */
    }
  SETSEMA (semid, "CircLocateAhead", (char*)-1);

  d->nr_try_retreive++;
  d->nr_try_retreive&=0x3fffffff;

  if (d->loc_ahead == 0)	/* loc_head == head */
    {
      if (d->head >= d->ceiling)
	{
	  d->head = 0;
	  d->ceiling = d->length - 1;
	}
      d->loc_head = d->head;
    }
  else
    {
      if (d->loc_head >= d->ceiling)
	d->loc_head = 0;
    }

  Event = (struct CircEntry *) &(d->buffer[d->loc_head]);
  if (Event->EventHeader.Status != CIRC_READY)
    {
      CLRSEMA (semid, "CircLocateAhead", (char*)-1);
      DEBUG(("LocCircAhead> next event not yet ready.\n"));
      return (char *) -1;	/* the oldest event is not yet ready */
    }
  Event->EventHeader.Status = CIRC_EMPTYING;
  *size   = Event->EventHeader.EventLen;
  *number = Event->EventHeader.EventNr;
  DEBUG(("LocCircAhead> event nr=%d size=%d buffer[0-3]=%d %d %d %d\n",
	 *number,*size,
	 Event->buffer[0],Event->buffer[1],Event->buffer[2],Event->buffer[3]
	 ));

  d->loc_head += Event->EventHeader.Blen;
  d->loc_ahead++;

  d->nr_retreive++;
  d->nr_retreive&=0x3fffffff;

  CLRSEMA (semid, "CircLocateAhead", (char*)-1);
  return &(Event->buffer[0]);
}

/**
 * Release latest event located with CircLocateAhead() and free space in 
 * circular buffer.
 *
 *  cid: circular buffer id returned by CircOpen().
 * 
 *  Return Values:
 *   = 0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircReleaseAhead (int cid)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  if (d->contents < 1)
    {
      ErrorSet(CIRC_ERR_RELEASE, "CircReleaseAhead",
	       "FATAL error: tried to release event in empty buffer");
      return (Error_YES);
    }
  SETSEMA (semid, "CircReleaseAhead", Error_YES);
  if (d->head >= d->ceiling)
    {
      d->head = 0;
      d->ceiling = d->length - 1;
    }
  Event = (struct CircEntry *) &(d->buffer[d->head]);
  if (Event->EventHeader.Status != CIRC_EMPTYING)
    {
      CLRSEMA (semid, "CircReleaseAhead", Error_YES);
      ErrorSet(CIRC_ERR_RELEASE,"CircReleaseAhead",
	       "FATAL error: tried to release not ready event");
      return (Error_YES);
    }
  d->head += Event->EventHeader.Blen;
  d->contents -= 1;
  if ((d->contents+d->filling) == 0)
    {				/* reset to beginning */
      d->head = d->tail = 0;
      d->ceiling = d->length - 1;
    }
  if (d->loc_ahead)
    {
      d->loc_ahead--;
    }

  CLRSEMA (semid, "CircReleaseAhead", Error_YES);
  return (Error_NO);
}

/**
 * Put an event into the circular buffer. Actually CircReserve(), memcpy()
 * and CircValidateParam() are used. Convenient to copy data into the circular
 * buffer.
 *
 * WARNING: See CircValidateParam
 *
 *  cid      : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number   : number of this event.
 *  buf      : pointer to event buffer.
 *  size     : event lenght.
 *  setready : if true(!=0), the event can be read from the circular buffer
 *               (same as CircPut)
 *             else, the event will be ready for reading after a call to CircCondConfirm
 *               (same as CircCondPut)
 *
 *  Return Values:
 *   >=0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircPutParam (int cid, int number, char *buf, int size, int setready)
{
  char *ptr;

  RETURN_IF_CID_INVALID (cid, "CircPut", Error_YES);

  DEBUG1(("CircPut> cid=%d: put nr=%d, size=%d\n",cid, number, size));
  ptr = CircReserve (cid, number, size);
  if (ptr == (char *) -1)
    return (Error_YES);
  memcpy (ptr, buf, size);
  (void)CircValidateParam (cid, number, ptr, size, setready);
  return (size);
}

/**
 * Put an event into the circular buffer. Actually CircReserve(), memcpy()
 * and CircValidate() are used. Convenient to copy data into the circular
 * buffer.
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: number of this event.
 *  buf   : pointer to event buffer.
 *  size  : event lenght.
 *
 *  Return Values:
 *   >=0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircPut (int cid, int number, char *buf, int size)
{
  return CircPutParam(cid,number,buf,size,1);
}

/**
 * Put an event into the circular buffer; suitable for use by a threaded
 * input, when more than one thread can access the buffer to copy data.
 * The entire CircPut operation is protected by mutex.
 * The packet counter is counted internally, using a static variable.
 *
 *  cid   : circular buffer id returned by CircOpen_t() or CircOpenMulti_t().
 *  buf   : pointer to event buffer.
 *  size  : event lenght.
 *
 *  Return Values:
 *   >=0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircPut_t (int cid, char *buf, int size)
{
  int ret;
  static int put_packet_number = 0;

  pthread_mutex_lock (&access_mutex_);

  if ((ret = CircPut (cid, put_packet_number, buf, size)) != Error_YES)
    ++put_packet_number;

  pthread_mutex_unlock (&access_mutex_);

  return ret;
}

/**
 * Conditionaly put an event into the circular buffer. Actually CircReserve(),
 * memcpy() and CircCondValidate() are used. Convenient to copy data into the 
 * circular buffer.
 *
 * WARNING: See CircCondValidate
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: number of this event.
 *  buf   : pointer to event buffer.
 *  size  : event lenght.
 *
 *  Return Values:
 *   >=0 if everything was ok.
 *   < 0 in case of error(s).
 */

int CircCondPut (int cid, int number, char *buf, int size)
{
  return CircPutParam(cid,number,buf,size,0);
}

/**
 * Get an event from the circular buffer.  CircLocate(), memcpy() and CircRelease()
 * are used to implement this function. Convenient to copy data out of
 * the circular buffer.
 *
 *  cid    : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number : number of this event.
 *  buf    : pointer to event buffer.
 *  max_len: maximum event lenght.
 *
 *  Return Values:
 *   length of event if everything was ok.
 *   < 0 in case of error(s).
 */

int CircGet (int cid, int *number, char *buf, int max_len)
{
  char *ptr;
  int loc_len;

  RETURN_IF_CID_INVALID (cid, "CircGet", Error_YES);

  ptr = CircLocate (cid, number, &loc_len);
  if (ptr == (char *) -1)
    return (Error_YES);

  if (loc_len > max_len)
    {
      ErrorSetF(CIRC_ERR_USERBUF,"CircGet","user buffer too small to hold %d bytes",
		loc_len);
      return (Error_YES);
    }
  memcpy (buf, ptr, loc_len);
  (void) CircRelease (cid);
  DEBUG1(("CircGet> cid=%d: got nr=%d, size=%d\n",cid, *number, loc_len));

  return (loc_len);
}

/**************************************************************************/
/* Monitoring functions                                                   */
/**************************************************************************/

/**
 * Locate the next event in the circular buffer newer than the last processed.
 *
 *  cid   : circular buffer id returned by CircOpen() or CircOpenMulti().
 *  number: Input/output data
 *          input = number of the last processed event
 *          output number of the event found 
 *  size  : size in bytes of event found.
 *
 *  Return values:
 *   pointer where event was found.
 *   (char*)-1 if no events available (buffer empty).
 */

char* CircSearch (int cid, int *number, int *size)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  long32 head;
  long32 ceiling;
  int n;
  char *evp;

  SETSEMA (semid, "CircSearch", (char*)-1);

  d->nr_try_spy++;
  d->nr_try_spy&=0x3fffffff;

  n = d->contents;

  if (!n)
    {
      CLRSEMA (semid, "CircSearch", (char*)-1)
      return (char *) -1;		/* there are no events */
    }

  head = d->head;
  ceiling = d->ceiling;
  if (head >= ceiling) head = 0;

  evp = (char *) &(d->buffer[head]);

  while (n)
    {
      Event = (struct CircEntry *) evp;

      switch (Event->EventHeader.Status)
	{
	  case CIRC_FILLING:
            CLRSEMA (semid, "CircSearch", (char*)-1);
	    /*printf("CircSearch> WARNING: A filling object was found!\n");*/
            return (char *) -1;	/* the oldest event is not yet ready */
	  case CIRC_COPYING:
	    if (Event->EventHeader.EventNr > *number)
              {
                CLRSEMA(semid,"CircSearch", (char*)-1);
                return (char*) -1;
              }
	    else
	      {
		evp += Event->EventHeader.Blen;
                if (evp >= (char *) &(d->buffer[ceiling]))
                    evp = (char *) &(d->buffer[0]);
		--n;
		break;
	      }
	  case CIRC_EMPTYING:
	    evp += Event->EventHeader.Blen;
            if (evp >= (char *) &(d->buffer[ceiling]))
                evp = (char *) &(d->buffer[0]);
	    --n;
	    break;
	  case CIRC_READY:
	    if (Event->EventHeader.EventNr > *number)
	      {
                Event->EventHeader.Status = CIRC_COPYING;
                *size   = Event->EventHeader.EventLen;
                *number = Event->EventHeader.EventNr;

		d->nr_spy++;
		d->nr_spy&=0x3fffffff;

                CLRSEMA (semid, "CircSearch", (char*)-1);
                return &(Event->buffer[0]);
	      }
	    else
	      {
	        evp += Event->EventHeader.Blen;
                if (evp >= (char *) &(d->buffer[ceiling]))
                    evp = (char *) &(d->buffer[0]);
	        --n;
		break;
	      }
	}
    }

   CLRSEMA (semid, "CircSearch", (char*)-1);
   return  (char*) -1;
}

/**
 * Release latest event processed by monitoring deamon.
 *
 *  cid: circular buffer id returned by CircOpen() or CircOpenMulti().
 *  eventptr is a pointer to the event returned by CircSearch().
 * 
 *  Return Values: 
 *    = 0 if everything was ok.  
 *    < 0 in case of error(s).  
 */

int CircCopied (int cid, char* eventptr)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  SEMID semid = cb[cid].sem_id;

  SETSEMA (semid, "CircCopied", Error_YES);
  if (d->contents < 1)
    {
      CLRSEMA (semid, "CircCopied", Error_YES)
      ErrorSet(CIRC_ERR_RELEASE, "CircCopied",
		"FATAL error: tried to release event in empty buffer");
      return (Error_YES);
    }
  Event = (struct CircEntry*) (eventptr-sizeof(Event->EventHeader));

  if (Event->EventHeader.Status != CIRC_COPYING)
    {
      CLRSEMA (semid, "CircCopied", Error_YES);
      ErrorSet(CIRC_ERR_RELEASE, "CircCopied",
	       "FATAL error: tried to release not ready event");
      return (Error_YES);
    }
  Event->EventHeader.Status = CIRC_READY;
  CLRSEMA (semid, "CircCopied", Error_YES);
  return (Error_NO);
}

/***************************************************************************/
/* Private functions                                                       */
/***************************************************************************/

int CircGetId (void)
{
  int cid;

  if (cb == NULL) /* first channel */
    {
      cb  = (struct CircBuffer*)malloc(sizeof(struct CircBuffer));
      cid = channels_allocated++;
    }
  else
    {
      for (cid=0; cid<channels_allocated; cid++)
	{
	  if (cb[cid].cid != cid) /* there is free channel */
 	    break;
	}
      if (cid==channels_allocated) /* we have to allocate more */
	{
	  cb = (struct CircBuffer*)
	    realloc(cb,(channels_allocated+1)*sizeof(struct CircBuffer));
	  cid = channels_allocated++;
	}
    }
  if (cb == NULL)
    {
      ErrorSetSystemF(CIRC_ERR_CIDALLOC,"CircGetId","can not malloc/realloc channel %d",
		     channels_allocated);
      return (Error_YES);
    }

  cb[cid].cid    = -1;
  cb[cid].shm_id = -1;
  cb[cid].nr     =  1;

  return (cid);
}

/*
 * Create an empty circular buffer at the location given by descrip of 
 * length bytes. Should only be called by the master but harmless if
 * call otherwise.
 */

int CircInit (struct CircBuffer* cbuf, int multi)
{
  int               length = cbuf->shm_size;
  struct CircDesc *descrip = cbuf->shm_desc;
  
  DEBUG(("CircInit> IN: cid=%d ptr=0x%x size=%d semid=0x%x multi=%d\n",
	 cbuf->cid, (unsigned int) descrip, length,
	 cbuf->sem_id, multi));

  if (length < 1)
    return (Error_NO);

  SETSEMA (cbuf->sem_id, "CircInit", Error_YES);

  /* EP: added the master ID to uniquely identify the buffer master */
  descrip->master   = (long32) cbuf->master;
  descrip->magic    = CIRC_HEADER_MAGIC;

  if (multi >= 0) /* otherwise we do just a reset */
    {
      /* indicate next buffer for multi-buffer support */
      descrip->multi    = multi;  /* multiple buffers to follow (last=0) */ 
      descrip->size     = length; /* total size including header */
    }
  /* IS: Was inside the if, I think now is correct */
  /* length in raw data bytes without header */
  length           -= CIRC_HEADER_SIZE;
  length           &= ~(ALIGN_TO -1); /* align with long word boundaries */

  descrip->head     = descrip->tail      = descrip->firstcond = 0;
  descrip->contents = descrip->filling   = 0;
  descrip->lastreserved = 0;
  descrip->loc_head = descrip->loc_ahead = 0;
  descrip->ceiling  = length - 1;
  descrip->length   = length;

  descrip->nr_insert = 0;
  descrip->nr_retreive = 0;
  descrip->nr_try_retreive = 0;
  descrip->nr_spy = 0;
  descrip->nr_try_spy = 0;

  memset (descrip->buffer, 0, length);

  CLRSEMA (cbuf->sem_id, "CircInit", Error_YES);

  DEBUG(("CircInit> OUT: head=0x%x tail=0x%x\n",
         descrip->head, descrip->tail));

  return (Error_NO);
}

int CircCompact (int cid, int number, char *ptr, int size)
{
  register struct CircDesc *d = cb[cid].shm_desc;
  register struct CircEntry *Event;
  int diff;
  SEMID semid = cb[cid].sem_id;
  if (d->filling < 1)
    {
      ErrorSet(CIRC_ERR_VALIDATE,"CircCompact","FATAL error: no filling elements found");
      return (Error_YES);
    }
  Event = (struct CircEntry *) (ptr - sizeof (struct EventHeader));
  if (Event->EventHeader.EventNr != number)
    {
      ErrorSetF(CIRC_ERR_VALIDATE,"CircCompact",
                "FATAL error: tried to cut Event %d but Event found was %d",
                number, Event->EventHeader.EventNr);
      return (Error_YES);
    }
  diff = Event->EventHeader.EventLen - size;
  if (diff < 0)
    {
      ErrorSetF(CIRC_ERR_VALIDATE, "CircCompact",
                "FATAL error: tried to extend buffer length by %d", diff);
      return (Error_YES);
    }
  SETSEMA (semid, "CircValidateParam", Error_YES);

  if (diff > 0)
    {
      Event->EventHeader.EventLen = size;

      if ((long32) ((long)ptr - (long)&(d->buffer[0]) +
		    Event->EventHeader.Blen - sizeof (struct EventHeader)) == d->tail)
        {                       /* free space in buffer too */
          diff &= ~(ALIGN_TO -1); /* Align on longword boundary */
          Event->EventHeader.Blen -= diff;
          d->tail -= diff;
        }
    }
   CLRSEMA (semid, "CircValidateParam", Error_YES);
   return (Error_NO);
}

#ifndef USE_THREAD

/***************************************************************************/
/* Fortran bindings                                                        */
/***************************************************************************/

#define MAX_CIRC_NAME_LEN   1024

int circopen_ (char *keyname, int *size, int keylen)
{
  char name[MAX_CIRC_NAME_LEN];

  if (keylen >= MAX_CIRC_NAME_LEN)
  {
    fprintf (stderr, "CircOpen> key name too long !");
    return (Error_YES);
  }

  memcpy (name, keyname, keylen);
  name[keylen] = '\0';

  return (CircOpen ((char *) NULL, name, *size));
}

int circclose_ (int *cid)
{
  return (CircClose (*cid));
}

int circreset_ (int *cid)
{
  return (CircReset (*cid));
}

int circsetdead_ (int *cid)
{
  return (CircSetDead (*cid));
}

int circisalive_ (int *cid)
{
  return (CircIsAlive (*cid));
}

int circgetcontents_ (int *cid)
{
  return (CircGetContents (*cid));
}

int circgetoccupancy_ (int *cid)
{
  return (CircGetOccupancy (*cid));
}

int circgetsize_ (int *cid)
{
  return (CircGetSize (*cid));
}

long circreserve_ (int *cid, int *number, int *size)
{
  return ((long) CircReserve (*cid, *number, *size));
}

int circvalidate_ (int *cid, int *number, char *ptr, int *size)
{
  return (CircValidate (*cid, *number, ptr, *size));
}

long circlocate_ (int *cid, int *number, int *size)
{
  return ((long) CircLocate (*cid, number, size));
}

int circrevalidate_ (int *cid)
{
  return (CircRevalidate (*cid));
}

int circrelease_ (int *cid)
{
  return (CircRelease (*cid));
}

int circput_ (int *cid, int *number, char *buf, int *len)
{
  return (CircPut (*cid, *number, buf, *len));
}

int circget_ (int *cid, int *number, char *buf, int *max_len)
{
  return (CircGet (*cid, number, buf, *max_len));
}

#endif
