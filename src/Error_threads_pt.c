/***************************************************************************/
/* Error_threads_pt.c
 * =h=> Threads realted stuf, pthreads
 * =p=>
 *  This is the no thread version.
 * ===< 
 ***************************************************************************/

#ifdef LynxOS
# include <sys/time.h>
#else
# include <time.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "circ/Error_private.h"

typedef struct _ErrorList
        {
	  pthread_t  pthread_id;            /* associated thread id */
	  struct _ErrorList *next;

	  ErrorRec rec;
        } ErrorList;

/* this will be used if there is no other memory left */
/* I think this solution is better than a core dump */

static ErrorRec criticErrorRec = {0, Error_PRINT_AT_SET, 0, "no message"};

/* list of records */

static ErrorList *firstError = NULL;

/* syncronizing mutex */

#ifdef LynxOS
static pthread_mutex_t mutex;
#else
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
#endif

/***************************************************************************/
void ErrorGetRec (ErrorRec **rec) /* OUT */
/*----------------------------------
 * =p=>
 * Find record associated with the current thread.
 * If not found, create a new one.
 * ===<
*/
{
/*.........................................................................*/
  ErrorList *tmp;
  pthread_t me_pt = pthread_self();

  /* use a linear search, since the threads are normally only a few */
  pthread_mutex_lock(&mutex);
  for (tmp = firstError; tmp!=NULL; tmp=tmp->next)
    if (tmp->pthread_id == me_pt)
      { /* found */
	*rec = &(tmp->rec);
	pthread_mutex_unlock(&mutex);
	return;
      }
  pthread_mutex_unlock(&mutex);

  /* not found, create another one */
  tmp = (ErrorList *) malloc(sizeof(ErrorList));
  if (tmp==NULL)
    { /* try to prevent a core dump */
      fprintf(stderr,"ErrorGetRec> Malloc error, using a static record!\n");
      *rec = &criticErrorRec;
      return;
    }

  tmp->pthread_id = me_pt;
  tmp->rec.number = 0;
  tmp->rec.mode = Error_PRINT_AT_SET;
  tmp->rec.count = 0;
  strcpy(tmp->rec.message,"no message");

  pthread_mutex_lock(&mutex);
  tmp->next = firstError;
  firstError = tmp;
  pthread_mutex_unlock(&mutex);
  /* end of mutex */

  *rec = &(tmp->rec);

  return;
}

void ErrorDispose()
{
  ErrorList *tmp, *prev;
  pthread_t me_pt = pthread_self();

  /* use a linear search, since the threads are normally only a few */
  pthread_mutex_lock(&mutex);
  for (tmp = firstError, prev=NULL; 
       tmp!=NULL; 
       prev=tmp, tmp=tmp->next)
    if (tmp->pthread_id == me_pt)
      { /* found */
	if (NULL==prev)
	  firstError = tmp->next;
	else
	  prev->next = tmp->next;
	pthread_mutex_unlock(&mutex);
	free(tmp);
	return;
      }
  pthread_mutex_unlock(&mutex);

}

/***************************************************************************/
/* Private functions */
/***************************************************************************/

/***************************************************************************/
int ErrorPrintMessage (ErrorRec *rec,
		       const char *header)
/*----------------------------------
 * ===<
*/
{
/*.........................................................................*/
  time_t now_nr;
  int thread_nr;
  struct tm now_buf;
  struct tm *now_struct;
  char now_str[64];


  time(&now_nr);
  localtime_r(&now_nr,&now_buf);
  now_struct = &now_buf;
  {
    pthread_t thread_id = pthread_self();
    memcpy(&thread_nr,&thread_id,sizeof(thread_nr)); /* if id is longer than an int, take only a part */
  }
  sprintf(now_str,
	  "thr: %04i, %04i-%02i-%02i-%02i.%02i.%02i",
	  thread_nr,
	  now_struct->tm_year+1900,
	  now_struct->tm_mon+1,
	  now_struct->tm_mday, 
          now_struct->tm_hour,
	  now_struct->tm_min,
	  now_struct->tm_sec);

  if (strlen(header)>0)
    printf ("%s %s: %s\n",now_str,header, rec->message);
  else
    printf ("%s %s\n",now_str,rec->message);

  return(rec->number);
}

