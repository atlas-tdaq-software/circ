C     fCircwrite.f, fortran 77 circular buffer example. 

      PROGRAM FCIRCWRITE

      IMPLICIT NONE

      INTEGER CIRCOPEN, CIRCPUT, CIRCGETCONTENTS
      INTEGER CID
      INTEGER STAT, I
      INTEGER BUFSIZE, BUF(100)

      CHARACTER*21 KEY

      KEY = '/tmp/epasqual/_f_Circ'
      BUFSIZE = 100000

      CID = CIRCOPEN (KEY, BUFSIZE)

      IF (CID .LT. 0) THEN
         PRINT *, 'CIRCOPEN ERROR'
      ENDIF

      DO I=1,100
         BUF(I)=I
      ENDDO

      DO I=1,1000
         PRINT *, '--> PUT ', I

 1       STAT = CIRCPUT (CID, I, BUF, 400)

         IF (STAT .LT. 0) THEN
            CALL SLEEP (1)
            GOTO 1
         ENDIF
      ENDDO

      CALL CIRCSETDEAD (CID)

      I = CIRCGETCONTENTS (CID)

      DO WHILE (I .GT. 0)
         CALL SLEEP (1)
         I = CIRCGETCONTENTS (CID)
      ENDDO

      CALL CIRCCLOSE (CID)

      STOP
      END
