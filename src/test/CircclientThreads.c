#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sched.h>
#include <unistd.h>
#include <pthread.h>

#include "circ/Error.h"
#include "circ/Circ.h"
#include "circ/Circservice.h"

#define TRUE              1
#define FALSE             0

#define MAX_THREADS      64
#define DEF_INTERVAL  10000
#define DEF_MAXEV         1

int cid;
int nthreads = 1;
int evtsize = 512;
int bufsize = -1;
int maxev = -1;
int interval = 10000;

int use_put = FALSE;
int use_thr = FALSE;
int use_opn = FALSE;

char *bufname = (char *) NULL;
char *data = (char *) NULL;
unsigned short port = CIRC_SERVER_DEFAULT_PORT;

int end_loop = 0;

pthread_t id_[MAX_THREADS];
pthread_mutex_t cond_mutex_;
pthread_mutex_t my_access_mutex_;

void *circ_client_threads_run ()
{
  char *ptr;
  static int number = 0;

  if (use_opn)
  {
    cid = CircOpenCircConnection_t (port, bufname, bufsize);
  }

  while (! end_loop)
  {
    /* Locking at each event can severily affect performance */

    if (use_put)
    {
      if (CircPut_t (cid, data, evtsize) != Error_YES)
      {
	pthread_mutex_lock (&my_access_mutex_);

	++number;

	if (interval && (number % interval == 0))
	  printf (" CircclientThread> Number= %d \n", number);

	if (number == maxev)
	{
	  pthread_mutex_lock (&cond_mutex_);
	  end_loop = 1;
	  pthread_mutex_unlock (&cond_mutex_);
	}

	pthread_mutex_unlock (&my_access_mutex_);
      }
    }
    else if (use_thr)
    {
      if ((ptr = CircReserve_t (cid, evtsize)) != (char *) -1)
      {
	memcpy (ptr, data, evtsize);

	(void) CircValidate_t (cid, ptr, evtsize);

	pthread_mutex_lock (&my_access_mutex_);

	++number;

	if (interval && (number % interval == 0))
	  printf (" CircclientThread> Number= %d \n", number);

	if (number == maxev)
	{
	  pthread_mutex_lock (&cond_mutex_);
	  end_loop = 1;
	  pthread_mutex_unlock (&cond_mutex_);
	}

	pthread_mutex_unlock (&my_access_mutex_);
      }
    }
    else
    {
      pthread_mutex_lock (&my_access_mutex_);

      if ((ptr = CircReserve (cid, number, evtsize)) != (char *) -1)
      {
	memcpy (ptr, data, evtsize);

	(void) CircValidate (cid, number, ptr, evtsize);

	++number;

	if (interval && (number % interval == 0))
	  printf (" CircclientThread> Number= %d \n", number);

	if (number == maxev)
	{
	  pthread_mutex_lock (&cond_mutex_);
	  end_loop = 1;
	  pthread_mutex_unlock (&cond_mutex_);
	}
      }

      pthread_mutex_unlock (&my_access_mutex_);
    }

    sched_yield ();
  }

  if (use_opn)
    CircCloseCircConnection_t (port, bufname, cid);

  return 0;
}

/* End of thread specific functions */

void circ_client_threads_signal_handler (int val)
{
  printf ("Signal %d received\n", val);

  pthread_mutex_lock (&cond_mutex_);
  end_loop = 1;
  pthread_mutex_unlock (&cond_mutex_);
}

int circ_client_threads_help ()
{
  printf ("Usage: CircclientThreads [options] <circ_name> <circ_size> [<event_size>]\n"
	  "\t\t\t\t[-p port]\n\t\t\t\t[-n threads]\n\t\t\t\t[-i interval]\n\t\t\t\t[-e maxev]\n"
          "\t\t\t\t[-u (use threaded put)]\n\t\t\t\t[-t (use threaded functions)]\n"
	  "\t\t\t\t[-o (use multiple open requests)]\n");

  return -1;
}

int circ_client_threads_opt (int c, char **v)
{
  register int i;

  for (i=1; i < c ; ++i)
    if (v[i][0] == '-')
    {
      switch (v[i][1])
      {
        case 'p':
          port = (unsigned short) atoi (v[++i]);
          break;
        case 'n':
          nthreads = atoi (v[++i]);
          break;
        case 'i':
          interval = atoi (v[++i]);
          break;
        case 'e':
          maxev = atoi (v[++i]);
          break;
        case 'u':
	  use_put = TRUE;
	  break;
        case 't':
	  use_thr = TRUE;
	  break;
        case 'o':
	  use_opn = TRUE;
	  break;
        default:
          return circ_client_threads_help ();
      }
    }
    else if (bufsize >= 0)
      evtsize = atoi (v[i]);
    else if (bufname)
      bufsize = atoi (v[i]);
    else
      bufname = v[i];

  if (! bufname || bufsize < 0)
    return circ_client_threads_help ();

  return 0;
}

int circ_client_threads_init ()
{
  int i;

  /* Init mutexes */

  pthread_mutex_init (&cond_mutex_, 0);
  pthread_mutex_init (&my_access_mutex_, 0);

  /* Install signals - to be done after mutex init */

  signal (SIGTERM, circ_client_threads_signal_handler);
  signal (SIGINT, circ_client_threads_signal_handler);

  /* Allocate data buffer to be copied */

  data = (char *) malloc (evtsize);

  /* Prepare data */

  for (i=0; i < evtsize; ++i)
    data[i] = (char) (i & 0xff);

  if (!use_opn)
  {
    /* Ask the server to open a buffer and connect to it */

    cid = CircOpenCircConnection_t (port, bufname, bufsize);
  }

  /* Start data writing threads */

  for (i=0; i < nthreads; ++i)
    pthread_create (&id_[i], NULL, circ_client_threads_run, 0);

  return (cid < 0);
}

int circ_client_threads_loop ()
{
  int i;
  int err = 0;

  /* Just wait until receive an interrupt */

  while (! end_loop)
    sleep (1);

  /* Stop the writing threads */

  for (i=0; i < nthreads; ++i)
    pthread_join (id_[i], NULL);

  return err;
}

int circ_client_threads_end ()
{
  int err = 0;

  /* Destroy mutexes */

  pthread_mutex_destroy (&cond_mutex_);
  pthread_mutex_destroy (&my_access_mutex_);

  free (data);

  if (!use_opn)
  {
    /* Close the buffer connection and ask 
       the server to close the buffer */

    err = (CircCloseCircConnection_t (port, bufname, cid) != Error_NO);
  }

  return (err);
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = circ_client_threads_opt (argc, argv)))
    return err;

  if ((err = circ_client_threads_init ()))
    return err;

  if ((err = circ_client_threads_loop ()))
    return err;

  if ((err = circ_client_threads_end ()))
    return err;

  return err;
}
