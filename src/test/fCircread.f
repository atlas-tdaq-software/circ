C     fCircread.f, fortran 77 circular buffer example. 

      PROGRAM FCIRCREAD

      IMPLICIT NONE

      INTEGER CIRCOPEN, CIRCGET, CIRCISALIVE
      INTEGER CID
      INTEGER STAT, I
      INTEGER BUFSIZE
      CHARACTER*512 BUF

      CHARACTER*21 KEY

      KEY = '/tmp/epasqual/_f_Circ'
      BUFSIZE = 0

      CID = CIRCOPEN (KEY, BUFSIZE)

      IF (CID .LT. 0) THEN
         PRINT *, 'CIRCOPEN ERROR'
      ENDIF

      I = 0
      DO WHILE (I .GE. 0)
         PRINT *, '--> GET ', I

 1       STAT = CIRCGET (CID, I, BUF, 512)

         IF (STAT .LT. 0) THEN
            STAT = CIRCISALIVE (CID)

            IF (STAT .GT. 0) THEN
               CALL SLEEP (1)
               GOTO 1
            ELSE
               I = -1
            ENDIF
         ENDIF
      ENDDO

      CALL CIRCCLOSE (CID)

      STOP
      END
