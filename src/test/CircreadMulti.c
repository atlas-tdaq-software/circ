/***************************************************************************/
/* CircreadMulti.c 
 * Usage: Circread <key-name> <buffer-size> <cid-amount>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circread <key-name> <buffer-size> <cid-amount>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sched.h>

#include "circ/Circ.h"

/* timing */
#include "circ/Circ_timing.h"

#define MAX_EVTSIZE 65536*2
#define GO_ON   0
#define STOP_IT 1
#define MAGIC 0x31071965

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
{
  char *key;
  int *data, *ptr;
  int ncid, bufsize, evtsize, repeat = 0, empty = 0;
  int* cids;
  int number, i;
  int cid, cid_next = 0;

  timer_id tid = -1;
  double real_time, cpu_time, cpu_usage;

  fd_set fdset;
  bzero((char *)(&fdset), sizeof(*(&fdset)));

/*......................................................................... */

  if (argc != 4)
    USAGE;

  key = argv[1];
  bufsize = atoi (argv[2]);
  ncid = atoi (argv[3]);

  data = (int*) malloc (MAX_EVTSIZE);
  cids = (int*) malloc (ncid * sizeof(int));

  if ((ncid = CircOpenMulti (NULL, key, bufsize, ncid, cids)) < 0)
    {
      fprintf (stderr, "%s > Open for %d circular buffers failed !\n", 
	       argv[0], ncid);
      exit (1);
    }
  printf ("%s > %d circular buffer opened for read : key=%s, cids=%d..%d\n",
	  argv[0], ncid, key, cids[0], cids[ncid-1]);


  for (i = 0; i < ncid; i++)
    {
      cid = cids[i];
      printf ("%s > cid=%d: Circular buffer base address: %lx\n",
	      argv[0], cid, (unsigned long) CircGetAddress (cid));
    }

  /* Test 1: CircLocate, CircRelease without memcpy
   */
  printf ("%s > Test #2 using CircLocate/CircRelease without moving data\n", argv[0]);
  repeat = 0;
  data[0] = GO_ON;
  while (data[0] == GO_ON)
    {
      cid = cids[cid_next];
      while ((ptr = (int *) CircLocate (cid, &number, &evtsize)) == (int *) -1)
	{
	  empty++;
	  sched_yield ();
	}

      if (repeat++ == 0)
	tid = circ_timing_start ();

      if (ptr[evtsize / sizeof (int) - 1] != MAGIC)
	{
	  printf ("%s > Invalid MAGIC number %x found for event #%d !!!\n",
		  argv[0], ptr[evtsize / sizeof (int) - 1], repeat);
	  exit (1);
	}

      if (repeat != number)
	{
	  printf ("%s > Invalid event number %d found instead of %d !!!\n",
		  argv[0], number, repeat);
	  exit (1);
	}

      data[0] = ptr[0];
      (void) CircRelease (cid);
      if ( (++cid_next) >= ncid) cid_next = 0;
    }

  circ_timing_stop (tid, &real_time, &cpu_time, &cpu_usage);

  sleep (1);
  printf ("%s > Real-time: %.2fs, CPU-time: %.2fs, CPU-usage: %.1f%%\n",
	  argv[0], real_time, cpu_time, cpu_usage);
  printf ("%s > Events: %d, %.0f per sec\n", argv[0],
	  repeat, (double) repeat / real_time);
  printf ("%s > Bytes : %.0f, %.1f MB /sec\n", argv[0],
	  (double) repeat * evtsize,
	  (double) repeat * evtsize / 1024. / 1024. / real_time);
  printf ("%s > Buffer empty: %d\n", argv[0], empty);

  (void) CircCloseMulti (ncid, cids);

  return 0;
}
