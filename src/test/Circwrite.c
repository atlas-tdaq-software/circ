/***************************************************************************/
/* Circwrite.c, circular buffer example and test program between processes. 
 *
 * Usage: Circwrite [-d] <key-name> <buffer-size> <event-lenght> <repeat-count>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circwrite [-d] <key-name> <buffer-size> <event-lenght> <repeat-count>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sched.h>

/* Circ */
#include "circ/Circ.h"

/* timing */
#include "circ/Circ_timing.h"

#define GO_ON            0
#define STOP_IT          1
#define MAGIC            0x31071965
#define DEFAULT_TIMEOUT  60

int end_loop = 0;
int debug = 0;

void Circwrite_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
{
  char *key;
  int *data, *ptr;
  int cid, bufsize, evtsize, repeat, last;
  int full = 0; 
  int np = 0, i = 0;
  int circOpenError = 0;
  time_t t0, t1, dt, timeout = DEFAULT_TIMEOUT;

  double real_time, cpu_time, cpu_usage;

  timer_id tid;

/*......................................................................... */

  /* Get parameters form the command line */

  if (argc < 5)
    USAGE;

  while (argv[++i][0] == '-')
    switch (argv[i][1])
    {
      case 'd':
        debug = 1;
	++np;
        break;
      default:
        USAGE;
    }

  if (argc != 5 + np)
    USAGE;

  key     = argv[1+np];
  bufsize = atoi (argv[2+np]);
  evtsize = atoi (argv[3+np]);
  repeat  = atoi (argv[4+np]);

  signal (SIGTERM, Circwrite_signal_handler);
  signal (SIGINT, Circwrite_signal_handler);

  data = (int *) malloc (evtsize);

  /* Open the circular buffer */

  time (&t0);

  do
  {
    if ((cid = CircOpen (NULL, key, bufsize)) < 0)
    {
      circOpenError = 1;
      time (&t1);
      dt = t1 - t0;
      usleep (10);
    }
    else
    {
      circOpenError = 0;
      break;
    }
  } while (bufsize == 0 && dt < timeout);
                           
  if (circOpenError)
  {
    fprintf (stderr, "%s> Circular buffer open failed !\n", argv[0]);
    return (1);
  }

  printf ("%s> Circular buffer opened for write: key=%s, id=%d\n",
	  argv[0], key, cid);
  printf ("%s> write %d events of size %d\n", argv[0], repeat, evtsize);
  printf ("%s> Circular buffers base address: 0x%lx and size: %d\n",
	  argv[0], (unsigned long) CircGetAddress (cid), CircGetSize (cid));

  last       = evtsize / sizeof (int) - 1;
  data[0]    = GO_ON;
  data[last] = MAGIC;

  /* Test #1: CircReserve, CircValidate without memcpy
  ****************************************************/
  printf ("%s> Test #1: CircReserve/CircValidate without moving data\n",
	  argv[0]);

  tid = circ_timing_start ();
  for (i = 1; i <= repeat && (!end_loop); i++)
  {
    while ((ptr = (int *) CircReserve (cid, i, evtsize)) 
	   == (int *) -1 && (!end_loop))
    {
      full++;
      sched_yield ();
    }

    if (end_loop)
      break;

    if (i < repeat)
      ptr[0] = GO_ON;
    else
      ptr[0] = STOP_IT; /* mark end of test */

    ptr[last] = MAGIC;

    (void) CircValidate (cid, i, (char *) ptr, evtsize);
  }

  circ_timing_stop (tid, &real_time, &cpu_time, &cpu_usage);

  printf ("%s> Real-time: %.2fs, CPU-time: %.2fs, CPU-usage: %.1f%%\n",
	  argv[0], real_time, cpu_time, cpu_usage);
  printf ("%s> Events: %d, %.0f per sec\n", argv[0],
	  repeat, (double) repeat / real_time);
  printf ("%s> Bytes : %.0f, %.1f MB /sec\n", argv[0],
	  (double) repeat * evtsize,
	  (double) repeat * evtsize / 1024. / 1024. / real_time);
  printf ("%s> Buffer full: %d\n", argv[0], full);

  sleep (5);

  /* Test 2: CircPut with moving data (memcpy)
  ********************************************/

  printf ("%s> Test #2: CircPut with moving data (memcpy)\n", argv[0]);

  tid = circ_timing_start ();
  for (i = 1; i <= repeat && (!end_loop); i++)
  {
    if (i == repeat)
      data[0] = STOP_IT; /* mark end of test */

    while (CircPut (cid, i, (char *) data, evtsize) < 0 && (!end_loop))
    {
      full++;
      sched_yield ();
    }
  }

  circ_timing_stop (tid, &real_time, &cpu_time, &cpu_usage);

  printf ("%s> Real-time: %.2fs, CPU-time: %.2fs, CPU-usage: %.1f%%\n",
	  argv[0], real_time, cpu_time, cpu_usage);
  printf ("%s> Events: %d, %.0f per sec\n", argv[0],
	  repeat, (double) repeat / real_time);
  printf ("%s> Bytes : %.0f, %.1f MB /sec\n", argv[0],
	  (double) repeat * evtsize,
	  (double) repeat * evtsize / 1024. / 1024. / real_time);
  printf ("%s> Buffer full: %d\n", argv[0], full);

  /* Close circular buffer */
  CircSetDead(cid);

  if (debug)
  {
    int nr;

    for (nr=CircGetContents(cid);
	 nr>0;
	 nr=CircGetContents(cid))
      printf("Debug: %i events left\n",nr);
  }

  (void) CircClose (cid);

  return 0;
}
