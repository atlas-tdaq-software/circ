/***************************************************************************/
/* Circreadrecord.c, ASCII circular buffer consumer, reads out the buffer
 *                   record by record. 
 *
 * Usage: Circreadrecord <key-name> <buffer-size>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circconsumer [-d] [-p <printout-interval>] [-s <sleep>] \
<key-name> <buffer-size>\
";
#define USAGE {printf("%s\n",Usage); return(1);}

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sched.h>

/* Circ */
#include "circ/Circ.h"
#include "circ/Circutils.h"

#define DEFAULT_TIMEOUT  60
#define MAX_RECORD_LEN   8192

int end_loop = 0;

void Circreadrecord_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *key;
  char *rec;
  int i = 0;
  int dump = 0;
  int cid, bufsize, nloop = 0, empty = 0;
  int number = 0;
  int interval = 0, nsleep = 0, np = 0;
  int circOpenError = 0;
  time_t t0, t1, dt, timeout = DEFAULT_TIMEOUT;
                                                                                
  if (argc < 3)
    USAGE;

  while (argv[++i][0] == '-')
    switch (argv[i][1]) {
      case 'd':
        dump = 1;
        np ++;
        break;
      case 'p':
        interval = atoi (argv[++i]);
        np += 2;
        break;
      case 's':
        nsleep = atoi (argv[++i]);
        np += 2;
        break;
      default:
        USAGE;
    }

  if (argc != 3 + np)
    USAGE;

  key     = argv[1+np];
  bufsize = atoi (argv[2+np]);

  signal (SIGTERM, Circreadrecord_signal_handler);
  signal (SIGINT, Circreadrecord_signal_handler);

  rec = (char *) malloc (MAX_RECORD_LEN);

  /* Open the circular buffer */

  time (&t0);
                                                
  do
  {
    if ((cid = CircOpen (NULL, key, bufsize)) < 0)
    {
      circOpenError = 1;
      time (&t1);
      dt = t1 - t0;
      usleep (10);
    }
    else
    {
      circOpenError = 0;
      break;
    }
  } while (bufsize == 0 && dt < timeout);
                                                                                
  if (circOpenError)
  {
    fprintf (stderr, "%s> Circular buffer open failed !\n", argv[0]);
    return (1);
  }

  printf ("%s> Circular buffer opened for read: key=%s, id=%d\n",
	  argv[0], key, cid);

  while (end_loop == 0)
  {
    int reclen = MAX_RECORD_LEN;

    while (CircGetRecord (cid, rec, &reclen) <= 0)
    {
      empty++;

      if (end_loop)
      {
	(void) CircClose (cid);
	return (0);
      }

      sched_yield ();
    }

    ++number;

    if (dump)
    {
      printf (" Circreadrecord> Record number %d, size = %d\n",
	      number, reclen);
      printf ("%s\n", rec);
    }

    sched_yield ();

    if (interval && (++nloop%interval == 0))
      printf (" Circreadrecord> Record Number= %d \n",number);

    if (nsleep)
      sleep (nsleep);
  }

  (void) CircClose (cid);

  free (rec);

  return 0;
}
