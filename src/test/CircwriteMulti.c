/***************************************************************************/
/* CircwriteMulti.c 
 * Usage: CircwriteMulti <key-name> <event-lenght> <repeat-count>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circwrite <key-name> <event-lenght> <repeat-count>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sched.h>

/* timing */
#include "circ/Circ_timing.h"

#define GO_ON   0
#define STOP_IT 1
#define MAGIC   0x31071965

#include "circ/Circ.h"

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
{
  char *key;
  int *data, *ptr;
  int evtsize, repeat, full = 0;
  int i;
  int cids[16];
  int ncid, cid_next = 0;

  double real_time, cpu_time, cpu_usage;

  timer_id tid;

  fd_set fdset;
  bzero((char *)(&fdset), sizeof(*(&fdset)));

/*......................................................................... */

  if (argc != 4)
    USAGE;

  key = argv[1];
  evtsize = atoi (argv[2]);
  repeat = atoi (argv[3]);

  evtsize &= ~(0x3);
  data = (int *) malloc (evtsize);

  if ((ncid = CircOpenMulti (NULL, key, 0, 16, cids)) < 1)
    {
      fprintf (stderr, "%s > Multiple circular buffer open failed !\n", 
	       argv[0]);
      exit (1);
    }

  printf ("%s> Circular buffer opened for write: key=%s, cid=%d..%d\n",
	  argv[0], key, cids[0], cids[ncid-1]);
  printf ("%s> write %d events of size %d\n", 
	  argv[0],repeat,evtsize);

  data[0] = 0;
  data[evtsize / sizeof (int) - 1] = MAGIC;

  /* Test #1: CircReserve, CircValidate without memcpy
     ****************************************** */
  printf ("%s> Test #2 using CircReserve/CircValidate without moving data\n", argv[0]);
  tid = circ_timing_start ();
  for (i = 1; i <= repeat; i++)
    {
      int cid;

      cid = cids[cid_next];
      while ((ptr = (int *) CircReserve (cid, i, evtsize)) == (int *) -1)
	{
	  full++;
	  sched_yield ();
	}

      if (i < repeat)
	ptr[0] = 0;
      else
	ptr[0] = 1;		/* mark end of test */

      ptr[evtsize / sizeof (int) - 1] = MAGIC;

      (void) CircValidate (cid, i, (char *) ptr, evtsize);
      if ( (++cid_next) >= ncid) cid_next = 0;
    }

  circ_timing_stop (tid, &real_time, &cpu_time, &cpu_usage);
  printf ("%s> Real-time: %.2fs, CPU-time: %.2fs, CPU-usage: %.1f%%\n",
	  argv[0], real_time, cpu_time, cpu_usage);
  printf ("%s> Events: %d, %.0f per sec\n", argv[0],
	  repeat, (double) repeat / real_time);
  printf ("%s> Bytes : %.0f, %.1f MB /sec\n", argv[0],
	  (double) repeat * evtsize,
	  (double) repeat * evtsize / 1024. / 1024. / real_time);
  printf ("%s> Buffer full: %d\n", argv[0], full);

  (void) CircCloseMulti (16, cids);

  return 0;
}
