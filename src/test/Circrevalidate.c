/***************************************************************************/
/* Circrevalidate.c, to test revalidate function. 
 *
 * Usage: Circrevalidate <key-name> <buffer-size> <event-size>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circrevalidate [-p <printout-interval>] [-n <number of events>] \
<key-name> <buffer-size> <event-size> \
";
#define USAGE {printf("%s\n",Usage); return(1);}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sched.h>

/* Circ */
#include "circ/Circ.h"

#define DEFAULT_TIMEOUT  60

int end_loop = 0;

void Circrevalidate_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *key;
  int i = 0;
  int np = 0;
  char *data, *ptr;
  int interval = 1;
  int cid, bufsize, evtsize, size, nloop = 0;
  int number = 0;
  int n = -1;

  TCircStatistics cstat;

  if (argc < 4)
    USAGE;

  while (argv[++i][0] == '-')
    switch (argv[i][1])
    {
      case 'p':
        interval = atoi (argv[++i]);
        np += 2;
        break;
      case 'n':
        n = atoi (argv[++i]);
        np += 2;
        break;
      default:
        USAGE;
    }

  if (argc != 4 + np)
    USAGE;

  key     = argv[1+np];
  bufsize = atoi (argv[2+np]);
  evtsize = atoi (argv[3+np]);

  if (bufsize == 0 || evtsize == 0)
    return (1);

  signal (SIGTERM, Circrevalidate_signal_handler);
  signal (SIGINT, Circrevalidate_signal_handler);

  data = (char *) malloc (evtsize);

  /* Prepare data */

  for (i=0; i < evtsize; ++i)
    data[i] = (char) (i & 0xff);

  /* Open the circular buffer */

  if ((cid = CircOpen (NULL, key, bufsize)) < 0)
  {
    fprintf (stderr, "%s> Circular buffer open failed !\n", argv[0]);
    return (1);
  }

  while (end_loop == 0 && nloop != n)
  {
    if ((ptr = CircReserve (cid, number, evtsize)) == (char *) -1)
    {
      printf ("Error reserving memory\n");
      break;
    }

    memcpy (ptr, data, evtsize);

    (void) CircValidate (cid, number, ptr, evtsize);

    if ((ptr = CircLocate (cid, &i, &size)) == (char *) -1)
    {
      printf ("Error locating event\n");
      break;
    }

    (void) CircRevalidate (cid);

    if ((ptr = CircLocate (cid, &i, &size)) == (char *) -1)
    {
      printf ("Error locating event after revalidate\n");
      break;
    }

    (void) CircRelease (cid);

    ++number;

    if (interval && (++nloop%interval == 0))
      printf (" Circrevalidate> Number= %d \n",number);
  }

  (void) CircGetStatistics (cid, &cstat);

  printf ("Inserted %d, Retreived %d, Try retreive %d\n",
	  cstat.nr_insert, cstat.nr_retreive, cstat.nr_try_retreive);

  (void) CircClose (cid);

  free (data);

  return 0;
}

