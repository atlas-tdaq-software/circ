/***************************************************************************/
/* Circbuster.c, open circular buffer discovery. 
 *
 * Usage: Circbuster <root-key-name>
 */
/***************************************************************************/
static char Usage[] = "Usage: Circbuster <root-key-name>";
#define USAGE {printf("%s\n",Usage); return(1);}

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <dirent.h>
#include <signal.h>
#include <unistd.h>

#define DO_YIELD    /* fast switch to next process */

#ifdef DO_YIELD
#include <sched.h>
#endif

/* Circ */

#include "circ/Circ.h"
#include "circ/Error.h"

struct flist {
  char *name;
  struct flist *next;
};

int end_loop = 0;

extern int errno;

void Circbuster_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *key;
  char *p, *dirname, *froot;

  DIR *d;

  if (argc < 2)
    USAGE;

  key = argv [1];

  signal (SIGTERM, Circbuster_signal_handler);
  signal (SIGINT, Circbuster_signal_handler);

  /* Get the directory name and strip off it from file name root */

  if ((p = strrchr (key, '/')))
  {
    dirname = key;
    *p = '\0';
    froot = ++p;
  }
  else
  {
    dirname = (char *) malloc (strlen (".") + 1);
    strcpy (dirname, ".");
    froot = key;
  }

  printf ("directory %s root %s\n", dirname, froot);

  /* Loops forever checking for files called <file root>... */

  if ((d = opendir (dirname)) == (DIR *) NULL)
  {
    perror (strerror (errno));
    return -1;
  }

  while (!end_loop)
  {
    int n = 0;
    char *kname;
    struct dirent *fentry;
    struct flist *k, *klist = (struct flist *) NULL;

    while ((fentry = readdir (d)))
    {
      kname = (char *) malloc (strlen (fentry->d_name) + strlen (dirname) + 2);
      sprintf (kname, "%s/%s", dirname, fentry->d_name);

      if ((p = strstr (fentry->d_name, froot)) == fentry->d_name)
	if (strstr (fentry->d_name, "._CIRC_SEM") ||
	    strstr (fentry->d_name, "._CIRC_SHM"))
        {
	  struct flist *lastk;

	  p = strstr (kname, "._CIRC_S");
	  *p = '\0';

	  if (klist)
	  {
	    k = klist;

	    while (k)
	    {
	      lastk = k;

	      if (!strcmp (kname, k->name))
		break;

	      k = k->next;
	    }

	    if (!k)
	    {
	      k = lastk->next = (struct flist *) malloc (sizeof (struct flist));
	      k->name = (char *) malloc (strlen (kname) + 1);
	      strcpy (k->name, kname);
	      k->next = (struct flist *) NULL;
	    }
	  }
	  else
	  {
	    k = klist = (struct flist *) malloc (sizeof (struct flist));
	    k->name = (char *) malloc (strlen (kname) + 1);
	    strcpy (k->name, kname);
	    k->next = (struct flist *) NULL;
	  }
	}

      free (kname);
    }

    /* try to open ... */

    ErrorSetPrintMode (Error_PRINT_OFF);

    k = klist;
    while (k)
    {
      int cid;

      printf ("%s\n", k->name);

      if ((cid = CircOpen (NULL, k->name, 0)) < 0)
      {
	k = k->next;
	continue;
      }

      if (! CircIsAlive (cid))
      {
	k = k->next;
	continue;
      }

      (void) CircClose (cid);
      printf ("%s is open\n", k->name);
      k = k->next;
      ++n;
    }

    printf ("%d\n", n);
    rewinddir (d);

    /* list cleanup */

    k = klist;
    klist = (struct flist *) NULL;

    do
    {
      struct flist *knext;

      knext = k->next;
      free (k->name);
      free (k);
      k = knext;
    } while (k);

#ifdef DO_YIELD
    sched_yield ();
#endif
  }

  closedir (d);

  return 0;
}
