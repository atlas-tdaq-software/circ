C     fCircread.f, fortran 77 circular buffer example. 

      PROGRAM FCIRCREADRECORD

      IMPLICIT NONE

      INTEGER CIRCOPEN, CIRCGETRECORD, CIRCISALIVE
      INTEGER CID
      INTEGER DUMP
      INTEGER STAT, I
      INTEGER RECLEN
      INTEGER BUFSIZE, RECSIZE
      CHARACTER*256 REC

      CHARACTER*21 KEY

      KEY = '/tmp/epasqual/_f_Circ'
      BUFSIZE = 0
      RECSIZE = 256

      CID = CIRCOPEN (KEY, BUFSIZE)

      IF (CID .LT. 0) THEN
         PRINT *, 'CIRCOPEN ERROR'
      ENDIF

      I = 0
      DO WHILE (I .GE. 0)
         RECLEN = RECSIZE
 1       STAT = CIRCGETRECORD (CID, REC, RECLEN)

         IF (STAT .LE. 0) THEN
            STAT = CIRCISALIVE (CID)

            IF (STAT .GT. 0) THEN
               CALL SLEEP (1)
               GOTO 1
            ELSE
               I = -1
            ENDIF
         ENDIF

         IF (DUMP .GT. 0 .AND. I .GE. 0) THEN
            WRITE (6, *) RECLEN, REC
         ENDIF
      ENDDO

      CALL CIRCCLOSE (CID)

      STOP
      END
