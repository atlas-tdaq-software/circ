#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sched.h>

#include "circ/Error.h"
#include "circ/Circ.h"
#include "circ/Circservice.h"

int cid;
int evtsize = 512;
int bufsize = -1;
char *bufname = (char *) NULL;
char *data = (char *) NULL;
unsigned short port = CIRC_SERVER_DEFAULT_PORT;

int end_loop = 0;

void circ_client_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

int circ_client_help ()
{
  printf ("Usage: Circclient [-p port] <circ_name> <circ_size> [<event_size>]\n");
  return -1;
}

int circ_client_opt (int c, char **v)
{
  register int i;

  for (i=1; i < c ; ++i)
    if (v[i][0] == '-')
    {
      switch (v[i][1])
      {
        case 'p':
          port = (unsigned short) atoi (v[++i]);
          break;
        default:
          return circ_client_help ();
      }
    }
    else if (bufsize >= 0)
      evtsize = atoi (v[i]);
    else if (bufname)
      bufsize = atoi (v[i]);
    else
      bufname = v[i];

  if (! bufname || bufsize < 0)
    return circ_client_help ();

  return 0;
}

int circ_client_init ()
{
  int i;

  signal (SIGTERM, circ_client_signal_handler);
  signal (SIGINT, circ_client_signal_handler);

  data = (char *) malloc (evtsize);

  /* Prepare data */

  for (i=0; i < evtsize; ++i)
    data[i] = (char) (i & 0xff);

  /* Ask the server to open a buffer and connect to it */

  cid = CircOpenCircConnection (port, bufname, bufsize);

  printf ("Circclient got cid = %d\n", cid);
  return (cid < 0);
}

int circ_client_loop ()
{
  int err = 0;
  int number = 0;
  int interval = 10000;
  char *ptr;

  /* Send data until receive an interrupt */

  while (! end_loop)
  {
    if ((ptr = CircReserve (cid, number, evtsize)) != (char *) -1)
    {
      memcpy (ptr, data, evtsize);

      (void) CircValidate (cid, number, ptr, evtsize);
      sched_yield ();

      ++number;

      if (interval && (++number % interval == 0))
	printf (" Circclient> Number= %d \n",number);
    }
  }

  return err;
}

int circ_client_end ()
{
  /* Close the buffer connection and ask 
     the server to close the buffer */

  free (data);

  return (CircCloseCircConnection (port, bufname, cid) != Error_NO);
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = circ_client_opt (argc, argv)))
    return err;

  if ((err = circ_client_init ()))
    return err;

  if ((err = circ_client_loop ()))
    return err;

  if ((err = circ_client_end ()))
    return err;

  return err;
}
