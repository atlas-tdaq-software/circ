/***************************************************************************/
/* Circread.c, circular buffer example and test program between processes. 
 *
 * Usage: Circread <key-name> <buffer-size>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circread <key-name> <buffer-size>\
";
#define USAGE printf("%s\n",Usage), exit(1)

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sched.h>

/* Circ */
#include "circ/Circ.h"

/* timing */
#include "circ/Circ_timing.h"

#define MAX_EVTSIZE      65536
#define MAGIC            0x31071965
#define GO_ON            0
#define STOP_IT          1
#define DEFAULT_TIMEOUT  60

int end_loop = 0;

void Circread_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *key;
  int *data, *ptr;
  int cid, bufsize, evtsize, repeat = 0, empty = 0;
  int number;
  int circOpenError = 0;
  time_t t0, t1, dt, timeout = DEFAULT_TIMEOUT;

  double real_time, cpu_time, cpu_usage;

  timer_id tid = -1;
/*......................................................................... */

  /* Get parameters form the command line */
  if (argc != 3)
    USAGE;
  key     = argv[1];
  bufsize = atoi (argv[2]);

  signal (SIGTERM, Circread_signal_handler);
  signal (SIGINT, Circread_signal_handler);

  data = (int *) malloc (MAX_EVTSIZE);

  /* Open the circular buffer */

  time (&t0);

  do
  {
    if ((cid = CircOpen (NULL, key, bufsize)) < 0)
    {
      circOpenError = 1;
      time (&t1);
      dt = t1 - t0;
      usleep (10);
    }
    else
    {
      circOpenError = 0;
      break;
    }
  } while (bufsize == 0 && dt < timeout);

  if (circOpenError)
  {
    fprintf (stderr, "%s> Circular buffer open failed !\n", argv[0]);
    exit (1);
  }

  printf ("%s> Circular buffer opened for read: key=%s, id=%d\n",
	  argv[0], key, cid);

  /* Test 1: CircLocate/CircRelease without memcpy
  ************************************************/

  printf ("%s> Test #1: CircLocate/CircRelease without moving data\n",
	  argv[0]);

  repeat  = 0;
  data[0] = GO_ON;

  while (data[0] == GO_ON && (!end_loop))
  {
    while ((ptr = (int *) CircLocate (cid, &number, &evtsize))
	   == (int *) -1 && (!end_loop))
    {
      empty++;
      sched_yield ();
    }

    if (end_loop)
      break;

    if (repeat++ == 0)
      tid = circ_timing_start ();

    if (ptr[evtsize / sizeof (int) - 1] != MAGIC)
    {
      printf("%s> Invalid MAGIC number %x found for event #%d !\n",argv[0],
	     ptr[evtsize / sizeof (int) - 1], repeat);
      exit (1);
    }

    if (repeat != number)
    {
      printf("%s> Invalid event number %d found instead of %d !\n",argv[0],
	     number, repeat);
      exit (1);
    }

    printf (" Circread> Number= %d \n", number);

    data[0] = ptr[0];
    (void) CircRelease (cid);

    sched_yield ();
  }

  if (end_loop)
    exit (0);

  circ_timing_stop (tid, &real_time, &cpu_time, &cpu_usage);

  sleep (1);
  printf ("%s> Real-time: %.2fs, CPU-time: %.2fs, CPU-usage: %.1f%%\n",
	  argv[0], real_time, cpu_time, cpu_usage);
  printf ("%s> Events: %d, %.0f per sec\n", argv[0],
	  repeat, (double) repeat / real_time);
  printf ("%s> Bytes : %.0f, %.1f MB /sec\n", argv[0],
	  (double) repeat * evtsize,
	  (double) repeat * evtsize / 1024. / 1024. / real_time);
  printf ("%s> Buffer empty: %d\n", argv[0], empty);

  /* Test 2: CircGet with moving data (memcpy)
  ********************************************/

  printf ("%s> Test #2: CircGet and moving data (memcpy)\n", argv[0]);

  repeat  = 0;
  data[0] = GO_ON;

  while (data[0] == GO_ON && (!end_loop))
  {
    while ((evtsize = CircGet (cid, &number, (char *) data, MAX_EVTSIZE)) 
	   < 0 && (!end_loop))
    {
      empty++;
      sched_yield ();
    }

    if (end_loop)
      break;

    if (repeat++ == 0)
      tid = circ_timing_start ();

    if (data[evtsize / sizeof (int) - 1] != MAGIC)
    {
      printf ("%s> Invalid MAGIC number %x found for event number %d !\n",
	      argv[0], data[evtsize / sizeof (int) - 1], repeat);
      exit (1);
    }

    if (repeat != number)
    {
      printf ("%s> Invalid event number %d found instead of %d !\n",
	      argv[0], number, repeat);
      exit (1);
    }
  }

  circ_timing_stop (tid, &real_time, &cpu_time, &cpu_usage);

  sleep (1);
  printf ("%s> Real-time: %.2fs, CPU-time: %.2fs, CPU-usage: %.1f%%\n",
	  argv[0], real_time, cpu_time, cpu_usage);
  printf ("%s> Events: %d, %.0f per sec\n", argv[0],
	  repeat, (double) repeat / real_time);
  printf ("%s> Bytes : %.0f, %.1f MB /sec\n", argv[0],
	  (double) repeat * evtsize,
	  (double) repeat * evtsize / 1024. / 1024. / real_time);
  printf ("%s> Buffer empty: %d\n", argv[0], empty);

  (void) CircClose (cid);

  return 0;
}
