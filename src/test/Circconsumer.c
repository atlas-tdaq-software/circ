/***************************************************************************/
/* Circconsumer.c, circular buffer consumer, to test circular buffer writers. 
 *
 * Usage: Circconsumer <key-name> <buffer-size>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circconsumer [-d] [-p <printout-interval>] [-s <sleep>] \
<key-name> <buffer-size>\
";
#define USAGE {printf("%s\n",Usage); return(1);}

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sched.h>

/* Circ */
#include "circ/Circ.h"

#define DEFAULT_TIMEOUT  60

int end_loop = 0;

void Circconsumer_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *key;
  int i = 0;
  int dump = 0;
  char *ptr;
  int cid, bufsize, evtsize, nloop = 0, empty = 0;
  int number;
  int interval = 0, nsleep = 0, np = 0;
  int circOpenError = 0;
  time_t t0, t1, dt, timeout = DEFAULT_TIMEOUT;
                                                                                
  if (argc < 3)
    USAGE;

  while (argv[++i][0] == '-')
    switch (argv[i][1]) {
      case 'd':
        dump = 1;
        np ++;
        break;
      case 'p':
        interval = atoi (argv[++i]);
        np += 2;
        break;
      case 's':
        nsleep = atoi (argv[++i]);
        np += 2;
        break;
      default:
        USAGE;
    }

  if (argc != 3 + np)
    USAGE;

  key     = argv[1+np];
  bufsize = atoi (argv[2+np]);

  signal (SIGTERM, Circconsumer_signal_handler);
  signal (SIGINT, Circconsumer_signal_handler);

  /* Open the circular buffer */

  time (&t0);
                                                                                
  do
  {
    if ((cid = CircOpen (NULL, key, bufsize)) < 0)
    {
      circOpenError = 1;
      time (&t1);
      dt = t1 - t0;
      usleep (10);
    }
    else
    {
      circOpenError = 0;
      break;
    }
  } while (bufsize == 0 && dt < timeout);
                                                                                
  if (circOpenError)
  {
    fprintf (stderr, "%s> Circular buffer open failed !\n", argv[0]);
    return (1);
  }

  printf ("%s> Circular buffer opened for read: key=%s, id=%d\n",
	  argv[0], key, cid);

  while (end_loop == 0)
  {
    while ((ptr = CircLocate (cid, &number, &evtsize)) == (char *) -1)
    {
      empty++;

      if (end_loop)
      {
	(void) CircClose (cid);
	return (0);
      }

      sched_yield ();
    }

    if (dump)
    {
      printf (" Circconsumer> Event number %d, size = %d\n",
	      number, evtsize);

      for (i=0; i < evtsize ; ++i)
	printf (" 0x%x ", 0xff & ptr[i]);

      printf ("\n");
      printf ("%s\n", ptr);
    }

    (void) CircRelease (cid);
    sched_yield ();

    if (interval && (++nloop%interval == 0))
      printf (" Circconsumer> Number= %d \n",number);

    if (nsleep)
      sleep (nsleep);
  }

  (void) CircClose (cid);

  return 0;
}
