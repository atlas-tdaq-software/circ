#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sched.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/select.h>

#include "circ/Error.h"
#include "circ/Circ.h"
#include "circ/Circservice.h"

int end_loop = 0;
int verbose = 0;
int evnum = 0;
int dump = 0;
int nc = 0;
float occupancy = 0;
float long_interval = 10.;
struct timeval tout = {0, 100000};

unsigned short port = CIRC_SERVER_DEFAULT_PORT;

/* Thread conditions */

enum Condition
{
  UNSTARTED,
  RUNNING,
  CANCEL_REQUESTED,
  CLEANING_UP,
  TERMINATED
};

/* Only one single thread is started to deal with circ readout.
 * This implies that the following variables can be global.     */

pthread_t id_;
pthread_attr_t attributes_;
pthread_mutex_t mutex_;
pthread_mutex_t request_mutex_;
pthread_mutex_t cond_mutex_;
pthread_cond_t cond_;

enum Condition condition_;

void circ_server_thread_specific_cleanup ()
{
}

void circ_server_thread_run ()
{
  int cidlen = 0;
  static int *cids = (int *) NULL;

  register int i;

  evnum = 0;

  while (1)
  {
    /* Locking at each event can severily affect performance */

    pthread_mutex_lock (&request_mutex_);

    /* Check the list of connected cids */

    nc = CircServerGetCircList (&cids, &cidlen);
    occupancy = 0;

    for (i = 0; i < nc; ++i)
    {
      char *ptr;
      int number, evtsize;

      if ((ptr = CircLocate (cids[i], &number, &evtsize)) != (char *) -1)
      {
	if (dump)
	{
          int k;
	  static int linesize = 8;
          int sizeint = evtsize / sizeof (unsigned int);

	  unsigned int *j = (unsigned int *) ptr;
          unsigned int *jmax = j + sizeint;

          printf ("Event number: %d, Event size %d\n", number, evtsize);

	  while (j < jmax)
	  {
            for (k = 0; k < linesize && j < jmax; ++k, ++j)
	      printf ("%x ", *j);
	    printf ("\n");
	  }

	  printf ("\n");
	}

	(void) CircRelease (cids[i]);
        ++evnum;
        occupancy += (float) CircGetOccupancy (cids[i]) / CircGetSize (cids[i]);
      }
    }

    if (nc > 0) occupancy /= nc;
    pthread_mutex_unlock (&request_mutex_);

    sched_yield ();
    pthread_testcancel ();
  }

  if (cids)
    free (cids);
}

void circ_server_thread_init ()
{
  pthread_mutex_init (&mutex_, 0);
  pthread_mutex_init (&cond_mutex_, 0);
  pthread_mutex_init (&request_mutex_, 0);
  pthread_cond_init (&cond_, 0);
  pthread_attr_init (&attributes_);

  /* Set thread attributes */

  pthread_attr_setdetachstate (&attributes_, PTHREAD_CREATE_DETACHED);
}

int circ_server_thread_wait_for_condition (enum Condition condition, int timeout)
{
  int code = 0;
  struct timespec timeLimit = {0, 0};
  timeLimit.tv_sec = time (0) + timeout;

  pthread_mutex_lock (&cond_mutex_);

  while (condition_ < condition)
  {
    if (pthread_cond_timedwait (&cond_, &cond_mutex_, &timeLimit) == ETIMEDOUT)
    {
      code = -1;
      break;
    }
  }

  pthread_mutex_unlock (&cond_mutex_);

  return code;
}

void circ_server_thread_set_condition (enum Condition condition) 
{
  pthread_mutex_lock (&cond_mutex_);
  condition_ = condition;
  pthread_cond_broadcast (&cond_);
  pthread_mutex_unlock (&cond_mutex_);
}

void circ_server_thread_cleanup ()
{
  pthread_mutex_lock (&mutex_);
  circ_server_thread_set_condition (CLEANING_UP);
  circ_server_thread_specific_cleanup ();
  circ_server_thread_set_condition (TERMINATED);
  pthread_mutex_unlock (&mutex_);
}

void *circ_server_thread_entry_point ()
{
  /* Update cleanup stack */

  pthread_cleanup_push (circ_server_thread_cleanup, 0);

  pthread_setcanceltype (PTHREAD_CANCEL_DEFERRED, 0);
  pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);

  /* Run the user code */

  circ_server_thread_run ();

  pthread_cleanup_pop (1);

  return (0);
}

int circ_server_thread_start_execution () 
{
  int rc = 1;

  pthread_mutex_lock (&mutex_);

  if (condition_ == RUNNING)
  {
    rc = 1;
  }
  else if ((rc = pthread_create (&id_, &attributes_,
				 circ_server_thread_entry_point, 0)) == 0)
  {
    circ_server_thread_set_condition (RUNNING);
  }

  pthread_mutex_unlock (&mutex_);

  return rc;
}

void circ_server_thread_stop_execution () 
{
  pthread_mutex_lock (&mutex_);

  if (condition_ == RUNNING)
  {
    circ_server_thread_set_condition (CANCEL_REQUESTED);

    if (id_ == pthread_self ())
    {
      pthread_exit (0);
    }
    else
    {
      pthread_cancel (id_);
    }
  }

  pthread_mutex_unlock (&mutex_);
}

void circ_server_thread_end ()
{
  pthread_mutex_destroy (&mutex_);
  pthread_mutex_destroy (&cond_mutex_);
  pthread_mutex_destroy (&request_mutex_);
  pthread_cond_destroy (&cond_);
}

void circ_server_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

int circ_server_help ()
{
  printf ("Usage: Circserver [-p port] [-v] [-t connection_check_time] "
                           " [-l garbage_collection_time] [-d]\n");
  return -1;
}

int circ_server_opt (int c, char **v)
{
  register int i;
  float timeout;

  for (i=1; i < c ; ++i)
    if (v[i][0] == '-')
    {
      switch (v[i][1])
      {
        case 'p':
          port = (unsigned short) atoi (v[++i]);
          break;
        case 't':
          timeout = atof (v[++i]);
          tout.tv_sec = (unsigned int) timeout;
          tout.tv_usec = (unsigned int) ((timeout - (float) tout.tv_sec) * 1000000);
          break;
        case 'l':
          long_interval = atof (v[++i]);
          break;
        case 'd':
	  dump = 1;
          break;
        case 'v':
          verbose = 1;
          break;
        default:
          return circ_server_help ();
      }
    }

  return 0;
}

int circ_server_init ()
{
  /* Register signal handlers */

  signal (SIGTERM, circ_server_signal_handler);
  signal (SIGINT, circ_server_signal_handler);

  /* Initialize the reading thread */

  circ_server_thread_init ();

  /* Initialize the server */

  return (CircServerInit (port));
}

int circ_server_loop ()
{
  int err = 0;
  struct timeval t, t0;
  float ft0, ft;

  circ_server_thread_start_execution ();

  gettimeofday (&t0, 0);
  ft0 = (float) t0.tv_sec + (float) t0.tv_usec / 1000000;

  while (! end_loop)
  {
    struct timeval tout_v;

    if (CircServerCheckRequest ())
    {
      pthread_mutex_lock (&request_mutex_);
      CircServerServeRequest ();
      pthread_mutex_unlock (&request_mutex_);
    }

    gettimeofday (&t, 0);
    ft = (float) t.tv_sec + (float) t.tv_usec / 1000000;

    pthread_mutex_lock (&request_mutex_);

    if ((ft - ft0) > long_interval)
    {
      CircServerGarbageCollector ();
      gettimeofday (&t0, 0);
      ft0 = (float) t0.tv_sec + (float) t0.tv_usec / 1000000;
    }

    if (verbose)
    {
      printf ("Buffer number = %d Event number = %d Occupancy = %f\n", nc, evnum, occupancy);
    }

    pthread_mutex_unlock (&request_mutex_);

    memcpy (&tout_v, &tout, sizeof (struct timeval));
    select (0, 0, 0, 0, &tout_v);
  }

  /* Stop the reading thread */

  circ_server_thread_stop_execution ();
  circ_server_thread_wait_for_condition (TERMINATED, 5);

  return err;
}

int circ_server_end ()
{
  int err = 0;

  /* End the server */

  err = CircServerEnd ();

  /* Clean up */

  circ_server_thread_end ();

  return err;
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = circ_server_opt (argc, argv)))
    return err;

  if ((err = circ_server_init ()))
    return err;

  if ((err = circ_server_loop ()))
    return err;

  if ((err = circ_server_end ()))
    return err;

  return err;
}
