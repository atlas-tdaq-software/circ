/***************************************************************************/
/* Circproducer.c, circular buffer event producer, to test readers. 
 *
 * Usage: Circproducer <key-name> <buffer-size> <event-size>
 */
/***************************************************************************/
static char Usage[] = "\
Usage: Circproducer [-p <printout-interval>] [-a <number of records>] \
<key-name> <buffer-size> <event-size> \
";
#define USAGE {printf("%s\n",Usage); return(1);}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sched.h>

/* Circ */
#include "circ/Circ.h"

#define DEFAULT_TIMEOUT  60

int end_loop = 0;

void Circproducer_signal_handler (int val)
{
  printf ("Signal %d received\n", val);
  end_loop = 1;
}

/***************************************************************************/
int main (int argc, char *argv[])
/*---------------------------
*/

/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  char *key;
  int i = 0;
  char *data, *ptr;
  int dump = 0;
  int cid, bufsize, evtsize, nloop = 0, full = 0;
  int number = 0;
  int interval = 0, np = 0;
  int nascii = 0;
  int circOpenError = 0;
  time_t t0, t1, dt, timeout = DEFAULT_TIMEOUT;

  if (argc < 4)
    USAGE;

  while (argv[++i][0] == '-')
    switch (argv[i][1])
    {
      case 'd':
        dump = 1;
        np++;
        break;
      case 'p':
        interval = atoi (argv[++i]);
        np += 2;
        break;
      case 'a':
        nascii = atoi (argv[++i]);
        np += 2;
        break;
      default:
        USAGE;
    }

  if (argc != 4 + np)
    USAGE;

  key     = argv[1+np];
  bufsize = atoi (argv[2+np]);
  evtsize = atoi (argv[3+np]);

  signal (SIGTERM, Circproducer_signal_handler);
  signal (SIGINT, Circproducer_signal_handler);

  data = (char *) malloc (evtsize);

  /* Prepare data */

  if (nascii)
  {
    int j;
    int recordlen = evtsize / nascii;

    ptr = data;

    for (j=0; j < nascii; ++j)
    {
      for (i=0; i < recordlen - 1; ++i, ++ptr)
	sprintf (ptr, "%d", ((i + j) % 10));

      *ptr = '\n'; ++ptr;
    }

    evtsize = recordlen * nascii;
    data[evtsize - 1] = '\0';

    if (dump)
    {
      printf ("Event:\n%s\n", data);
    }
  }
  else
    for (i=0; i < evtsize; ++i)
      data[i] = (char) (i & 0xff);

  /* Open the circular buffer */

  time (&t0);

  do
  {
    if ((cid = CircOpen (NULL, key, bufsize)) < 0)
    {
      circOpenError = 1;
      time (&t1);
      dt = t1 - t0;
      usleep (10);
    }
    else
    {
      circOpenError = 0;
      break;
    }
  } while (bufsize == 0 && dt < timeout);

  if (circOpenError)
  {
    fprintf (stderr, "%s> Circular buffer open failed !\n", argv[0]);
    return (1);
  }

  printf ("%s> Circular buffer opened for write: key=%s, id=%d\n",
	  argv[0], key, cid);

  while (end_loop == 0)
  {
    while ((ptr = CircReserve (cid, number, evtsize)) == (char *) -1)
    {
      full++;

      if (end_loop)
      {
	(void) CircClose (cid);
	return (0);
      }

      sched_yield ();
    }

    memcpy (ptr, data, evtsize);

    (void) CircValidate (cid, number, ptr, evtsize);
    sched_yield ();

    ++number;

    if (interval && (++nloop%interval == 0))
      printf (" Circproducer> Number= %d \n",number);
  }

  (void) CircClose (cid);

  free (data);

  return 0;
}

