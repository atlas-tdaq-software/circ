#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/times.h>

#include "circ/Circ_timing.h"

static double ticks;
static timer *timer_btms = 0;
static unsigned int used_timers = 0;

timer_id circ_timing_start (void)
{
  register unsigned int i;
  timer *btms_element = (timer *) NULL;

  static unsigned int allocated_timers = 0;
  static unsigned int timing_initialized = FALSE;

  /* Get the number of clock ticks per second */

  if (! timing_initialized)
    ticks = (double) sysconf (_SC_CLK_TCK);

  /* If we are using all the pre-allocated slots, allocate a new block */

  if (used_timers == allocated_timers)
  {
    timer *new_pointer;

    if ((new_pointer = (timer *) realloc (timer_btms,
					  TIMER_ALLOCATION_BLOCK * sizeof (timer))))
    {
      memset (new_pointer + allocated_timers, 0, TIMER_ALLOCATION_BLOCK * sizeof (timer));
      allocated_timers += TIMER_ALLOCATION_BLOCK;
      timer_btms = new_pointer;
    }
    else
      return TIMER_ERROR;
  }

  /* Get the first available slot */

  for (i = 0; i < allocated_timers; ++i)
  {
    if (timer_btms[i].time_struct.tms_utime == 0)
    {
      btms_element = timer_btms + i;
      break;
    }
  }

  if (i == allocated_timers)
    return TIMER_ERROR;
  else
  {
    btms_element->time = times (&(btms_element->time_struct));
    return i;
  }
}

void circ_timing_get (timer_id tid, double *real_time, double *cpu_time, double *cpu_usage)
{
  timer current_timer;

  current_timer.time = times (&(current_timer.time_struct));

  *real_time = (double) (current_timer.time - timer_btms[tid].time) / ticks;
  *cpu_time = ((double) (current_timer.time_struct.tms_utime -
			 timer_btms[tid].time_struct.tms_utime) +
	       (double) (current_timer.time_struct.tms_stime -
			 timer_btms[tid].time_struct.tms_stime)) / ticks;

  if (*real_time > 0.0)
    *cpu_usage = *cpu_time / *real_time * 100.;
  else
    *cpu_usage = 0.0;
}

void circ_timing_stop (timer_id tid, double *real_time, double *cpu_time, double *cpu_usage)
{
  circ_timing_get (tid, real_time, cpu_time, cpu_usage);

  memset (timer_btms + tid, 0, sizeof (timer));
  --used_timers;
}
