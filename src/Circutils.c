/***************************************************************************/
/* Circutils.c 
 *
 * =h=> Circular buffer utilities
 * =p=>
 * This file contains add-on to the application interface.
 * Utilities are provided to
 * get single records from ascii formatted events/buffers.
 * Fortran bindings are provided for all the functions (inter-process only).
 * ===<
 * Modification history:
 * 03/01/07 EP  : file created.  
 */
/***************************************************************************/

#include <string.h>
#include <stdlib.h>

#include "circ/Error.h"

#include "circ/Circ.h"
#include "circ/Circutils.h"

/***************************************************************************/
/* =i=> */
char *CircGetEventRecord (char **buf, int *blen)
/*----------------------------------------------------
 * =p=>
 * Returns the pointer to a record in an event
 *            
 *  buf: Pointer to the current record pointer.
 *   It is supposed to contain the pointer to the current record in input,
 *   it contains the pointer to next record or (char *) NULL in output.
 *  
 *  blen: Pointer to the size of the remaining part of event. It is
 *   updated in output.
 *  
 *  Return value:
 *   == 0 : No more data.
 *   >  0 : pointer to the current record (content of the buf input value)
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int n = 0;
  char *orig = *buf;
  char *p = *buf;

  while (n < *blen && *p != '\n')
  {
    ++p;
    ++n;
  }

  *blen -= n;

  if (*blen)
    *p++ = '\0';

  *buf = *blen ? p : (char *) NULL;

  return orig;
}

/***************************************************************************/
/* =i=> */
int CircGetRecord (int cid, char *rec, int *reclen)
/*----------------------------------------------------
 * =p=>
 * Returns the pointer to the next record. It does not require the knowledge
 * of current buffer pointer, since the buffer readout is performed
 * internally.
 * WARNING: This function is not reentrant, and can be used in threaded mode only
 * if a single reading thread is using it.
 *
 *  cid: circular buffer id returned by CircOpen().
 *  
 *  Return value:
 *   == 0 : no data available
 *   >  0 : record length
 *   < 0 in case of error(s).
 * ===< 
 */
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
{
  int l, maxlen = *reclen;
  char *r;

  static int packlen = 0;
  static int reslen = 0;
  static char *pack = 0;
  static char *p;

  memset (rec, ' ', maxlen);

  if (reslen == 0)
  {
    char *ptr;
    int ierr;
    int number, loc_len;

    if ((ierr = CircGetContents (cid)) <= 0)
      return ierr;

    ptr = CircLocate (cid, &number, &loc_len);

    if (ptr == (char *) -1)
      return 0;

    if (loc_len > packlen)
    {
      char *tmp;

      tmp = (char *) realloc (pack, loc_len);
      pack = tmp;
      packlen = loc_len;
    }

    p = pack;
    reslen = loc_len;
    memcpy (pack, ptr, reslen);

    CircRelease (cid);
  }

  l = reslen;
  r = CircGetEventRecord (&p, &reslen);

  l -= reslen;
  *reclen = l;

  if (l > maxlen)
    return (Error_YES);

  memcpy (rec, r, l);

  return l;
}

#ifndef USE_THREAD

/***************************************************************************/
/* Fortran bindings                                                        */
/***************************************************************************/

/***************************************************************************/
int circgetrecord_ (int *cid, char *rec, int *reclen)
/***************************************************************************/
{
  return CircGetRecord (*cid, rec, reclen);
}

#endif
