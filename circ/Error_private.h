/***************************************************************************/
/* Error_private.h
 * =h=> Error, private (non public) definitions 
 * =p=>
 * ===<
 ***************************************************************************/


#ifndef ERROR_PRIVATE_H
#define ERROR_PRIVATE_H

#include "Error.h"

typedef struct _Error
{
  int  number;		 	    /* Error code */
  int  mode;                        /* Output mode */
  int  count;                       /* Error count */
  char message[Error_MESSAGE_SIZE]; /* Error message */
} ErrorRec;

#endif
