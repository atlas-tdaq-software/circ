/* =i=> Circutils.h 
 * =p=>
 * The header file for package Circ utilities.
 * ===<
 */
#ifndef Circutils_h
#define Circutils_h

/* Prototype declarations 
*/

#ifdef __cplusplus
extern "C" {
#endif

char* CircGetEventRecord    (char**, int*);
int   CircGetRecord         (int, char*, int*);

#ifdef __cplusplus
}
#endif

#endif /*Circutils_h*/

