#ifndef Error_protos_h
#define Error_protos_h

/* Prototypes */

#ifdef __cplusplus
extern "C" {
#endif

/* single thread */

void  ErrorClear        (void);
void  ErrorInit         (int);
void  ErrorSet          (int, const char *, const char *);
void  ErrorSetF         (int, const char *, const char *,...);
void  ErrorSetSystem    (int, const char *, const char *);
void  ErrorSetSystemF   (int, const char *, const char *,...);
void  ErrorSetPrintMode (int);
int   ErrorGetPrintMode (void);
int   ErrorGetNumber    (void);
char *ErrorGetMessage   (void);
int   ErrorGetCount     (void);
int   ErrorCopyMessage  (char *);

/* Dispose any resources used by the Error module */
/* Should be called before exiting a thread */

void  ErrorDispose (void);

#ifdef __cplusplus
}
#endif

#endif /* Error_protos_h */
