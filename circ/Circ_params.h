#ifndef _Circ_params_h
#define _Circ_params_h

/* parameters */

#define CIRC_MAX_FILENAME               1024

/* Error codes and messages */

#define CIRC_ERR_BIT                    20
#define CIRC_ERR_SYSTEM_BIT             19
#define CIRC_ERR_CODE                   (1 << CIRC_ERR_BIT)
#define CIRC_ERR_SYSTEM                 (1 << CIRC_ERR_SYSTEM_BIT | CIRC_ERR_CODE)
#define CIRC_ERR_NOERR                  (0x0 | CIRC_ERR_CODE)

/* Circ errors */

#define CIRC_ERR_OPENERR                (0x1 | CIRC_ERR_CODE)
#define CIRC_ERR_CIDALLOC               (0x2 | CIRC_ERR_CODE)
#define CIRC_ERR_INITERR                (0x3 | CIRC_ERR_CODE)
#define CIRC_ERR_SETDEAD                (0x4 | CIRC_ERR_CODE)
#define CIRC_ERR_VALIDATE               (0x5 | CIRC_ERR_CODE)
#define CIRC_ERR_CONFIRM                (0x6 | CIRC_ERR_CODE)
#define CIRC_ERR_REMOVE                 (0x7 | CIRC_ERR_CODE)
#define CIRC_ERR_RELEASE                (0x8 | CIRC_ERR_CODE)
#define CIRC_ERR_USERBUF                (0x9 | CIRC_ERR_CODE)
#define CIRC_ERR_REVALIDATE             (0xA | CIRC_ERR_CODE)
#define CIRC_ERR_CLOSEERR               (0xB | CIRC_ERR_CODE)
#define CIRC_ERR_INVALID                (0xC | CIRC_ERR_CODE)
#define CIRC_ERR_HEADER                 (0xD | CIRC_ERR_CODE)
#define CIRC_ERR_MULTI_FIT              (0xE | CIRC_ERR_CODE)
#define CIRC_ERR_MALLOC                 (0xF | CIRC_ERR_CODE)
#define CIRC_ERR_REQUEST                (0x10 | CIRC_ERR_CODE)

/* System errors */

#define CIRC_ERR_SEMCREATE              (0x1 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SETSEMA                (0x2 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SEMDESTROY             (0x3 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SHMCREATE              (0x4 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SHMMAP                 (0x5 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SHMDESTROY             (0x6 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SHMINFO                (0x7 | CIRC_ERR_SYSTEM)
#define CIRC_ERR_SOCKET                 (0x8 | CIRC_ERR_SYSTEM)

/* Circ error messages */

#define CIRC_MESSAGE_OPENERR            "Error in opening circular buffer"
#define CIRC_MESSAGE_CIDALLOC           "Error in allocating cid"
#define CIRC_MESSAGE_INITERR            "Error in initializing circular buffer"
#define CIRC_MESSAGE_SETDEAD            "Error in setting circular buffer dead"
#define CIRC_MESSAGE_VALIDATE           "Error in validating packet"
#define CIRC_MESSAGE_CONFIRM            "Error in confirming packet"
#define CIRC_MESSAGE_REMOVE             "Error in removing packet"
#define CIRC_MESSAGE_RELEASE            "Error in releasing packet"
#define CIRC_MESSAGE_USERBUF            "Error in getting packet, user buffer is too small"
#define CIRC_MESSAGE_REVALIDATE         "Error in revalidating packet"
#define CIRC_MESSAGE_CLOSEERR           "Error in closing circular buffer"
#define CIRC_MESSAGE_INVALID            "Buffer is invalid"
#define CIRC_MESSAGE_HEADER             "Buffer header is invalid"
#define CIRC_MESSAGE_MULTI_FIT          "Buffer id do not fit in cid array"
#define CIRC_MESSAGE_MALLOC             "Error allocating memory"
#define CIRC_MESSAGE_REQUEST            "Unrecognized request"

/* System error messages */

#define CIRC_MESSAGE_SEMCREATE          "Error creating semaphore"
#define CIRC_MESSAGE_SETSEMA            "Error setting semaphore"
#define CIRC_MESSAGE_SEMDESTROY         "Error destroying semaphore"
#define CIRC_MESSAGE_SHMCREATE          "Error creating shared memory"
#define CIRC_MESSAGE_SHMMAP             "Error mapping shared memory"
#define CIRC_MESSAGE_SHMDESTROY         "Error destroying shared memory"
#define CIRC_MESSAGE_SHMINFO            "Error getting shared memory info"
#define CIRC_MESSAGE_SOCKET             "Error in remote communication protocol"

#endif
