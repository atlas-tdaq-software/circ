/* =i=> Circ.h 
 * =p=>
 * The header file for package Circ: Should be included by 
 * each application using the circular buffer interface!
 * ===<
 */

#ifndef Circ_h
#define Circ_h

#include <sys/ipc.h>
#include <sys/shm.h>

typedef struct
{
  /* Statistic params, all mod 1024^3 */

  unsigned int nr_insert;        /* Nr. of elements inserted in the CB */
  unsigned int nr_retreive;      /* Nr. of elements read-out from the CB */
  unsigned int nr_try_retreive;  /* Nr. of attempts to read-out */
  unsigned int nr_spy;           /* Nr. of Elements spied from the CB */
  unsigned int nr_try_spy;       /* Nr. of attempts to spy */
} TCircStatistics;

/* if CircSemIgnoreIntr true, SETSEMA will ignore the EINTR signal */
/* if CircSemTimout is not 0, SETSEMA will use a sleep timout */
/* This is the default */

extern int CircSemIgnoreIntr;
extern int CircSemTimeout;

/* Prototype declarations */

#ifdef __cplusplus
extern "C" {
#endif

int   CircOpen         (char*, char*, int);
int   CircOpen_t       (char*, char*, int);
int   CircOpenMulti    (char*, char*, int, int, int*);
int   CircClose        (int);
int   CircClose_t      (int);
int   CircCloseMulti   (int, int*);
char* CircReserve      (int, int, int);
char* CircReserve_t    (int, int);
int   CircValidateParam(int, int, char*, int,  int);
int   CircValidate     (int, int, char*, int);
int   CircValidate_t   (int, char*, int);
int   CircCondValidate (int, int, char*, int);
int   CircCondConfirm  (int);
int   CircCondRemove   (int);
char* CircLocate       (int, int*, int*);
int   CircLocateMulti  (int, int,  int*, char**, int* , int*);
char* CircLocateAhead  (int, int*, int*);
int   CircRevalidate   (int);
int   CircRelease      (int);
int   CircReleaseMulti (int, int, int*);
int   CircReleaseAhead (int);
int   CircPutParam     (int, int , char*, int,  int);
int   CircPut          (int, int , char*, int);
int   CircPut_t        (int, char*, int);
int   CircCondPut      (int, int , char*, int);
int   CircGet          (int, int*, char*, int);
int   CircReset        (int);
int   CircSetDead      (int);
int   CircSetDeadMulti (int, int*);

int   CircIsAlive      (int);
char* CircGetAddress   (int);
int   CircGetContents  (int);
int   CircGetOccupancy (int);
int   CircGetStatistics(int, TCircStatistics *);
int   CircGetSize      (int);
int   CircGetSizeMulti (int, int*);
int   CircGetInfo      (int, struct shmid_ds*);
int   CircResetStatistics (int);

char* CircSearch       (int, int*, int*); 
int   CircCopied       (int, char*);                       

#ifdef __cplusplus
}
#endif

#endif /* Circ_h */
