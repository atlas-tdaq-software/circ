/* =i=> Circservice.h 
 * =p=>
 * The header file for Circservice library.
 * ===<
 */
#ifndef Circservice_h
#define Circservice_h

#define CIRC_SERVER_DEFAULT_HOSTNAME         "localhost"
#define CIRC_SERVER_DEFAULT_PORT             9876

#define CIRC_SERVER_MIN_CONNECTION_LIFETIME  0.0

#define CIRC_SERVER_REQTYPE_NULL             0
#define CIRC_SERVER_REQTYPE_OPEN             1
#define CIRC_SERVER_REQTYPE_CONNECT          2
#define CIRC_SERVER_REQTYPE_CLOSE            3

struct buflist_elem {
  int cid;
  int connected;
  time_t buftime;
  char *bufname;
  struct buflist_elem *last;
  struct buflist_elem *next;
};

struct circserver_request {
  int reqtype;
  int circsize;
  char *circname;
};

/* Prototype declarations 
*/

#ifdef __cplusplus
extern "C" {
#endif

/* Common ancillary functions */

int CircSafeSend (int, char *, int);
int CircSafeReceive (int, char *, int);

/* Server ancillary functions */

int CircServerAddToList (struct buflist_elem **, int, char *);
int CircServerRemoveFromList (struct buflist_elem **, int);
int CircServerSearchList (struct buflist_elem *, char *);
int CircServerGetInstances (struct buflist_elem *, char *);
void CircServerCleanAll ();

/* Server public functions */

int CircServerInit (unsigned short);
int CircServerCheckRequest (void);
int CircServerGetRequest (struct circserver_request *);
int CircServerExecuteRequest (int, struct circserver_request *);
int CircServerServeRequest (void);
int CircServerGetCid (char *);
int CircServerGarbageCollector (void);
int CircServerEnd (void);
int CircServerGetCircList (int **, int *);

/* Client ancillary functions */

int CircClientOpenConnection (char *, unsigned short);
int CircClientCloseConnection (int s);
int CircSendRequest (int, char *, unsigned short, char *, int);
int CircRequestOpen (char *, unsigned short, char *, int);
int CircRequestConnect (char *, unsigned short, char *, int);
int CircRequestClose (char *, unsigned short, char *);

/* Client public functions */

int CircOpenCircConnection (unsigned short, char *, int);
int CircCloseCircConnection (unsigned short, char *, int);
int CircOpenCircConnection_t (unsigned short, char *, int);
int CircCloseCircConnection_t (unsigned short, char *, int);

#ifdef __cplusplus
}
#endif

#endif /*Circservice_h*/
