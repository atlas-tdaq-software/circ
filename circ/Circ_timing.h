#ifndef CIRCTIMING_H

#include <sys/times.h>

#define TIMER_ERROR             -1
#define TIMER_ALLOCATION_BLOCK   16

#ifndef TRUE
#  define TRUE    1
#  define FALSE   0
#endif

typedef int timer_id;

typedef struct {
  long time;
  struct tms time_struct;
} timer;

#ifdef __cplusplus
extern "C" {
#endif

  timer_id circ_timing_start (void);
  void     circ_timing_get   (timer_id id, double *real_time, double *cpu_time, double *cpu_usage);
  void     circ_timing_stop  (timer_id id, double *real_time, double *cpu_time, double *cpu_usage);

#ifdef __cplusplus
}
#endif

#endif
