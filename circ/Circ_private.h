/* =i=> Circ_private.h 
 * =p=>
 * Private header file for package Circ. Should not be called by any
 * public application
 * ===<
 * Modification history:
 *
 * some time ago: original idea and implementation from Harry Beker.
 * 20/12/96 WG  : code review and restructuring to support POSIX stuff. 
 * 21/12/96 WG  : allow multiple locate of events for parallel processing!
 * 10/12/98 IS  : EINTR handling in SETSEMA introduced
 */

#ifndef Circ_private_h
#define Circ_private_h

/* if CircSemIgnoreIntr true, SETSEMA will ignore the EINTR signal */
/* if CircSemTimout is not 0, SETSEMA will use a sleep timout */
/* This is the default */

extern int CircSemIgnoreIntr;
extern int CircSemTimeout;

#include <errno.h>
#include <unistd.h>
#include <sys/types.h>

#ifdef USE_THREAD		/* POSIX THREAD */

# include <pthread.h>
# define SEMID pthread_mutex_t

/* this does not (yet) manage EINTR signal */

# define SETSEMA(id,where,return_code) { \
    if (pthread_mutex_lock(&id) < 0) {\
      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"lock failed (pthread_mutex_lock");\
      return(return_code); }}
# define CLRSEMA(id,where,return_code) { \
    if (pthread_mutex_unlock(&id) < 0) {\
      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"unlock failed (pthread_mutex_unlock)");\
      return(return_code); }}

#else

# ifdef USE_POSIX
#  define USE_POSIX_SEM
#  define USE_POSIX_SHM
# else
#  ifdef USE_SYSV
#   define USE_SYSV_SEM
#   define USE_SYSV_SHM
#  endif
# endif

# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
				/* POSIX shared memory and semaphore */
# ifdef USE_POSIX_SHM
#  include <sys/mman.h>
# endif

# ifdef USE_POSIX_SEM
#  include <semaphore.h>
#  define SEMID sem_t*
#  define SETSEMA(id,where,return_code) \
          { \
	    int SETSEMA_repeat; \
	    \
	    do \
	      { \
		SETSEMA_repeat = 0; \
		\
                if (CircSemTimeout > 0) \
                { \
		  time_t secs; \
		  time_t time (&secs); \
                  const struct timespec tout = {secs + CircSemTimeout,0}; \
                  \
                  if (sem_timedwait(id,&tout) < 0) \
                  { \
                    if (CircSemIgnoreIntr && (errno==EINTR)) \
                      SETSEMA_repeat = 1; /* interupt, retry */ \
                    else \
                    { /* serious error or timeout expired */ \
                      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"lock failed (sem_wait)"); \
                      return(return_code); \
                    } \
                  } \
                } \
                else \
		{ \
		  if (sem_wait(id) < 0) \
                  { \
		    if (CircSemIgnoreIntr && (errno==EINTR)) \
		      SETSEMA_repeat = 1; /* interrupt, retry */ \
		    else \
		    { /* serious error */ \
		      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"unlock failed (sem_post)"); \
		      return(return_code); \
		    } \
		  } \
                } \
	      } \
	    while (SETSEMA_repeat); \
	  }
#  define CLRSEMA(id,where,return_code) { \
     if (sem_post(id) < 0) {\
       ErrorSetSystem(CIRC_ERR_SETSEMA,where,"unlock failed (sem_post)"); \
       return(return_code); }}
# endif

# ifdef USE_SYSV_SHM 
#  include <sys/shm.h>
#  ifndef SHM_R
#   define SHM_R 0
#  endif
#  ifndef SHM_W 
#   define SHM_W 0
#  endif
# endif

# ifdef USE_SYSV_SEM
#  include <sys/sem.h>
#  define SEMID key_t
#  ifdef THIS_IS_CircIPC
   struct sembuf waitop[1], clrop[1];
#  else
   extern struct sembuf waitop[1], clrop[1];
#  endif
#  define SETSEMA(id,where,return_code) \
          { \
	    int SETSEMA_repeat; \
	    \
	    do \
	      { \
		SETSEMA_repeat = 0; \
		\
		if (CircSemTimeout > 0) \
		{ \
		  const struct timespec tout = {CircSemTimeout,0}; \
		  \
                  if (semtimedop(id,waitop,1,&tout) < 0) \
      		  { \
                    if (CircSemIgnoreIntr && (errno==EINTR)) \
                      SETSEMA_repeat = 1; /* interupt, retry */ \
                    else \
                    { /* serious error or timeout expired */ \
                      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"lock failed (sem_wait)"); \
                      return(return_code); \
                    } \
		  } \
	        } \
		else \
		{ \
		  if (semop(id,waitop,1) < 0) \
                  { \
		    if (CircSemIgnoreIntr && (errno==EINTR)) \
		      SETSEMA_repeat = 1; /* interupt, retry */ \
		    else \
		    { /* serious error */ \
		      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"lock failed (sem_wait)"); \
		      return(return_code); \
		    } \
		  } \
                } \
	      } \
	    while (SETSEMA_repeat); \
	  }
#  define CLRSEMA(id,where,return_code) { \
     if (semop(id,clrop,1) < 0) {\
      ErrorSetSystem(CIRC_ERR_SETSEMA,where,"unlock failed (semop clear)"); \
      return(return_code); }}
# endif

# if !defined(USE_SYSV_SEM) && !defined(USE_POSIX_SEM) 
#  define SEMID int
#  define SETSEMA(id,where,return_code) 
#  define CLRSEMA(id,where,return_code) 
# endif

#endif

#define long32 int32_t

#define CIRC_HEADER_MAGIC 0x09121998
#define CIRC_HEADER_SIZE  (19*sizeof(long32)) /* all words before buffer */
struct CircDesc
  { long32 
    master,          /* pid of the master process */
    length,          /* total lenght of buffer in long words */
    head,            /* index (! not address) of first event */ 
    tail,            /* index where next event will be put */
    firstcond,       /* index of the first filling event in the buffer */
    contents,        /* number of non filling events in buffer */
    filling,         /* number of events that are filling */
    lastreserved,    /* if >0, the last event was reserved but not validated *
                      * can be >1, if consequent reserve were called */
    ceiling,         /* current top of buffer (avoid event wrap) */
    loc_head,        /* head of the most previous event located */
    loc_ahead,       /* number of events located in advance */
    multi,           /* multiple buffers to follow n, n-1 etc. */  
    size,            /* total size of this buffer including header */
    magic,           /* for consistency checks 
		      * after a call to close, it is 0*/

      /* Statistic params, all mod 1024^3 */
    nr_insert,       /* Nr. of elements inserted in the CB */
    nr_retreive,     /* Nr. of elements read-out from the CB */
    nr_try_retreive, /* Nr. of attempts to read-out */
    nr_spy,          /* Nr. of Elements spied from the CB */
    nr_try_spy;      /* Nr. of attempts to spy */

    char buffer[1];  /* raw data */  
  };

struct EventHeader
{
  long32 Blen;   /* total length of entry including unused area 
		    event header */
  long32 Status; /*  one of CIRC_FILLING, CIRC_READY, 
		     CIRC_EMPTYING  */
  long32 EventNr; /* consecutive event number */
  long32 EventLen;/* rawevent length in long words */
};

struct CircEntry
  {
    struct EventHeader EventHeader ;
    char buffer[1];
  };

#define CIRC_FILLING    1
#define CIRC_READY      2
#define CIRC_EMPTYING   3
#define CIRC_COPYING    4

/* Buffer management structure
 */
struct CircBuffer
  {
    int   cid;
    pid_t master;
    struct CircDesc *shm_desc;
    int   shm_id;
    int   shm_size;
#ifdef USE_POSIX_SHM
    int   shm_size_real;
#endif
    char* shm_name;
    SEMID sem_id;
    char* sem_name;
    int nr;
  };

/* Prototype declarations 
*/
int CircGetId      (void);
int CircInit       (struct CircBuffer*, int);
int CircSemCreate  (struct CircBuffer*, char*, int);
int CircSemDestroy (struct CircBuffer*);
int CircShmCreate  (struct CircBuffer*, char*, int);
int CircShmDestroy (struct CircBuffer*);
int CircShmInfo    (struct CircBuffer*, struct shmid_ds *);

#define DEBUG_ONno
#ifdef DEBUG_ON
# define DEBUG(x) printf x
# define DEBUG1(x) 
#else
# define DEBUG(x) 
# define DEBUG1(x) 
#endif

#endif /* Circ_private_h */
