#ifndef Error_h
#define Error_h

#define Error_HEADER_SIZE  8192
#define Error_MESSAGE_SIZE 131072

/* Error handling */

#define Error_PRINT_OFF     1<<0
#define Error_PRINT_AT_SET  1<<1
#define Error_PRINT_AT_GET  1<<2
#define Error_PRINT_DEFAULT Error_PRINT_AT_SET

/* Default Return codes. A negative value indicates failure! */

#define Error_NO      0
#define Error_YES    -1 
#define Error_NULL    0 /* failure for pointers */

/* Error types */

#define Error_UNKNOWN 0
#define Error_SYSTEM  0
#define Error_FATAL   1<<16
#define Error_WARNING 1<<17

#include "Error_protos.h"

#endif /* Error_h */
